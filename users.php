<?php  include "a_includes/header.php"; ?>
    <!-- ============================================================== -->
<?php  include "a_includes/topNav.php"; ?>

        <!-- End Top Navigation -->
        
<?php  include "a_includes/sideNav.php"; ?>   
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">System Members</h4> </div>
                    
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /row -->
                <div class="row">
                
              
                     <div class="col-sm-12">
                        <div class="white-box">
                             
                           
                            <div class="table-responsive">

                            <table  id="myTables"
                                class="table table-striped table-bordered "
                               >
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>NAME</th>                         
                                        <th>PHONE</th>
                                        <th>STATUS</th> 
                                        <th>ROLE</th>                       
                                        <th><i class="fa fa-user"></i> </th>
                                        <th><i class="fa fa-plus"></i> </th>
                                        <th><i class="fa fa-cogs"></i> </th>
                                        <th> </th>
                                        <th> </th>
                                    </tr>
                                </thead>


                                <tbody>
                                    <?php echo fetch_users(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </div>
    <!-- end page content-->

</div>
<!-- page wrapper end -->


<!--   modal begins here -->





<!-- Footer -->
<?php include "a_includes/footer.php";?>