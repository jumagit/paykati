  $(document).on("submit", ".editInstitution", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({   
            title: "Are you sure?",   
            text: "You will be able to edit an Institution!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#922d00",   
            confirmButtonText: "Yes, edi it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_institution.php?t=edit",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! An Institution has been Updated Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.href = "modes.php";
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    }); 
            } else {     
                swal("Cancelled", "Your Request has been cancelled :)", "error");   
            } 
        });
    });



 deleteInstitution = function (id){
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this Institution!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                 $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_institution.php?t=delete&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait an Institution has been deleted!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });  
            } else {     
                swal("Cancelled", "Your imaginary file is safe :)", "error");  
                  setTimeout(function() {
                           window.location.reload();
                     }, 1000); 
            } 
        });
    };


//modes 




  $(document).on("submit", ".editMode", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({   
            title: "Are you sure?",   
            text: "You will be able to edit this Mode!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#922d00",   
            confirmButtonText: "Yes, edi it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_modes.php?t=edit",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A Mode has been Updated Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.href = "modes.php";
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    }); 
            } else {     
                swal("Cancelled", "Your Request has been cancelled :)", "error");   
            } 
        });
    });


//delete mode
 deleteMode = function (id){
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this Mode!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                 $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_modes.php?t=delete&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait a Payment modes has been deleted!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });  
            } else {     
                swal("Cancelled", "Your imaginary file is safe :)", "error");  
                  setTimeout(function() {
                           window.location.reload();
                     }, 1000); 
            } 
        });
    };



//project expenses 


 $(document).on("submit", "#create_expense_cat", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({   
            title: "Are you sure?",   
            text: "You will be able to add Expense category!",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#384888",   
            confirmButtonText: "Yes, add it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_expense_categories.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A New Expense category has been Created Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.href = "expense_projection.php";
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    }); 
            } else {     
                swal("Cancelled", "Your Request has been cancelled :)", "error");  
                              setTimeout(function() {
                                        window.location.href = "expense_projection.php";
                                   }, 1000); 
            } 
        });
    });


//delete expense category


 deleteExpenseCat = function (id){
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this Expense Category!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                 $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_expense_categories.php?t=delete&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait an Expense Category has been deleted!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });  
            } else {     
                swal("Cancelled", "Your imaginary file is safe :)", "error");  
                  setTimeout(function() {
                           window.location.reload();
                     }, 1000); 
            } 
        });
    };


//deleteExpenseProjection

 deleteExpenseProjection = function (id){
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this Expense Projection!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                 $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_project_expense.php?t=delete&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait a Project Expense has been deleted!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });  
            } else {     
                swal("Cancelled", "Your imaginary file is safe :)", "error");  
                  setTimeout(function() {
                           window.location.reload();
                     }, 1000); 
            } 
        });
    };



 //add Projection


  $(document).on("submit", "#addProjection", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({   
            title: "Are you sure?",   
            text: "You will be able to add Projection Expense!",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#384888",   
            confirmButtonText: "Yes, add it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_project_expense.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A New Expense category has been Created Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.href = "expense_projection.php";
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    }); 
            } else {     
                swal("Cancelled", "Your Request has been cancelled :)", "error");  
                              setTimeout(function() {
                                        window.location.href = "expense_projection.php";
                                   }, 1000); 
            } 
        });
    });



//expense projection


  $(document).on("submit", ".editProjectionSide", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({   
            title: "Are you sure?",   
            text: "You will be able to edit this Projection!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#922d00",   
            confirmButtonText: "Yes, edi it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_project_expense.php?t=edit",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A Project Expense has been Updated Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.href = "expense_projection.php";
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    }); 
            } else {     
                swal("Cancelled", "Your Request has been cancelled :)", "error");   
            } 
        });
    });


  //revenue Categories



 $(document).on("submit", "#create_revenue_cat", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({   
            title: "Are you sure?",   
            text: "You will be able to add Revenue category!",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#384888",   
            confirmButtonText: "Yes, add it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_revenue_categories.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A New Revenue category has been Created Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.href = "income_projections.php";
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    }); 
            } else {     
                swal("Cancelled", "Your Request has been cancelled :)", "error");  
                              setTimeout(function() {
                                        window.location.href = "income_projections.php";
                                   }, 1000); 
            } 
        });
    });


//delete revenue cat

 deleteRevenueCat = function (id){
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this Revenue Category!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                 $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_revenue_categories.php?t=delete&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait a Revenue Category has been deleted!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });  
            } else {     
                swal("Cancelled", "Your imaginary file is safe :)", "error");  
                  setTimeout(function() {
                           window.location.reload();
                     }, 1000); 
            } 
        });
    };

//add revenue projection 

  $(document).on("submit", "#addRevenueExpense", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({   
            title: "Are you sure?",   
            text: "You will be able to add Projection Revenue!",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#384888",   
            confirmButtonText: "Yes, add it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_revenue_expense.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A New Revenue Expense has been Created Successfully!",
                                        type: "success"
                                   });

                                 setTimeout(function() {
                                       window.location.reload();
                                 }, 1000); 
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    }); 
            } else {     
                swal("Cancelled", "Your Request has been cancelled :)", "error");  
                              setTimeout(function() {
                                 window.location.reload();
                           }, 1000); 
            } 
        });
    });


//deleteRevenueProjection

 deleteRevenueProjection = function (id){
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this Revenue Expense!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                 $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_revenue_expense.php?t=delete&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait a Revenue Projection Expense has been deleted!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });  
            } else {     
                swal("Cancelled", "Your imaginary file is safe :)", "error");  
                  setTimeout(function() {
                           window.location.reload();
                     }, 1000); 
            } 
        });
    };

      $(document).on("submit", ".editRevCat", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({   
            title: "Are you sure?",   
            text: "You will be able to edit this Revenue Category!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#922d00",   
            confirmButtonText: "Yes, edi it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_revenue_categories.php?t=edit",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A Project Revenue Category has been Updated Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    }); 
            } else {     
                swal("Cancelled", "Your Request has been cancelled :)", "error");   
            } 
        });
    });

//creating revenues




 $(document).on("submit", "#Add_Revenue", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({   
            title: "Are you sure?",   
            text: "You will be able to add Revenue !",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#384888",   
            confirmButtonText: "Yes, add it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_revenue.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A New Revenue category has been Created Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    }); 
            } else {     
                swal("Cancelled", "Your Request has been cancelled :)", "error");  
                              setTimeout(function() {
                                        window.location.reload();
                                   }, 1000); 
            } 
        });
    });


 //edit_revenue

  $(document).on("submit", ".edit_revenue", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({   
            title: "Are you sure?",   
            text: "You will be able to edit this Revenue !",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#922d00",   
            confirmButtonText: "Yes, Edit it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_revenue.php?t=edit",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A  Revenue  has been Updated Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    }); 
            } else {     
                swal("Cancelled", "Your Request has been cancelled :)", "error");   
            } 
        });
    });


//delete revenue


 deleteRevenue = function (id){
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this Revenue!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                 $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_revenue.php?t=delete&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait a Revenue has been deleted!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });  
            } else {     
                swal("Cancelled", "Your imaginary file is safe :)", "error");  
                  setTimeout(function() {
                           window.location.reload();
                     }, 1000); 
            } 
        });
    };



    //create expense 


 $(document).on("submit", "#Add_Expense", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({   
            title: "Are you sure?",   
            text: "You will be able to add Expense !",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#384888",   
            confirmButtonText: "Yes, add it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_expense.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A New Expense category has been Added Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    }); 
            } else {     
                swal("Cancelled", "Your Request has been cancelled :)", "error");  
                              setTimeout(function() {
                                        window.location.reload();
                                   }, 1000); 
            } 
        });
    });


//deleteOneExpense


 deleteOneExpense = function (id){
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this Expense Add!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                 $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_expense.php?t=delete&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait a Expense has been Removed!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });  
            } else {     
                swal("Cancelled", "Your imaginary file is safe :)", "error");  
                  setTimeout(function() {
                           window.location.reload();
                     }, 1000); 
            } 
        });
    };


//add general title


 $(document).on("submit", "#Add_general_expense", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({   
            title: "Are you sure?",   
            text: "You will be able to add General Expense !",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#384888",   
            confirmButtonText: "Yes, add it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_expense.php?t=expense",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A New Expense category has been Added Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    }); 
            } else {     
                swal("Cancelled", "Your Request has been cancelled :)", "error");  
                              setTimeout(function() {
                                        window.location.reload();
                                   }, 1000); 
            } 
        });
    });

//temp sale


 $(document).on("submit", ".add_sale", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({   
            title: "Are you sure?",   
            text: "You will be able to add Sale item !",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#384888",   
            confirmButtonText: "Yes, add it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_temp_order.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! Added Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    }); 
            } else {     
                swal("Cancelled", "Your Request has been cancelled :)", "error");  
                              setTimeout(function() {
                                        window.location.reload();
                                   }, 1000); 
            } 
        });
    });


 //deleteOneExpense


 deleteCartItem = function (id){
           
                 $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_temp_order.php?t=delete&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "  Removed from cart",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 500);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });  
           
    };


 addCartItem = function (id,price){
           
                 $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_temp_order.php?t=add&id="+id+"&price="+price,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "  Increased cart",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 500);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });  
           
    };


removeCartItem = function (id,price){
           
                 $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_temp_order.php?t=remove&id="+id+"&price="+price,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "  Decreased cart",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 500);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });  
           
    };