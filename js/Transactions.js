$(function () {

     $(document).on("submit", "#deposit", function (e) {
          e.preventDefault();

          var formData = new FormData(this);
          swal({
               title: "Are you sure?",
               text: "You are making A deposit!",
               type: "warning",
               showCancelButton: true,
               confirmButtonColor: "#384888",
               confirmButtonText: "Yes, Deposit!",
               cancelButtonText: "No, cancel !",
               closeOnConfirm: false,
               closeOnCancel: false
          }, function (isConfirm) {
               if (isConfirm) {
                    $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/deposit.php?t=true",
                         data: formData,
                         success: function (result) {                             
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A deposit has been made Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function () {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function (jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
               } else {
                    swal("Cancelled", "Your Request has been cancelled :)", "error");
                    setTimeout(function () {
                         window.location.reload();
                    }, 1000);
               }
          });
     });


     //deposit method

     $(document).on("submit", ".withdraw", function (e) {
          e.preventDefault();

          var formData = new FormData(this);
          swal({
               title: "Are you sure?",
               text: "You are making A Withdraw!",
               type: "warning",
               showCancelButton: true,
               confirmButtonColor: "#384888",
               confirmButtonText: "Yes, Withdraw!",
               cancelButtonText: "No, cancel !",
               closeOnConfirm: false,
               closeOnCancel: false
          }, function (isConfirm) {
               if (isConfirm) {
                    $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/withdraw.php?t=true",
                         data: formData,
                         success: function (result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A Withdraw has been made Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function () {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function (jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
               } else {
                    swal("Cancelled", "Your Request has been cancelled :)", "error");
                    setTimeout(function () {
                         window.location.reload();
                    }, 1000);
               }
          });
     });



     //Transfer method

     $(document).on("submit", ".Transfer", function (e) {
          e.preventDefault();

          var formData = new FormData(this);
          swal({
               title: "Are you sure?",
               text: "You are making A Transfer!",
               type: "warning",
               showCancelButton: true,
               confirmButtonColor: "#384888",
               confirmButtonText: "Yes, Deposit!",
               cancelButtonText: "No, cancel !",
               closeOnConfirm: false,
               closeOnCancel: false
          }, function (isConfirm) {
               if (isConfirm) {
                    $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/transfer.php?t=true",
                         data: formData,
                         success: function (result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A Transfer has been made Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function () {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function (jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
               } else {
                    swal("Cancelled", "Your Request has been cancelled :)", "error");
                    setTimeout(function () {
                         window.location.reload();
                    }, 1000);
               }
          });
     });




});