<?php  include "includes/header.php"; ?>
<!-- ============================================================== -->
<?php  include "includes/topNav.php"; ?>

<!-- End Top Navigation -->

<?php  include "includes/sideNav.php"; ?>
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">LOGS</h4>
            </div>

            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">


            <div class="col-sm-12">
                <div class="white-box">


                    <div class="table-responsive">


                        <table id="myTable" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead class="">
                                <tr>
                                    <th>S/N</th>
                                    <th>User</th>
                                    <th>Activity</th>
                                    <th>Time</th>
                                    <th>Log Status</th>

                                </tr>
                            </thead>


                            <tbody>


                                <?php  echo fetch_logs($_SESSION['client_id']);  ?>



                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <?php   include "includes/footer.php"; ?>