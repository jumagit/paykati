<?php include "includes/header.php";?>
<!-- ============================================================== -->
<?php include "includes/topNav.php";?>

<!-- End Top Navigation -->

<?php include "includes/sideNav.php";?>
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">CREATE RECEIPIENT</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <form action="" id="addClientFormMEMBER">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group ">

                                            <label for="email">Email :</label>

                                            <input type="email" name="email" class="form-control" parsley-type="email"
                                                required placeholder="Enter Email Address">

                                        </div>


                                        <div class="form-group ">

                                            <label for="location">Location :</label>

                                            <input type="text" name="location" class="form-control"
                                                placeholder="Enter Location" required>

                                        </div>



                                        <div class="form-group ">

                                            <label for="mobile">Phone :</label>

                                            <input type="text" name="mobile" class="form-control"
                                                data-parsley-type="number" placeholder="Enter Phone Contact" required
                                                data-parsley-minlength="10" data-parsley-maxlength="10">

                                        </div>

                                        <div class="form-group ">


                                            <label for="username">Full Name :</label>

                                            <input type="text" name="fullName" class="form-control"
                                                placeholder="Enter Full Names" required>

                                        </div>


                                        <div class="form-group ">
                                            <label for="username">Username :</label>
                                            <input type="text" name="username" class="form-control"
                                                placeholder="Enter Username" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <p style="color:red;">Account Details ***</p>
                                        </div>
                                        <div class="form-group">
                                            <label class=" control-label">Mobile Money Number <span
                                                    style="color:red;">*</span></label>

                                            <input type="text" class="form-control" id="" name="mobile_money_number"
                                                placeholder="Mobile Money Number" required="">
                                        </div>


                                        <div class="form-group">
                                            <label class=" control-label">Account Number <span
                                                    style="color:red;">*</span></label>
                                            <input type="text" class="form-control" name="accountno" id=""
                                                placeholder="Account Number">

                                        </div>

                                        <div class="form-group">
                                            <label class=" control-label">Account Name <span
                                                    style="color:red;">*</span></label>
                                            <input type="text" class="form-control" name="accountname" required="">


                                        </div>


                                    </div>
                                </div>


                                <div class="form-group">

                                    <button type="submit" class="btn btn-info btn-block waves-effect waves-light">
                                        Create Account
                                    </button>


                                </div>



                            </form>





                        </div>
                    </div>
                </div>
            </div>

            <!--./row-->
        </div>


    </div>
</div>


<script>
    $('form#addClientFormMEMBER').on("submit", function(e) {
        e.preventDefault();

        var formData = new FormData(this);

        swal({
            title: "Are you sure?",
            text: "Okay to add a  New Receipient",
            type: "info",
            padding: 20,
            showCancelButton: true,
            confirmButtonColor: "#384888",
            confirmButtonText: "Yes, create!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "php_action/create_client_member.php?t=true",
                    data: formData,
                    success: function(result) {

                        if (result.status) {
                            swal({
                                title: "Good job!",
                                padding: 20,
                                text: "Good Job! A New client has been Created Successfully!",
                                type: "success"
                            });

                            setTimeout(function() {
                                window.location.reload();
                            }, 2000);
                        } else {
                            swal({
                                title: "Oops!",
                                padding: 20,
                                text: result.msg + "..please try again!",
                                type: "warning"
                            });
                        }
                    },
                    error: function(jqXHR) {
                        console.log(jqXHR);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });
    });
</script>

<?php include "includes/footer.php";?>