            <footer class="footer text-center"> <?php echo date("Y"); ?> &copy; Copyrights Reserved. </footer>
        </div>
        
    </div>
    
    
  
    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->

    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--Counter js -->
    <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
    <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
  
    <!--slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    
    <script src="plugins/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="plugins/bower_components/datatables/media/js/dataTables.bootstrap.js"></script>
    
    
     <script src="plugins/chart.min.js"></script>
     <script src="js/custom.min.js"></script>
     <script src="assets/js/charts.js"></script>
    <script src="js/permissions.js"></script>
    <script src="plugins/bower_components/sweetalert/sweetalert.min.js"></script>
    <script src="plugins/bower_components/select2/js/select2.min.js"></script>
       <!-- Date Picker Plugin JavaScript -->
    <script src="plugins/bower_components/switchery/dist/switchery.min.js"></script>
    <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  

      <script>


        jQuery('#myTable').dataTable();
        $('#myTables').DataTable();


        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
          $('.js-switch').each(function() {
              new Switchery($(this)[0], $(this).data());
          });


           // Date Picker
    
   jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });


   function pieChart() {
             var chartdata = {
                 datasets: [{
                     data: [
                     <?php echo just_count_anything('clients') ?>, 
                     <?php echo just_count_anything('users') ?>, 
                     <?php echo just_count_anything('loan_list') ?>,                      
                     ],                    
                     backgroundColor: ['#88766a', '#ff0000','#009009'],                     
                 }],
                 labels: ['clients', 'Users','Loan Applications'],
             }
             var graphTarget = $("#pieC");
             var barGraph = new Chart(graphTarget, {
                 type: 'pie',
                 data: chartdata,                 
             });
         }
       
 pieChart();
    
     function sendMail(orderId) {
          var orderId = orderId;
          $.ajax({
               type: "POST",
               url: "php_action/send_email.php?t=true&id=" + orderId,
               data: { orderId: orderId },
               dataType: "json",
               success: function(result) {

                    if (result.status) {
                         swal({
                              title: "Good job!",
                              padding: 20,
                              text: "Good Job! Email Sent Successfully!",
                              type: "success"
                         });

                         setTimeout(function() {
                              window.location.reload();
                         }, 3000);
                    } else {
                         swal({
                              title: "Oops!",
                              padding: 20,
                              text: result.msg + "..please try again!",
                              type: "warning"
                         });
                    }

               }
          });
     }


   

     function ConfirmLoanApproval(id,ref) {
          var id = id;
          var ref = ref;
          swal({
               title: "Are you sure?",
               text: "Okay to approve this Loan",
               type: "info",
               padding: 20,             
              showCancelButton: true,   
              confirmButtonColor: "#384888",   
              confirmButtonText: "Yes, Approve!",   
              cancelButtonText: "No, cancel plx!",   
              closeOnConfirm: false,   
              closeOnCancel: false 
          }, function(isConfirm) {
               if (isConfirm) {
                    $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_plans.php?t=approve&ref="+ref,                     
                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Loan Approved successfully!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 3000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
               }
          });
     }





     function deleteClient(id) {
          var id = id;
          swal({
               title: "Are you sure?",
               text: "Okay to delete this Client",
               type: "info",
               padding: 20,             
              showCancelButton: true,   
              confirmButtonColor: "#384888",   
              confirmButtonText: "Yes, Delete!",   
              cancelButtonText: "No, cancel plx!",   
              closeOnConfirm: false,   
              closeOnCancel: false 
          }, function(isConfirm) {
               if (isConfirm) {
                    $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_clients.php?t=delete&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait a Client has been deleted!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 3000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
               }
          });
     }

  

     

     
     
     //user section

     //deleting User


       function deleteUser(id) {
          var id = id;
          swal({
               title: "Are you sure?",
               text: "Okay to delete this System User",
               type: "info",
               padding: 20,             
              showCancelButton: true,   
              confirmButtonColor: "#384888",   
              confirmButtonText: "Yes, Delete!",   
              cancelButtonText: "No, cancel plx!",   
              closeOnConfirm: false,   
              closeOnCancel: false 
          }, function(isConfirm) {
               if (isConfirm) {
                    $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_users.php?t=delete&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait a System User has been deleted!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 3000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
               }
          });
     }


     //changing username

     $(document).on("submit", "#changeUsername", function(e) {
          e.preventDefault();

          var formData = new FormData(this);

          swal({
               title: "Are you sure?",
               text: "Okay to change Username",
               type: "info",
               padding: 20,             
              showCancelButton: true,   
              confirmButtonColor: "#384888",   
              confirmButtonText: "Yes, add it!",   
              cancelButtonText: "No, cancel plx!",   
              closeOnConfirm: false,   
              closeOnCancel: false 
          }, function(isConfirm){
               if (isConfirm) {
                    $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/change_username.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! Username Changed Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 3000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
               }
          });
     });


       //adding permission

          $(document).on("submit", "#addPermissionForm", function(e) {
          e.preventDefault();

          var formData = new FormData(this);

          swal({
               title: "Are you sure?",
               text: "Okay to Add Permission",
               type: "info",
               padding: 20,             
              showCancelButton: true,   
              confirmButtonColor: "#444323",   
              confirmButtonText: "Yes, add it!",   
              cancelButtonText: "No, cancel plx!",   
              closeOnConfirm: false,   
              closeOnCancel: false 
          }, function(isConfirm){
               if (isConfirm) {
                    $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_permission.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! Permission group Added Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 3000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
               }
          });
     });

      //Adding Role

     $(document).on("submit", "#add_user_role", function(e) {
          e.preventDefault();

          var formData = new FormData(this);

          swal({
               title: "Are you sure?",
               text: "Okay to Add Role",
               type: "info",
               padding: 20,             
              showCancelButton: true,   
              confirmButtonColor: "#384888",   
              confirmButtonText: "Yes, add it!",   
              cancelButtonText: "No, cancel plx!",   
              closeOnConfirm: false,   
              closeOnCancel: false 
          }, function(isConfirm){
               if (isConfirm) {
                    $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_user_role.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! Role Added Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 3000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
               }
          });
     });

     //edit role

     $(document).on("submit", ".editRoleForm", function(e) {
          e.preventDefault();

          var formData = new FormData(this);

          swal({
               title: "Are you sure?",
               text: "Okay to Add Role",
               type: "info",
               padding: 20,             
              showCancelButton: true,   
              confirmButtonColor: "#384888",   
              confirmButtonText: "Yes, Update it!",   
              cancelButtonText: "No, cancel plx!",   
              closeOnConfirm: false,   
              closeOnCancel: false 
          }, function(isConfirm){
               if (isConfirm) {
                    $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_user_role.php?t=edit",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! Role Updated Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 3000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
               }
          });
     });




     //change password

     $(document).on("submit", "#changePassword", function(e) {
          e.preventDefault();

          var formData = new FormData(this);

          swal({
               title: "Are you sure?",
               text: "Okay to change Your Password",
               type: "info",
               padding: 20,
               showCancelButton: true,
               confirmButtonText: "Yes",
               cancelButtonText: "No, cancel!",
               confirmButtonClass: "btn btn-success",
               cancelButtonClass: "btn btn-danger m-l-10",
               buttonsStyling: false
          },function(isConfirm){
               if (isConfirm) {
                    $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/change_password.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! Password Changed Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 3000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
               }
          });
     });

     //addUserForm

   

       function deleteRole(id) {
          var id = id;
          swal({
               title: "Are you sure?",
               text: "Okay to delete this Role",
               type: "info",
               padding: 20,             
              showCancelButton: true,   
              confirmButtonColor: "#384888",   
              confirmButtonText: "Yes, delete it!",   
              cancelButtonText: "No, cancel plx!",   
              closeOnConfirm: false,   
              closeOnCancel: false 
          }, function(isConfirm) {
               if (isConfirm) {
                    $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_user_role.php?t=delete&id=" + id,

                         success: function(result) {
                             
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait a User Role has been deleted!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 3000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
               }
          });
     }


     //make admin

     function makeAdmin(id) {
          var id = id;
          swal({
               title: "Are you sure?",
               text: "Okay to Make this User Admin",
               type: "info",
               padding: 20,
               showCancelButton: true,
               confirmButtonText: "Yes",
               cancelButtonText: "No, cancel!",
               confirmButtonClass: "btn btn-success",
               cancelButtonClass: "btn btn-danger m-l-10",
               buttonsStyling: false
          },function(isConfirm) {
               if (isConfirm) {
                    $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_users.php?t=admin&id=" + id,

                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait, a User has been changed to Administrator!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 3000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
               }
          });
     }

     //revoke admin

     function revokeAdmin(id) {
          var id = id;
          swal({
               title: "Are you sure?",
               text: "Okay to Revoke Administration Access!",
               type: "info",
               padding: 20,
               showCancelButton: true,
               confirmButtonText: "Yes",
               cancelButtonText: "No, cancel!",
               confirmButtonClass: "btn btn-success",
               cancelButtonClass: "btn btn-danger m-l-10",
               buttonsStyling: false
          }).then(willSave => {
               if (willSave) {
                    $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_users.php?t=notadmin&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait, Administration Previlages have been removed Successifully!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 3000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
               }
          });
     }

     //deactivate account
  function deactivateAccount(id) {
          var id = id;
          swal({
               title: "Are you sure?",
               text: "Okay to Deactivate Account!",
               type: "info",
               padding: 20,
               showCancelButton: true,
               confirmButtonText: "Yes",
               cancelButtonText: "No, cancel!",
               confirmButtonClass: "btn btn-success",
               cancelButtonClass: "btn btn-danger m-l-10",
               buttonsStyling: false
          },function(isConfirm){
               if (isConfirm) {
                    $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_users.php?t=deactivateAccount&id=" + id,

                         success: function(result) {
                             
                              if (result.status) {
                                   alert('While you wait, Administration Previlages have been removed Successifully!!');

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 3000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
               }
          });
     }



//permission


function deactivateClientAccount(id) {
          var id = id;
          swal({
               title: "Are you sure?",
               text: "Okay to Deactivate this Account!",
               type: "info",
               padding: 20,
               showCancelButton: true,
               confirmButtonText: "Yes",
               cancelButtonText: "No, cancel!",
               confirmButtonClass: "btn btn-success",
               cancelButtonClass: "btn btn-danger m-l-10",
               buttonsStyling: false
          },function(isConfirm){
               if (isConfirm) {
                    $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_clients.php?t=deactivateAccount&id=" + id,

                         success: function(result) {
                             
                              if (result.status) {
                                   alert('While you wait, account has been deactivated Successifully!!');

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 3000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
               }
          });
     }

   
 function activateClientAccount(id) {
          var id = id;
          swal({
               title: "Are you sure?",
               text: "Okay to Activate Account!",
               type: "info",
               padding: 20,
               showCancelButton: true,
               confirmButtonText: "Yes",
               cancelButtonText: "No, cancel!",
               confirmButtonClass: "btn btn-success",
               cancelButtonClass: "btn btn-danger m-l-10",
               buttonsStyling: false
          },function(isConfirm){
               if (isConfirm) {
                    $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_clients.php?t=activateAccount&id=" + id,

                         success: function(result) {
                             
                              if (result.status) {
                                   alert('While you wait, this account has been activated Successifully!!');

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 3000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
               }
          });
     }

     
     //remove order

     $("#orderDate").datepicker();
     $("#end_date").datepicker();
     $("#start_date").datepicker();

     //permissions


      














     </script>

    <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>
