<?php  $user_role = $_SESSION['user_role']; ?>



        <div class="navbar-default sidebar" role="navigation" style=" background: #323232;" >
            <div class="sidebar-nav slimscrollsidebar">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu"> ADMIN</span></h3> </div>
                <ul class="nav" id="side-menu">
              
                     <li><a href="indexMain.php" class="waves-effect"><i class="fa fa-dashboard fa-fw"></i> <span class="hide-menu">Dashboard</span></a></li>
                  
                   
               
                   <li> <a href="suppliers.php" class="waves-effect "><i class="fa fa-users fa-fw" data-icon="v"></i> <span class="hide-menu"> Clients <span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="create_client.php"><i class="fa fa-plus fa-fw"></i><span class="hide-menu">Create Client</span></a> </li>
                            
                              <li> <a href="suppliers.php"><i class="fa fa-plus-circle fa-fw"></i><span class="hide-menu">View all Clients</span></a> </li>
                        </ul>
                    </li>

              
                   <li> <a href="users.php" class="waves-effect "><i class="fa fa-users fa-fw" data-icon="v"></i> <span class="hide-menu"> System Users <span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="add_user.php"><i class="fa fa-plus fa-fw"></i><span class="hide-menu">Create User</span></a> </li>
                            
                              <li> <a href="users.php"><i class="fa fa-plus-circle fa-fw"></i><span class="hide-menu">View all</span></a> </li>
                        </ul>
                    </li>

                  
                     
                    <li class="devider"></li>
                     <li><a href="#" class="waves-effect"><i class="ti-bar-chart fa-fw"></i> <span class="hide-menu">Reports</span></a></li>
                
                     <li class="devider"></li>
                    <li><a href="c_logout.php" class="waves-effect"><i class="fa fa-power-off fa-fw"></i> <span class="hide-menu">Log out</span></a></li>

                   
                           
                    
                </ul>
            </div>
        </div>

   



     