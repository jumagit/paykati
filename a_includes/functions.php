<?php

//include ('../includes/db.php');

require_once 'db.php';


function fetch_user_role_name($role){
    $result = query("SELECT * FROM user_roles WHERE id= '".$role."'");   
    $data = mysqli_fetch_array($result);
    return $data['role_name'];
}



function get_user_info($username)
{
    $sql = "SELECT sname, fname, email FROM users WHERE account_status=1 AND username='$username'";
    $result = query($sql);
    $data = array();
    while ($row = mysqli_fetch_array($result)) {
        $data[] = $row;
    }
    return $data;
}

//fetch user function
function fetch_users()
{

    $sql = "SELECT user_id,sname,role,fname, email,mobile,gender,account_status, created_by, created_on FROM users ";
    $result = query($sql);
    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

            $count++;
            $user_id = $trailRow['user_id'];
            $sname = $trailRow['sname'];
            $fname = $trailRow['fname'];
            $role = $trailRow['role'];
            $created_by = $trailRow['created_by'];
            $created_on = $trailRow['created_on'];
            $email = $trailRow['email'];
            $mobile = $trailRow['mobile'];
            $account_status = $trailRow['account_status'];
            $created_on = date_format(date_create($trailRow['created_on']), ' l jS F Y');

            if ($account_status == 1) {

                $accountStatus = "Active";

            } else {

                $accountStatus = "Inactive";
            }



            //role

            if ($role == 1) {

                $role = "Administrator";

            } else {

                $role = "User";
            }


            $output .= "<tr>
               <td>{$count}</td>
               <td>{$sname} {$fname}</td>
              
               <td>{$mobile}</td>
               <td>{$accountStatus}</td>  
               <td>{$role}</td>         
               <td><a onclick='makeAdmin($user_id)' class='btn btn-xs btn-info'> Make Admin  </a></td>
               <td><a onclick='revokeAdmin($user_id)' class='btn btn-success btn-xs'> Revoke Admin </a></td>
               <td><a onclick='deactivateAccount($user_id)' class='btn btn-xs btn-default'><i class='fa fa-times-circle'></i>  Deactivate  </a></td>
               <td><a href='edit_users.php?edit={$user_id}' class='btn btn-xs btn-info' ><i class='fa fa-edit'></i></a></td>
               <td><a onclick='deleteUser($user_id)' class='btn  btn-xs btn-danger'><i class='fa fa-trash'></i></a></td>

               </tr>";

        }


    return $output;


    }

}

//fetch log function

function fetch_logs()
{

    $sql = "SELECT * FROM system_access_logs ORDER BY id DESC";

    $result = query($sql);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

            $count++;
            $user = $trailRow['username'];
            $activityTime = $trailRow['activity_time'];
            $log_type = $trailRow['log_type'];
            $activity = $trailRow['activity'];

            $output .= "<tr>
           <td>{$count}</td>
           <td>{$user}</td>
           <td>{$activity}</td>
           <td>{$activityTime}</td>
           <td>{$log_type}</td>

           </tr>";

        }


    return $output;

    }

}

//fetch roles function

function fetch_roles()
{

    $sql = "SELECT * FROM user_roles ORDER BY id DESC";

    $result = query($sql);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

          $id = $trailRow['id'];

            $count++;
            $user_role = $trailRow['role_name'];
           

            $output .= "<tr>
           <td>{$count}</td>
           <td>{$user_role}</td>
           <td><button type='button' class='btn btn-info' data-toggle='modal' data-target='#exampleModal-{$id}' data-whatever='@fa'><i class='fa fa-edit'></i> Edit</button></td>
           
             <div class='modal fade' id='exampleModal-{$id}' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel1'>
                                <div class='modal-dialog' role='document'>
                                    <div class='modal-content'>
                                        <div class='modal-header alert-danger'>
                                            <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                            <h4 class='modal-title' id='exampleModalLabel1'> <i class='fa fa-edit'></i> Edit Role</h4> </div>
                                        <div class='modal-body'>
                                            <form method='POST' action='#' class='editRoleForm'>
                                                <div class='form-group'>
                                                    <label for='recipient-name' class='control-label'>Name:</label>
                                                     <input type='hidden' class='form-control' value='{$id}' name='id'>
                                                    <input type='text' class='form-control' value='{$user_role}' name='role_name'>
                                               </div>  
                                        </div>
                                        <div class='modal-footer'>
                                            <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                                            <button type='submit' class='btn btn-primary'> Edit </button>
                                        </div>
                                           </form>
                                    </div>
                                </div>
                            </div>
           <td><a onclick='deleteRole($id)' class='btn btn-danger'><i class='fa fa-trash'></i> Trash </a></td>

          
          

           </tr>";

        }

          return $output;

    }

  
}


//fetch clients
function fetch_clients()
{

    $sql = "SELECT * FROM clients ORDER BY client_id DESC";

    $result = query($sql);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

            $count++;
            $client_id = $trailRow['client_id'];
            $fullName = $trailRow['fullName'];
            $email = $trailRow['email'];
            $username = $trailRow['username'];
            $mobile = $trailRow['mobile'];
            $location = $trailRow['location'];
            $account_status = $trailRow['account_status'];
            if($account_status === '1'){
                $account_status = '<button class="btn btn-success btn-sm">Active</button>';
            }else if($account_status === '0'){
              $account_status = '<button class="btn btn-danger btn-sm">Not Active</button>';  
            }else{}
           $masked_id = base64_encode($client_id);

            $output .= "<tr>
           <td>{$count}</td>
           <td>{$fullName}</td>          
           <td>{$username}</td> 
           <td>{$mobile}</td> 
           <td>{$location}</td>
           <td>{$account_status}</td>
           <td><a   href='edit_supplier.php?edit={$masked_id}' ><i class='fa fa-edit fa-2x'></i> </a></td>
           <td><a   onclick='deactivateClientAccount($client_id)' class='text-info'><i class='fa fa-cogs fa-2x'></i> </a>
           </td>
           <td><a   onclick='activateClientAccount($client_id)' class='text-success'><i class='fa fa-plus fa-2x'></i> </a>
           </td>

           <td><a   onclick='deleteClient($client_id)' class='text-danger'><i class='fa fa-trash fa-2x'></i> </a>
           </td>
           </tr>";

        }
        
    return $output;

    }

}


function fetch_clients_on_dashboard()
{

    $sql = "SELECT * FROM clients ORDER BY client_id DESC";

    $result = query($sql);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

            $count++;
            $client_id = $trailRow['client_id'];
            $fullName = $trailRow['fullName'];
            $email = $trailRow['email'];
            $username = $trailRow['username'];
            $mobile = $trailRow['mobile'];
            $location = $trailRow['location'];
           $masked_id = base64_encode($client_id);

            $output .= "<tr>
           <td>{$count}</td>
           <td>{$fullName}</td>          
           <td>{$username}</td> 
           <td>{$mobile}</td>       
           <td>{$location}</td>
          
           </tr>";

        }
        
    return $output;

    }

}



