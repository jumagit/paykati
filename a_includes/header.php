
<?php session_start(); ?>
<?php  if(!isset($_SESSION['user_id'])){ header('Location:index.php'); exit;} ?>

<?php include "functions.php"; ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/logo.png">
    <title>ONLINE BANKING</title>
    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <link href="plugins/bower_components/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="plugins/bower_components/chartist-js/dist/chartist-init.css" rel="stylesheet">
  
     <link href="plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
      <link href="plugins/bower_components/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <!-- animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
      <link href="plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
       <!-- Date picker plugins css -->
    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
     <link href="plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet"  type="text/css"  />
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="css/colors/megna-dark.css" id="theme" rel="stylesheet">
  
     <link href="plugins/bower_components/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

     <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro:wght@200;300;400;500;700&display=swap" rel="stylesheet">


   <!--  script -->
  <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
   


   <!-- scripts end -->
  
</head>

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>

<style>
body{
  
  font-family: 'Source Code Pro', monospace;
font-weight: 700;
outline: none;
border-radius: 0px;
font-size: 14px;

}

#page-wrapper{
  background:#f1f2f7;
}


input,textarea{
    border:1px solid #000 !important;
}
</style>