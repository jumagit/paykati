<?php
ob_start();
system('ipconfig /all');
$mycom = ob_get_contents();
ob_clean();
$find_mac = "Physical"; 
$pmac = strpos($mycom, $find_mac);
session_start();
include "includes/db.php";
if (isset($_POST['login'])) {

    $username = clean($_POST['username']);
    $password = clean($_POST['password']);
    #$compName = gethostbyaddr($_SERVER['REMOTE_ADDR']);
    $computer = substr($mycom, ($pmac + 36), 17);

    if (!empty($username) and !empty($password)) {

        //fetching credentials from the database

        $sql = "SELECT * FROM users WHERE username = '$username'";
        $result = query($sql);
        if (mysqli_num_rows($result) == 1) {
            $hashed_password = md5($password);
            $mainSql = "SELECT * FROM users WHERE username = '$username' AND password = '$hashed_password'";
            $result2 = query($mainSql);
            if (mysqli_num_rows($result2) == 1) {
                while ($row = mysqli_fetch_array($result2)) {

                  //fetching the user role

                 
                    $accountStatus = $row['account_status'];
                    session_regenerate_id();
                    $_SESSION['user_role']  = $row['role'];
                    $_SESSION['username'] = $row['username'];
                    $_SESSION['user_id'] = $row['user_id'];
                    $_SESSION['computerName'] = $computer;
                    $_SESSION['adminFullName'] = $row['sname'] . '' . $row['fname'];
                    $_SESSION['email'] = $row['email'];
                    $_SESSION['accountType'] = $row['accountType'];
                    $_SESSION['mobile'] = $row['mobile'];
                       $_SESSION['gender'] = $row['gender'];
                    session_write_close();

                    if ($accountStatus = 1) {

                        if (writeLog("logged in from {$IP}", $_SESSION['username'], "SUCCESS")) {
                            $doLoginFeedback = "<div class=' text-center ' >                       
                         <img src='assets/images/35.gif'  width='30px'> <strong>Signing you in ...
                        <script type='text/javascript'>setTimeout(function() { window.location.href = 'indexMain.php';}, 3000);</script>
                        </div>";
                        }

                    } else {

                        $doLoginFeedback = "<div class='alert alert-danger bg-danger message' >                       
                        <p class='text-white font-weight-bold'> <strong> <i class='mdi mdi-close-circle'></i> Sorry!</strong>, Check your Password or Username Used.</p>
                       <script type='text/javascript'>setTimeout(function() { window.location.href = 'index1.php';}, 3000);</script>
                       </div>";

                    }

                }

            } else {

                $doLoginFeedback = "<div class='alert alert-warning ' >                       
                <p class='text-white font-weight-bold'> <strong> <i class='mdi mdi-close-circle'></i> Sorry!</strong>,
                 Incorrect Password Entered, Try Again!</p>
               <script type='text/javascript'>setTimeout(function() { window.location.href = 'index1.php';}, 3000);</script>
               </div>";

            }

        } else {

            $doLoginFeedback = "<div class='alert  alert-danger ' >                       
            <p class='text-white font-weight-bold'>
             <strong> <i class='mdi mdi-close-circle'></i> Sorry!</strong>,User Not Found In the System.</p>
           <script type='text/javascript'>setTimeout(function() { window.location.href = 'index1.php';}, 3000);</script>
           </div>";
        }

    }

}

?>
<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
<title>ONLINE BANKING</title>
<!-- Bootstrap Core CSS -->
<link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="css/colors/default.css" id="theme"  rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro:wght@200;300;400;500;700&display=swap" rel="stylesheet">



</head>

<style>
     body{
         font-family: 'Source Code Pro', monospace;
font-weight: 700;
     }
    .btn{
        background-color: #083666;
        outline: none;
        border-radius: 0px;
    }

    .white-box{
        padding: 30px;
        background-color: #fff;
        border-radius: 13px;
        border: 1px solid #26AFC5 ;

    }
    #wrapper{
        background: #d6d6d6 !important;
       
       
    }
</style>


<body>

<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="new-login-register">
      <div class="lg-info-panel">
              <div class="inner-panel" style="background: url(assets/images/bg.jpg);background-position: center;">
                  <a href="javascript:void(0)" class="p-20 di"><img src="assets/images/fair.png"></a>
                  <div class="lg-content">
                      <h2>Welcome to Online Banking</h2>
                      <p class="text-muted">“Do not save what is left after spending; instead spend what is left after saving.”
--Warren Buffett</p>
                      
                  </div>
              </div>
      </div>
      <div class="new-login-box">
                <div class="white-box">
                    <center><h3 class="box-title m-b-0">Welcome to PayKati Quick Banking</h3>
                    <small>Enter your login details below</small></center>
       <div class="row">
     
   
                   
                   
                     <?php if(isset($doLoginFeedback)){ echo $doLoginFeedback;}?>
                   <form class="form-horizontal " action="index1.php" method="POST">
                    
                    <div class="form-group  m-t-20">
                      <div class="col-xs-12">
                        <label>Username</label>
                        <input class="form-control" style="border:1px solid lightblue;" type="text" required="" name="username" placeholder="Username">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-xs-12">
                        <label>Password</label>
                        <input class="form-control" style="border:1px solid lightblue;" type="password" required="" name="password" placeholder="Password">
                      </div>
                    </div>
                       
                        
                    
                    
                    <div class="form-group">
                      <div class="col-md-12">
                        
                        <a href="forgot.php"  style="font-weight: normal;" id="to-recover" class="text-info pull-left"><i class="fa fa-lock m-l-5"></i> Forgot pwd? Reset From Here.</a> </div>
                    </div>
                    <div class="form-group m-l-10 m-t-20">
                    
                        <button class="btn btn-info  waves-effect waves-light" name="login" type="submit">Log In As Admin</button>
                     
                    </div>
                   
                  
                  </form>
                 
                </div>
     
     </div>           
  
  
</section>
<!-- jQuery -->
<script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/custom.min.js"></script>
<!--Style Switcher -->
<script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

<script>
  function myFunction() {
  var x = document.getElementById("myInput");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
</script>
</body>
</html>
