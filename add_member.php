<?php  include "mega_a_permission.php"; ?>
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title"> ADD MEMBER</h4>
            </div>

            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">

            <div class="col-md-12">
                <div class="white-box">

                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body ">
                           

                            <form action="" id="submitAgentForm" method="POST">

                                <div class="row">


                                    <div class="col-md-12">



                                        <div class="form-group ">
                                            <label for="fullName">FullNames :</label>
                                            <input type="text" name="fullName" class="form-control" parsley-type="text"
                                                required placeholder="Enter FullNames">
                                        </div>

                                        <div class="form-group ">
                                            <label for="email">Email :</label>
                                            <input type="email" name="email" class="form-control" parsley-type="email"
                                                required placeholder="Enter Email Address">
                                        </div>
                                        <div class="form-group ">
                                            <label for="location">Location :</label>
                                            <input type="text" name="location" class="form-control" parsley-type="text"
                                                required placeholder="Enter Agent Address">
                                        </div>
                                        <div class="form-group ">
                                            <label for="number_plate">Number Plate :</label>
                                            <input type="text" name="number_plate" class="form-control"
                                                parsley-type="text" required placeholder="Enter Agent Number Plate">
                                        </div>



                                   

                                        <div class="form-group ">
                                            <label for="role">Role</label>
                                            <select name="role" class="form-control">

                                                <option value="#" selected="selected">--Select Role --</option>


                                                <?php $sql = query("SELECT id, role_name FROM client_roles " );

                                                        while ($row = mysqli_fetch_array($sql)) {?>

                                                <option value="<?php echo $row[0]; ?>"><?php echo $row[1]; ?></option>

                                                <?php } ?>
                                            </select>
                                        </div>


                                        <div class="form-group ">
                                            <label for="username">Username :</label>
                                            <input type="text" name="username" class="form-control" parsley-type="email"
                                                required placeholder="Enter Username">
                                        </div>

                                        <div class="form-group ">
                                            <label for="contact">Contact :</label>
                                            <input type="text" name="contact" class="form-control" parsley-type="text"
                                                required placeholder="Enter Mobile Number">
                                        </div>

                                        <div class="form-group ">
                                            <label for="profileImage">Profile Image :</label>
                                            <input type="file" name="profileImage" class="form-control">
                                        </div>


                                    </div>

                                </div>


                        </div>

                      

                            <div class="form-group">

                                <button type="submit" class="btn a-button-primary btn-block waves-effect waves-light">
                                    <i class="mdi mdi-content-save"></i> Add Member
                                </button>

                        </div>



                        </form>
                    </div>
                </div>
            </div>

         </div>

     </div>
        </div>

    </div>
    <?php   include "includes/footer.php"; ?>