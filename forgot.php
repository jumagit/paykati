<?php
#Turn on output buffering
ob_start();
#Get the ipconfig details using system command
system('ipconfig /all');

#Capture the output into a variable
$mycom=ob_get_contents();
#Clean (erase) the output buffer
ob_clean();

$findme = "Physical";
#Search the "Physical" | Find the position of Physical text
$pmac = strpos($mycom, $findme);

# Get Physical Address
#$comp=substr($mycom,($pmac+36),17);
#Display Mac Address
#echo '<h1 style="color: #ff0000;">'.$comp.'</h1>';
?>
<?php
#Start session
session_start();

#Include database connection details
if(isset($_POST['request']))
{
    include('includes/db.php');

    $username = $_POST['username'];
    $token = bin2hex(openssl_random_pseudo_bytes(16));

    $sql1 = "SELECT * FROM clients WHERE username = '$username'";
    $result = query($sql1);
    if(mysqli_num_rows($result) === 1)
    {
        while ($row = mysqli_fetch_array($result))
        {
            $email = $row['email'];
            $fullNames = $row['fullName'];
        }
        if(!empty($email))
        {
            $subject = "Password Reset Token";

            $body = "
            <p>&nbsp;</p>
            <p>Dear {$fullNames},</p>
            <p>This email was sent automatically by the OMEGA Stores in response to your request to reset your password.</p> 
            <p>To reset your password, either click  or copy and paste the next link into the address bar of web browser.</p>
            <p><a href='".URL."reset_password.php?token={$token}'>".URL."reset_password.php?token={$token}</a></p>
            <p>If you didn't initiate this password reset request, please ignore this email. Your password won't be changed.</p>
            <p>&nbsp;</p>
            <p>Thank you</p>
            <p><small>Please don't reply this email !</small></p>
            <p>&nbsp;</p>";


            $SQL2 = "SELECT * FROM password_resets WHERE username='{$username}'";
            $query = query($SQL2);
            if((mysqli_num_rows($query) === 1))
            {
                $deleteKey3 = "DELETE FROM password_resets WHERE username='{$username}'";
                if(!(query($deleteKey3)))
                {
                    die(mysqli_error());
                }
                else
                {
                    $SQL3 = "INSERT INTO password_resets(reset_key,username)VALUES('$token','$username')";
                    if(!query($SQL3))
                    {
                        die(mysqli_error());
                    }
                    else
                    {
                        $mail =  sendMailFront($email,$subject,$body);
                        if(!$mail) // Dont forget to add "!" condition
                        {
                            $feedback = 'Failed';
                        }
                        else
                        {
                            $feedback = "Your password request was successful. Please check your email <strong>{$email}</strong>, for further instructions";
                        }
                    }
                }
            }
            else
            {
                $SQL3 = "INSERT INTO password_resets(reset_key,username)VALUES('{$token}','{$username}')";
                if(!query($SQL3))
                {
                    die(mysqli_error());
                }
                else
                {
                   $mail =  sendMailFront($email,$subject,$body);
                    if(!$mail) // Dont forget to add "!" condition
                    {
                        $feedback = "Fail";
                    }
                    else
                    {
                        $feedback = "Your password request was successful. Please check your email <strong>{$email}</strong>, for further instructions";
                    }
                }
            }
        }
        else
        {
            die("<div style='color: #a94442;background-color: #f2dede;border-color: #ebccd1;border: 1px solid transparent;font-family:Century Gothic; width:60%; margin:auto;padding:15px 25px;'><p><strong>ERROR: </strong></p><p>Sorry {$fullNames},</p> <p>We failed to send you a password reset link because your account has no defined Contact Email.</p> <p>Please contact the system administrator for a manual password reset !</p></div>");
        }


    }


}
?>

<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
<title>Omega</title>
<!-- Bootstrap Core CSS -->
<link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="css/colors/default.css" id="theme"  rel="stylesheet">


</head>

<style>
  


    .medd{
     border-radius: 5px;
     background: #ffffff !important;
     margin: 20px;
     padding: 20px;
      box-shadow:  4px 4px 10px #d9d9d9; 
    
    }


    .med{
     border-radius: 5px;
     background: #ffffff !important;
     margin: 20px;
     padding: 20px;
      
    
    }

</style>
<body>

<!-- <section id="wrapper" class="new-login-register " style="padding-top: 70px;background: url('assets/images/background.png') no-repeat;background-size: cover"> -->
   <section id="wrapper" class="new-login-register " style="padding-top: 70px;background-color: silver">
      <div class=" col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4  col-sm-4 col-sm-offset-4 " >
                <div class=" med" style="border:2px solid lightblue;" >


                      

                    <h3 class="text-center m-0">
                        <a href="index.php" class="logo logo-admin"><img src="assets/images/logo.png" height="30" alt="logo"></a>
                    </h3> <br>
                    <?php if(isset($feedback))
                     {?>
                
                <div class="alert alert-info" role="alert">
                  <h4 class="alert-heading">Password Reset Request Successful !</h4>
                  <p><?php echo $feedback; ?></p>
                </div>

            <?php }else{?>
                <div class="medd">
                  <h4 class="alert-heading">Don't Worry!</h4>
                  <p> Just type the username you use for login and we will send you the reset link to your email address we have on file.</p>
                </div>

            <?php }?>

                
                        <h3 class="box-title m-b-1 text-center">RESET PASSWORD</h3>
                       

                        <form class="form-horizontal" method="POST" action="">

                            <div class="form-group  m-t-10">
                      <div class="col-xs-12">
                      
                              
                                <input type="text" class="form-control"  name="username" id="username" placeholder="Enter Your Username"  style="border:1px solid lightblue;">
                            </div>
                     
                    </div>

                            

                             <button class="btn btn-danger  btn-block" type="submit" name="request">Request Reset Link</button>

                             <p class="text-left   m-t-10 m-b-10 ">
                                <a href="index.php" id="to-recover" class="text-info  pull-left"><i class="fa fa-times  m-r-5"></i> <b>Cancel</b></a>

                             </p><br>

                              

                            

                        </form>
                   

               
            </div>
                
                 
               
      </div>            
  
  
</section>
<!-- jQuery -->
<script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/custom.min.js"></script>
<!--Style Switcher -->
<script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
