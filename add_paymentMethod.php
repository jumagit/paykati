<?php  include "includes/header.php"; ?>
<!-- ============================================================== -->
<?php  include "includes/topNav.php"; ?>

<!-- End Top Navigation -->

<?php  include "includes/sideNav.php"; ?>
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">ADD PAYMENT MODE</h4>
            </div>

            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">



            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-plus-circle"></i>ADD PAYMENT MODE </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <form action="#" method="POST" id="addMode" class="form-horizontal">
                                <div class="form-body">


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Payment Mode</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="mode" class="form-control"
                                                        placeholder="Payment Mode"> </div>
                                            </div>
                                        </div>
                                        <!--/span-->

                                        <!--/span-->
                                    </div>
                                    <!--/row-->


                                </div>
                                <hr class="m-t-0 m-b-10">
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-6"> </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">


                                                    <button type="submit" class="btn btn-primary"><i
                                                            class="fa fa-save"></i> Add Payment Mode</button>


                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>




            <!--./row-->
        </div>

    </div>
    <?php   include "includes/footer.php"; ?>