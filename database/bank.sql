-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 26, 2021 at 09:32 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `store`
--

-- --------------------------------------------------------

--
-- Table structure for table `accountability`
--

CREATE TABLE `accountability` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `withdraws` int(11) NOT NULL DEFAULT 0,
  `transfers` int(11) NOT NULL DEFAULT 0,
  `deposits` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accountability`
--

INSERT INTO `accountability` (`id`, `client_id`, `withdraws`, `transfers`, `deposits`) VALUES
(1, 1, 12900000, 320000, 4320000),
(2, 4, 50000000, 320000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `borrowers`
--

CREATE TABLE `borrowers` (
  `id` int(30) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `middlename` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `contact_no` varchar(30) NOT NULL,
  `address` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `tax_id` varchar(50) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `borrowers`
--

INSERT INTO `borrowers` (`id`, `firstname`, `middlename`, `lastname`, `contact_no`, `address`, `email`, `tax_id`, `date_created`) VALUES
(1, 'John', 'C', 'Smith', '+16554 454654', 'Sample address', 'jsmith@sample.com', '789845-23', 0);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `client_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `fullName` varchar(100) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `mobile` varchar(60) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(120) DEFAULT NULL,
  `account_status` int(11) DEFAULT 1,
  `client_role` int(12) DEFAULT 1,
  `cpassword` varchar(100) DEFAULT NULL,
  `profileImage` text DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_on` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`client_id`, `code`, `fullName`, `email`, `mobile`, `location`, `username`, `password`, `account_status`, `client_role`, `cpassword`, `profileImage`, `created_by`, `created_on`, `updated_on`) VALUES
(1, '0086', 'MUKOOVA JUMA', 'mukoovajumag8@gmail.com', '0702499649', 'Kyanja', 'jumagits', '8490cd7ddb56e7c7d5db02d06c71a933', 1, 1, '?XXA7t8Ck', NULL, '1', '2021-07-24 18:11:17', '2021-07-24 18:11:17'),
(2, '0086', 'MWEEBE CHRISTOPHER', 'juma@gmail.com', '0769994433', 'TRENDS OF LIFE ZANA', 'chris', '88903e6ce550fc0817d04f5239a7b32a', 1, 1, 'mA4DZ!9%m', NULL, 'MUKOOVA JUMA', '2021-07-25 08:38:29', '2021-07-25 08:38:29'),
(3, '0086', 'malaria roed', 'jumado@gmail.com', '0799565043', 'greodr', 'rendeso', '8aa20604559cf598b5ac855ce3e02d7e', 1, 1, 's3kCMhdy&', NULL, 'MUKOOVA JUMA', '2021-07-25 08:52:24', '2021-07-25 08:52:24'),
(4, '0086', 'ARINDA POROE', 'treva@gmail.com', '0769959544', 'RELOADS', 'rendor', '225d027c5d046efaccc6d559a22506c8', 1, 1, '!7wdWyHCe', NULL, 'MUKOOVA JUMA', '2021-07-25 08:56:29', '2021-07-25 08:56:29');

-- --------------------------------------------------------

--
-- Table structure for table `client_account`
--

CREATE TABLE `client_account` (
  `id` int(11) NOT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `mobile_money_number` varchar(200) NOT NULL,
  `balance` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_account`
--

INSERT INTO `client_account` (`id`, `account_name`, `account_number`, `mobile_money_number`, `balance`, `status`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 'juma', '68958554933', '07066447534', 89991220000, 1, 1, '2021-07-24 18:11:17', '2021-07-25 13:20:45'),
(2, 'christopher', '5494044953', '07066447534', 0, 1, 0, '2021-07-25 08:38:35', '2021-07-25 08:38:35'),
(3, 'jouds', '4454433424', '07959559490', 0, 1, 83, '2021-07-25 08:52:31', '2021-07-25 08:52:31'),
(4, 'jueelo', '6598579424', '07959559490', 489023107198, 1, 4, '2021-07-25 08:56:29', '2021-07-25 11:29:16');

-- --------------------------------------------------------

--
-- Table structure for table `client_permission`
--

CREATE TABLE `client_permission` (
  `permission_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `add_products` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client_revenues`
--

CREATE TABLE `client_revenues` (
  `id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `transaction_type` varchar(100) DEFAULT NULL,
  `type` varchar(25) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL COMMENT 'ID of the transaction the income amount e.g charge on withdraw',
  `primary_id` int(11) DEFAULT NULL COMMENT 'ID of the transaction the transacted amount e.g withdraw',
  `amount` int(11) DEFAULT NULL,
  `last_balance` bigint(20) DEFAULT NULL,
  `total` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client_roles`
--

CREATE TABLE `client_roles` (
  `id` int(11) NOT NULL,
  `role_name` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_roles`
--

INSERT INTO `client_roles` (`id`, `role_name`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '2020-08-26 11:03:59', '2020-08-26 11:03:59'),
(2, 'Agent', '2020-08-26 11:03:59', '2020-08-26 11:03:59');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL,
  `address` varchar(100) NOT NULL,
  `customer_names` varchar(100) NOT NULL,
  `contact` varchar(30) NOT NULL,
  `email_address` varchar(60) NOT NULL,
  `client_id` int(11) NOT NULL,
  `suppliercode` varchar(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `address`, `customer_names`, `contact`, `email_address`, `client_id`, `suppliercode`, `created_at`, `updated_at`) VALUES
(1, 'mayuge', 'Dhiyuga Latif', '0709898789', 'latif@gmail.com', 1, 'REY', '2020-11-03 05:28:21', '2020-11-03 05:28:21'),
(2, 'kenya', 'READY MODE', '0790797965', 'readymode@gmail.com', 1, '3498', '2021-07-24 07:58:48', '2021-07-24 07:58:48');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `invoice_id` int(11) NOT NULL,
  `invoice_no` int(11) DEFAULT NULL,
  `voucher` varchar(20) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `paid` int(11) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_ledgers`
--

CREATE TABLE `invoice_ledgers` (
  `ledger_id` int(11) NOT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `balance` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loan_list`
--

CREATE TABLE `loan_list` (
  `id` int(30) NOT NULL,
  `ref_no` varchar(50) NOT NULL,
  `loan_type_id` int(30) NOT NULL,
  `borrower_id` int(30) NOT NULL,
  `purpose` text NOT NULL,
  `amount` double NOT NULL,
  `plan_id` int(30) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0= request, 1= confrimed,2=released,3=complteted,4=denied\r\n',
  `date_released` datetime NOT NULL,
  `date_created` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `loan_list`
--

INSERT INTO `loan_list` (`id`, `ref_no`, `loan_type_id`, `borrower_id`, `purpose`, `amount`, `plan_id`, `status`, `date_released`, `date_created`) VALUES
(3, '81409630', 1, 1, 'Sample Only', 100000, 1, 2, '2020-09-26 09:06:00', '2020-09-26 15:06:29'),
(4, '834396', 1, 1, '65', 899, 1, 0, '0000-00-00 00:00:00', '2021-07-25 17:14:58'),
(5, '71516747', 0, 1, 'GOOD APPLICATION', 4000, 1, 0, '0000-00-00 00:00:00', '2021-07-26 07:51:49'),
(6, '10215989', 3, 1, 'GOOD APPLICATION', 4000, 1, 0, '2021-07-26 00:00:00', '2021-07-26 07:53:15'),
(7, '58309641', 3, 1, 'GOOD APPLICATION', 4000, 1, 0, '2021-07-26 00:00:00', '2021-07-26 07:54:11'),
(8, '6541222', 3, 1, 'school fees', 300000, 3, 0, '2021-07-26 00:00:00', '2021-07-26 08:29:02');

-- --------------------------------------------------------

--
-- Table structure for table `loan_plan`
--

CREATE TABLE `loan_plan` (
  `id` int(30) NOT NULL,
  `months` int(11) NOT NULL,
  `interest_percentage` float NOT NULL,
  `penalty_rate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `loan_plan`
--

INSERT INTO `loan_plan` (`id`, `months`, `interest_percentage`, `penalty_rate`) VALUES
(1, 36, 8, 3),
(2, 24, 5, 2),
(3, 27, 6, 2),
(4, 5, 0, 1),
(5, 40, 21, 8);

-- --------------------------------------------------------

--
-- Table structure for table `loan_schedules`
--

CREATE TABLE `loan_schedules` (
  `id` int(30) NOT NULL,
  `loan_id` int(30) NOT NULL,
  `date_due` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `loan_schedules`
--

INSERT INTO `loan_schedules` (`id`, `loan_id`, `date_due`) VALUES
(2, 3, '2020-10-26'),
(3, 3, '2020-11-26'),
(4, 3, '2020-12-26'),
(5, 3, '2021-01-26'),
(6, 3, '2021-02-26'),
(7, 3, '2021-03-26'),
(8, 3, '2021-04-26'),
(9, 3, '2021-05-26'),
(10, 3, '2021-06-26'),
(11, 3, '2021-07-26'),
(12, 3, '2021-08-26'),
(13, 3, '2021-09-26'),
(14, 3, '2021-10-26'),
(15, 3, '2021-11-26'),
(16, 3, '2021-12-26'),
(17, 3, '2022-01-26'),
(18, 3, '2022-02-26'),
(19, 3, '2022-03-26'),
(20, 3, '2022-04-26'),
(21, 3, '2022-05-26'),
(22, 3, '2022-06-26'),
(23, 3, '2022-07-26'),
(24, 3, '2022-08-26'),
(25, 3, '2022-09-26'),
(26, 3, '2022-10-26'),
(27, 3, '2022-11-26'),
(28, 3, '2022-12-26'),
(29, 3, '2023-01-26'),
(30, 3, '2023-02-26'),
(31, 3, '2023-03-26'),
(32, 3, '2023-04-26'),
(33, 3, '2023-05-26'),
(34, 3, '2023-06-26'),
(35, 3, '2023-07-26'),
(36, 3, '2023-08-26'),
(37, 3, '2023-09-26');

-- --------------------------------------------------------

--
-- Table structure for table `loan_types`
--

CREATE TABLE `loan_types` (
  `id` int(30) NOT NULL,
  `type_name` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `loan_types`
--

INSERT INTO `loan_types` (`id`, `type_name`, `description`) VALUES
(1, 'Small Business', 'Small Business Loans'),
(2, 'Mortgages', 'Mortgages'),
(3, 'Personal Loans', 'Personal Loans'),
(4, '', 'ggui'),
(5, 'huu', 'ggui'),
(6, 'Yoki sugu', 'WOE S');

-- --------------------------------------------------------

--
-- Table structure for table `mega_trans_logs`
--

CREATE TABLE `mega_trans_logs` (
  `id` int(11) NOT NULL,
  `account_id` varchar(20) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `trans_type` varchar(20) NOT NULL,
  `record_date` datetime NOT NULL,
  `trans_date` datetime NOT NULL,
  `trans_amount` double NOT NULL,
  `description` text NOT NULL,
  `member` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mega_trans_logs`
--

INSERT INTO `mega_trans_logs` (`id`, `account_id`, `customer_id`, `trans_type`, `record_date`, `trans_date`, `trans_amount`, `description`, `member`, `client_id`, `created_at`) VALUES
(1, '3409694883', 1, 'Deposit', '2021-07-24 15:07:07', '2021-07-24 15:07:07', 55000, 'Deposit of 55,000 to Account (3409694883) by bryan on  Saturday 24th July 2021', 0, 1, '2021-07-24 12:59:07'),
(2, '3409694883', 1, 'Deposit', '2021-07-24 15:07:50', '2021-07-24 15:07:50', 55000, 'Deposit of 55,000 to Account (3409694883) by bryan on  Saturday 24th July 2021', 0, 1, '2021-07-24 12:59:50'),
(3, '3409694883', 1, 'Deposit', '2021-07-24 16:07:03', '2021-07-24 16:07:03', 2000, 'Deposit of 2,000 to Account (3409694883) by bryan on  Saturday 24th July 2021', 0, 1, '2021-07-24 13:20:03'),
(4, '', 1, 'Deposit', '2021-07-24 21:07:53', '2021-07-24 21:07:53', 4000, 'Deposit of 4,000 to Account () by juma on  Saturday 24th July 2021', 0, 1, '2021-07-24 18:12:53'),
(5, '68958554933', 1, 'Deposit', '2021-07-25 10:07:07', '2021-07-25 10:07:07', 3200, 'Deposit of 3,200 to Account (68958554933) by juma on  Sunday 25th July 2021', 0, 1, '2021-07-25 07:41:07'),
(6, '68958554933', 1, 'Deposit', '2021-07-25 10:07:33', '2021-07-25 10:07:33', 50000, 'Deposit of 50,000 to Account (68958554933) by juma on  Sunday 25th July 2021', 0, 1, '2021-07-25 07:41:33'),
(7, '68958554933', 0, 'Withdraw', '2021-07-25 11:07:32', '2021-07-25 11:07:32', 6000000, 'Withdraw of 6,000,000 from  Account (68958554933) by MUKOOVA JUMA on  Sunday 25th July 2021', 1, 1, '2021-07-25 08:18:32'),
(8, '68958554933', 1, 'Deposit', '2021-07-25 11:07:24', '2021-07-25 11:07:24', 70000000, 'Deposit of 70,000,000 to Account (68958554933) by juma on  Sunday 25th July 2021', 0, 1, '2021-07-25 08:19:24'),
(9, '68958554933', 0, 'Withdraw', '2021-07-25 11:07:36', '2021-07-25 11:07:36', 70000, 'Withdraw of 70,000 from  Account (68958554933) by MUKOOVA JUMA on  Sunday 25th July 2021', 1, 1, '2021-07-25 08:27:36'),
(10, '6598579424', 4, 'Deposit', '2021-07-25 12:07:09', '2021-07-25 12:07:09', 203000, 'Deposit of 203,000 to Account (6598579424) by jueelo on  Sunday 25th July 2021', 0, 4, '2021-07-25 09:00:09'),
(11, '', 0, 'Withdraw', '2021-07-25 13:07:39', '2021-07-25 13:07:39', 0, 'Withdraw of 0 from  Account () by  on  Sunday 25th July 2021', 0, 4, '2021-07-25 10:05:39'),
(12, '', 0, 'Withdraw', '2021-07-25 13:07:53', '2021-07-25 13:07:53', 0, 'Withdraw of 0 from  Account () by  on  Sunday 25th July 2021', 0, 4, '2021-07-25 10:05:53'),
(13, '1', 0, 'Withdraw', '2021-07-25 13:07:34', '2021-07-25 13:07:34', 4300000, 'Transfer withdraw of <b>4,300,000</b>  from Account (<b>1</b>) by ARINDA POROE on 2021-07-25 13:07:34', 0, 4, '2021-07-25 10:13:34'),
(14, '', 0, 'deposit', '2021-07-25 13:07:34', '2021-07-25 13:07:34', 4300000, 'Transfer Deposit of <b>4,300,000</b>   Account (<b>1</b>) by ARINDA POROE on 2021-07-25 13:07:34', 0, 4, '2021-07-25 10:13:34'),
(15, '4', 0, 'Withdraw', '2021-07-25 13:07:59', '2021-07-25 13:07:59', 4300499, 'Transfer withdraw of <b>4,300,499</b>  from Account (<b>4</b>) by ARINDA POROE on 2021-07-25 13:07:59', 0, 4, '2021-07-25 10:14:59'),
(16, '', 0, 'deposit', '2021-07-25 13:07:59', '2021-07-25 13:07:59', 4300499, 'Transfer Deposit of <b>4,300,499</b>   Account (<b>4</b>) by ARINDA POROE on 2021-07-25 13:07:59', 0, 4, '2021-07-25 10:14:59'),
(17, '4', 0, 'Withdraw', '2021-07-25 13:07:17', '2021-07-25 13:07:17', 4300499, 'Transfer withdraw of <b>4,300,499</b>  from Account (<b>4</b>) by ARINDA POROE on 2021-07-25 13:07:17', 0, 4, '2021-07-25 10:16:17'),
(18, '', 0, 'deposit', '2021-07-25 13:07:17', '2021-07-25 13:07:17', 4300499, 'Transfer Deposit of <b>4,300,499</b>   Account (<b>4</b>) by ARINDA POROE on 2021-07-25 13:07:17', 0, 4, '2021-07-25 10:16:17'),
(19, '4', 0, 'Withdraw', '2021-07-25 13:07:58', '2021-07-25 13:07:58', 4300499, 'Transfer withdraw of <b>4,300,499</b>  from Account (<b>4</b>) by ARINDA POROE on 2021-07-25 13:07:58', 0, 4, '2021-07-25 10:18:58'),
(20, '', 0, 'deposit', '2021-07-25 13:07:58', '2021-07-25 13:07:58', 4300499, 'Transfer Deposit of <b>4,300,499</b>   Account (<b>4</b>) by ARINDA POROE on 2021-07-25 13:07:58', 0, 4, '2021-07-25 10:18:58'),
(21, '4', 0, 'Withdraw', '2021-07-25 13:07:46', '2021-07-25 13:07:46', 4300499, 'Transfer withdraw of <b>4,300,499</b>  from Account (<b>4</b>) by ARINDA POROE on 2021-07-25 13:07:46', 0, 4, '2021-07-25 10:21:46'),
(22, '', 0, 'deposit', '2021-07-25 13:07:46', '2021-07-25 13:07:46', 4300499, 'Transfer Deposit of <b>4,300,499</b>   Account (<b>4</b>) by ARINDA POROE on 2021-07-25 13:07:46', 0, 4, '2021-07-25 10:21:46'),
(23, '4', 0, 'Withdraw', '2021-07-25 13:07:29', '2021-07-25 13:07:29', 4300499, 'Transfer withdraw of <b>4,300,499</b>  from Account (<b>4</b>) by ARINDA POROE on 2021-07-25 13:07:29', 0, 4, '2021-07-25 10:22:29'),
(24, '68958554933', 0, 'deposit', '2021-07-25 13:07:29', '2021-07-25 13:07:29', 4300499, 'Transfer Deposit of <b>4,300,499</b>   Account (<b>4</b>) by ARINDA POROE on 2021-07-25 13:07:29', 0, 4, '2021-07-25 10:22:29'),
(25, '6598579424', 0, 'Withdraw', '2021-07-25 13:07:06', '2021-07-25 13:07:06', 30000000000, 'Transfer withdraw of <b>30,000,000,000</b>  from Account (<b>6598579424</b>) by  on 2021-07-25 13:07:06', 0, 4, '2021-07-25 10:33:06'),
(26, '68958554933', 0, 'deposit', '2021-07-25 13:07:06', '2021-07-25 13:07:06', 30000000000, 'Transfer Deposit of <b>30,000,000,000</b>   Account (<b>6598579424</b>) by  on 2021-07-25 13:07:06', 0, 4, '2021-07-25 10:33:06'),
(27, '6598579424', 0, 'Withdraw', '2021-07-25 13:07:33', '2021-07-25 13:07:33', 30000000000, 'Transfer withdraw of <b>30,000,000,000</b>  from Account (<b>6598579424</b>) by  on 2021-07-25 13:07:33', 0, 4, '2021-07-25 10:34:33'),
(28, '68958554933', 0, 'deposit', '2021-07-25 13:07:33', '2021-07-25 13:07:33', 30000000000, 'Transfer Deposit of <b>30,000,000,000</b>   Account (<b>6598579424</b>) by  on 2021-07-25 13:07:33', 0, 4, '2021-07-25 10:34:33'),
(29, '6598579424', 0, 'Withdraw', '2021-07-25 13:07:07', '2021-07-25 13:07:07', 30000000000, 'Transfer withdraw of <b>30,000,000,000</b>  from Account (<b>6598579424</b>) by jueelo on 2021-07-25 13:07:07', 0, 4, '2021-07-25 10:35:07'),
(30, '68958554933', 0, 'deposit', '2021-07-25 13:07:07', '2021-07-25 13:07:07', 30000000000, 'Transfer Deposit of <b>30,000,000,000</b>   Account (<b>6598579424</b>) by jueelo on 2021-07-25 13:07:07', 0, 4, '2021-07-25 10:35:07'),
(31, '6598579424', 0, 'Transfer', '2021-07-25 13:07:03', '2021-07-25 13:07:03', 4000, 'Transfer withdraw of <b>4,000</b>  from Account (<b>6598579424</b>) by jueelo on 2021-07-25 13:07:03', 0, 4, '2021-07-25 10:38:03'),
(32, '', 0, 'Transfer', '2021-07-25 13:07:03', '2021-07-25 13:07:03', 4000, 'Transfer Deposit of <b>4,000</b>   Account (<b>6598579424</b>) by jueelo on 2021-07-25 13:07:03', 0, 4, '2021-07-25 10:38:03'),
(33, '6598579424', 4, 'Deposit', '2021-07-25 13:07:50', '2021-07-25 13:07:50', 12000000000, 'Deposit of 12,000,000,000 to Account (6598579424) by jueelo on  Sunday 25th July 2021', 0, 4, '2021-07-25 10:38:50'),
(34, '6598579424', 4, 'Deposit', '2021-07-25 13:07:06', '2021-07-25 13:07:06', 567000000000, 'Deposit of 567,000,000,000 to Account (6598579424) by jueelo on  Sunday 25th July 2021', 0, 4, '2021-07-25 10:39:06'),
(35, '68958554933', 0, 'Transfer', '2021-07-25 13:07:34', '2021-07-25 13:07:34', 120000, 'Transfer withdraw of <b>120,000</b>  from Account (<b>68958554933</b>) by juma on 2021-07-25 13:07:34', 0, 1, '2021-07-25 10:46:34'),
(36, '6598579424', 0, 'Transfer', '2021-07-25 13:07:34', '2021-07-25 13:07:34', 120000, 'Received <b>120,000</b>  from  Account (<b>68958554933</b>) by juma on 2021-07-25 13:07:34', 0, 1, '2021-07-25 10:46:34'),
(37, '6598579424', 0, 'Transfer', '2021-07-25 13:07:01', '2021-07-25 13:07:01', 2300000, 'Transfer withdraw of <b>2,300,000</b>  from Account (<b>6598579424</b>) by jueelo on 2021-07-25 13:07:01', 0, 4, '2021-07-25 10:50:01'),
(38, '68958554933', 0, 'Transfer', '2021-07-25 13:07:01', '2021-07-25 13:07:01', 2300000, 'Received <b>2,300,000</b>  from  Account (<b>6598579424</b>) by jueelo on 2021-07-25 13:07:01', 0, 1, '2021-07-25 10:50:01'),
(39, '68958554933', 0, 'Transfer', '2021-07-25 13:07:26', '2021-07-25 13:07:26', 74768198, 'Transfer withdraw of <b>74,768,198</b>  from Account (<b>68958554933</b>) by juma on 2021-07-25 13:07:26', 0, 1, '2021-07-25 10:52:26'),
(40, '6598579424', 0, 'Transfer', '2021-07-25 13:07:26', '2021-07-25 13:07:26', 74768198, 'Received <b>74,768,198</b>  from  Account (<b>68958554933</b>) by juma on 2021-07-25 13:07:26', 0, 4, '2021-07-25 10:52:26'),
(41, '68958554933', 0, 'Withdraw', '2021-07-25 14:07:25', '2021-07-25 14:07:25', 12000000, 'Withdraw of 12,000,000 from  Account (68958554933) by MUKOOVA JUMA on  Sunday 25th July 2021', 1, 1, '2021-07-25 11:17:25'),
(42, '68958554933', 0, 'Withdraw', '2021-07-25 14:07:53', '2021-07-25 14:07:53', 900000, 'Withdraw of 900,000 from  Account (68958554933) by MUKOOVA JUMA on  Sunday 25th July 2021', 1, 1, '2021-07-25 11:17:53'),
(43, '68958554933', 1, 'Deposit', '2021-07-25 14:07:50', '2021-07-25 14:07:50', 120000, 'Deposit of 120,000 to Account (68958554933) by juma on  Sunday 25th July 2021', 0, 1, '2021-07-25 11:21:50'),
(44, '68958554933', 0, 'Transfer', '2021-07-25 14:07:44', '2021-07-25 14:07:44', 320000, 'Transfer withdraw of <b>320,000</b>  from Account (<b>68958554933</b>) by juma on 2021-07-25 14:07:44', 0, 1, '2021-07-25 11:26:44'),
(45, '6598579424', 0, 'Transfer', '2021-07-25 14:07:44', '2021-07-25 14:07:44', 320000, 'Received <b>320,000</b>  from  Account (<b>68958554933</b>) by juma on 2021-07-25 14:07:44', 0, 4, '2021-07-25 11:26:44'),
(46, '6598579424', 0, 'Withdraw', '2021-07-25 14:07:16', '2021-07-25 14:07:16', 50000000, 'Withdraw of 50,000,000 from  Account (6598579424) by MUKOOVA JUMA on  Sunday 25th July 2021', 1, 4, '2021-07-25 11:29:16'),
(47, '68958554933', 1, 'Deposit', '2021-07-25 16:07:45', '2021-07-25 16:07:45', 4320000, 'Deposit of 4,320,000 to Account (68958554933) by juma on  Sunday 25th July 2021', 0, 1, '2021-07-25 13:20:46');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `id` int(11) NOT NULL,
  `reset_key` text DEFAULT NULL,
  `username` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`id`, `reset_key`, `username`) VALUES
(1, '8d673aaf3d3fce59775baa2b57749e2d', 'hamis');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(30) NOT NULL,
  `loan_id` int(30) NOT NULL,
  `payee` text NOT NULL,
  `amount` float NOT NULL DEFAULT 0,
  `penalty_amount` float NOT NULL DEFAULT 0,
  `overdue` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0=no , 1 = yes',
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `loan_id`, `payee`, `amount`, `penalty_amount`, `overdue`, `date_created`) VALUES
(2, 3, 'Smith, John C', 3000, 0, 0, '2020-09-26 15:51:01');

-- --------------------------------------------------------

--
-- Table structure for table `system_access_logs`
--

CREATE TABLE `system_access_logs` (
  `id` int(11) NOT NULL,
  `activity` text DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `month` varchar(40) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `username` text DEFAULT NULL,
  `activity_time` text NOT NULL,
  `log_type` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_access_logs`
--

INSERT INTO `system_access_logs` (`id`, `activity`, `day`, `month`, `year`, `username`, `activity_time`, `log_type`, `status`) VALUES
(1, 'logged in from 127.0.0.1', 23, 'July', 2021, 'gits', 'Friday 23<sup>rd</sup>, July 2021 (08:40:26 PM)', 'SUCCESS', 0),
(2, 'logged out from 127.0.0.1', 23, 'July', 2021, 'gits', 'Friday 23<sup>rd</sup>, July 2021 (08:40:47 PM)', 'INFO', 0),
(3, 'logged in from 127.0.0.1', 23, 'July', 2021, 'hamis', 'Friday 23<sup>rd</sup>, July 2021 (08:41:48 PM)', 'SUCCESS', 0),
(4, 'logged in from 127.0.0.1', 23, 'July', 2021, 'gits', 'Friday 23<sup>rd</sup>, July 2021 (09:09:40 PM)', 'SUCCESS', 0),
(5, 'Made an edit on the A Supplier : KERICHO HAMMER  from 127.0.0.1', 23, 'July', 2021, '1', 'Friday 23<sup>rd</sup>, July 2021 (11:18:49 PM)', 'INFO', 0),
(6, 'Made an edit on the A Supplier : KERICHO HAMMER  from 127.0.0.1', 23, 'July', 2021, '1', 'Friday 23<sup>rd</sup>, July 2021 (11:19:09 PM)', 'INFO', 0),
(7, 'logged out from 127.0.0.1', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (06:48:47 AM)', 'INFO', 0),
(8, 'logged in from 127.0.0.1', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (06:49:04 AM)', 'SUCCESS', 0),
(9, 'logged out from 127.0.0.1', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (07:52:34 AM)', 'INFO', 0),
(10, 'logged in from 127.0.0.1', 24, 'July', 2021, 'hamis', 'Saturday 24<sup>th</sup>, July 2021 (07:52:42 AM)', 'SUCCESS', 0),
(11, 'Made an update on the A Client : Emmanuel  from 127.0.0.1', 24, 'July', 2021, '', 'Saturday 24<sup>th</sup>, July 2021 (08:01:15 AM)', 'INFO', 0),
(12, 'logged in from 127.0.0.1', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (08:02:14 AM)', 'SUCCESS', 0),
(13, 'Made an update on the A Client : Emmanuel  from 127.0.0.1', 24, 'July', 2021, '1', 'Saturday 24<sup>th</sup>, July 2021 (08:02:32 AM)', 'INFO', 0),
(14, 'logged in from 127.0.0.1', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (09:16:23 AM)', 'SUCCESS', 0),
(15, 'Created a New Client Account  kulaba bryan', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (09:19:55 AM)', 'SUCCESS', 0),
(16, 'Created an new Account for client kulaba bryan', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (09:20:07 AM)', 'SUCCESS', 0),
(17, 'Created a New Client Account  kulaba bryan', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (09:20:11 AM)', 'SUCCESS', 0),
(18, 'Created an new Account for client kulaba bryan', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (09:20:17 AM)', 'SUCCESS', 0),
(19, 'logged in from 127.0.0.1', 24, 'July', 2021, 'bryan', 'Saturday 24<sup>th</sup>, July 2021 (09:26:38 AM)', 'SUCCESS', 0),
(20, 'Created a New Client Account  JUDSME', 24, 'July', 2021, 'bryan', 'Saturday 24<sup>th</sup>, July 2021 (09:57:57 AM)', 'SUCCESS', 0),
(21, 'Created an new Account for client JUDSME', 24, 'July', 2021, 'bryan', 'Saturday 24<sup>th</sup>, July 2021 (09:58:02 AM)', 'SUCCESS', 0),
(22, 'Created a New Client Account  JUDSME', 24, 'July', 2021, 'bryan', 'Saturday 24<sup>th</sup>, July 2021 (09:59:21 AM)', 'SUCCESS', 0),
(23, 'Created an new Account for client JUDSME', 24, 'July', 2021, 'bryan', 'Saturday 24<sup>th</sup>, July 2021 (09:59:25 AM)', 'SUCCESS', 0),
(24, 'Created a New Client Account  MENSD', 24, 'July', 2021, 'bryan', 'Saturday 24<sup>th</sup>, July 2021 (10:22:36 AM)', 'SUCCESS', 0),
(25, 'Created an new Account for client MENSD', 24, 'July', 2021, 'bryan', 'Saturday 24<sup>th</sup>, July 2021 (10:22:41 AM)', 'SUCCESS', 0),
(26, 'Created a New Client Account  MENSD', 24, 'July', 2021, 'bryan', 'Saturday 24<sup>th</sup>, July 2021 (10:24:18 AM)', 'SUCCESS', 0),
(27, 'Created an new Account for client MENSD', 24, 'July', 2021, 'bryan', 'Saturday 24<sup>th</sup>, July 2021 (10:24:24 AM)', 'SUCCESS', 0),
(28, 'logged out from 127.0.0.1', 24, 'July', 2021, 'bryan', 'Saturday 24<sup>th</sup>, July 2021 (10:38:37 AM)', 'INFO', 0),
(29, 'logged in from 127.0.0.1', 24, 'July', 2021, 'hamis', 'Saturday 24<sup>th</sup>, July 2021 (10:39:55 AM)', 'SUCCESS', 0),
(30, 'logged in from 127.0.0.1', 24, 'July', 2021, 'hamis', 'Saturday 24<sup>th</sup>, July 2021 (10:54:39 AM)', 'SUCCESS', 0),
(31, 'logged in from 127.0.0.1', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (10:55:12 AM)', 'SUCCESS', 0),
(32, 'Added a new Receipient READY MODE', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (10:58:48 AM)', 'SUCCESS', 0),
(33, 'Created an new Account for  READY MODE', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (10:58:48 AM)', 'SUCCESS', 0),
(34, 'logged in from 127.0.0.1', 24, 'July', 2021, 'hamis', 'Saturday 24<sup>th</sup>, July 2021 (12:27:49 PM)', 'SUCCESS', 0),
(35, 'logged out from 127.0.0.1', 24, 'July', 2021, 'hamis', 'Saturday 24<sup>th</sup>, July 2021 (12:28:02 PM)', 'INFO', 0),
(36, 'logged in from 127.0.0.1', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (12:28:10 PM)', 'SUCCESS', 0),
(37, 'logged in from 127.0.0.1', 24, 'July', 2021, 'hamis', 'Saturday 24<sup>th</sup>, July 2021 (12:30:42 PM)', 'SUCCESS', 0),
(38, 'logged out from 127.0.0.1', 24, 'July', 2021, 'hamis', 'Saturday 24<sup>th</sup>, July 2021 (02:12:49 PM)', 'INFO', 0),
(39, 'logged in from 127.0.0.1', 24, 'July', 2021, 'hamis', 'Saturday 24<sup>th</sup>, July 2021 (02:12:58 PM)', 'SUCCESS', 0),
(40, 'logged in from 127.0.0.1', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (02:14:04 PM)', 'SUCCESS', 0),
(41, 'Created a New Client Account  kulaba austin', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (02:29:34 PM)', 'SUCCESS', 0),
(42, 'Created an new Account for client kulaba austin', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (02:29:40 PM)', 'SUCCESS', 0),
(43, 'logged out from 127.0.0.1', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (04:03:53 PM)', 'INFO', 0),
(44, 'logged in from 127.0.0.1', 24, 'July', 2021, 'hamis', 'Saturday 24<sup>th</sup>, July 2021 (04:06:09 PM)', 'SUCCESS', 0),
(45, 'logged out from 127.0.0.1', 24, 'July', 2021, 'hamis', 'Saturday 24<sup>th</sup>, July 2021 (04:06:47 PM)', 'INFO', 0),
(46, 'logged in from 127.0.0.1', 24, 'July', 2021, 'hamis', 'Saturday 24<sup>th</sup>, July 2021 (04:06:58 PM)', 'SUCCESS', 0),
(47, 'logged out from 127.0.0.1', 24, 'July', 2021, 'hamis', 'Saturday 24<sup>th</sup>, July 2021 (04:08:32 PM)', 'INFO', 0),
(48, 'logged in from 127.0.0.1', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (04:19:03 PM)', 'SUCCESS', 0),
(49, 'logged out from 127.0.0.1', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (04:19:36 PM)', 'INFO', 0),
(50, 'logged in from 127.0.0.1', 24, 'July', 2021, 'hamis', 'Saturday 24<sup>th</sup>, July 2021 (04:19:42 PM)', 'SUCCESS', 0),
(51, 'logged out from 127.0.0.1', 24, 'July', 2021, 'hamis', 'Saturday 24<sup>th</sup>, July 2021 (04:25:24 PM)', 'INFO', 0),
(52, 'logged in from 127.0.0.1', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (04:25:33 PM)', 'SUCCESS', 0),
(53, 'Created a New Client Account  mujksdd', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (04:51:37 PM)', 'SUCCESS', 0),
(54, 'Created an new Account for client mujksdd', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (04:52:19 PM)', 'SUCCESS', 0),
(55, 'Created a New Client Account  MERE RENED', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (04:53:17 PM)', 'SUCCESS', 0),
(56, 'Created an new Account for client MERE RENED', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (04:53:17 PM)', 'SUCCESS', 0),
(57, 'Created a New Client Account  MERE RENED', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (04:53:24 PM)', 'SUCCESS', 0),
(58, 'Created an new Account for client MERE RENED', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (04:53:24 PM)', 'SUCCESS', 0),
(59, 'Created a New Client Account  MERE RENED', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (04:54:48 PM)', 'SUCCESS', 0),
(60, 'Created an new Account for client MERE RENED', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (04:54:48 PM)', 'SUCCESS', 0),
(61, 'Created a New Client Account  MERE RENED', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (05:09:36 PM)', 'SUCCESS', 0),
(62, 'Created a New Client Account  MERE RENED', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (05:10:18 PM)', 'SUCCESS', 0),
(63, 'Created a New Client Account  MUK MESA', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (05:11:10 PM)', 'SUCCESS', 0),
(64, 'Created a New Client Account  MUK MESA', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (05:12:12 PM)', 'SUCCESS', 0),
(65, 'logged in from 127.0.0.1', 24, 'July', 2021, 'hamis', 'Saturday 24<sup>th</sup>, July 2021 (07:44:47 PM)', 'SUCCESS', 0),
(66, 'logged out from 127.0.0.1', 24, 'July', 2021, 'hamis', 'Saturday 24<sup>th</sup>, July 2021 (07:45:18 PM)', 'INFO', 0),
(67, 'logged in from 127.0.0.1', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (07:45:28 PM)', 'SUCCESS', 0),
(68, 'Created a New Client Account  SOME PEOPLE', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (09:06:25 PM)', 'SUCCESS', 0),
(69, 'Created a New Client Account  MUKOOVA JUMA', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (09:11:17 PM)', 'SUCCESS', 0),
(70, 'logged in from 127.0.0.1', 24, 'July', 2021, 'jumagits', 'Saturday 24<sup>th</sup>, July 2021 (09:12:21 PM)', 'SUCCESS', 0),
(71, 'logged out from 127.0.0.1', 24, 'July', 2021, 'jumagits', 'Saturday 24<sup>th</sup>, July 2021 (09:24:14 PM)', 'INFO', 0),
(72, 'logged in from 127.0.0.1', 24, 'July', 2021, 'jumagits', 'Saturday 24<sup>th</sup>, July 2021 (09:24:37 PM)', 'SUCCESS', 0),
(73, 'logged out from 127.0.0.1', 24, 'July', 2021, 'jumagits', 'Saturday 24<sup>th</sup>, July 2021 (09:32:17 PM)', 'INFO', 0),
(74, 'logged in from 127.0.0.1', 24, 'July', 2021, 'jumagits', 'Saturday 24<sup>th</sup>, July 2021 (09:33:37 PM)', 'SUCCESS', 0),
(75, 'logged out from 127.0.0.1', 24, 'July', 2021, 'jumagits', 'Saturday 24<sup>th</sup>, July 2021 (09:34:36 PM)', 'INFO', 0),
(76, 'logged in from 127.0.0.1', 24, 'July', 2021, 'jumagits', 'Saturday 24<sup>th</sup>, July 2021 (09:34:48 PM)', 'SUCCESS', 0),
(77, 'logged out from 127.0.0.1', 24, 'July', 2021, 'jumagits', 'Saturday 24<sup>th</sup>, July 2021 (09:35:56 PM)', 'INFO', 0),
(78, 'logged in from 127.0.0.1', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (09:36:06 PM)', 'SUCCESS', 0),
(79, 'logged out from 127.0.0.1', 24, 'July', 2021, 'gits', 'Saturday 24<sup>th</sup>, July 2021 (09:37:50 PM)', 'INFO', 0),
(80, 'logged in from 127.0.0.1', 25, 'July', 2021, 'jumagits', 'Sunday 25<sup>th</sup>, July 2021 (10:20:02 AM)', 'SUCCESS', 0),
(81, 'Created a New Client Account  MWEEBE CHRISTOPHER', 25, 'July', 2021, 'jumagits', 'Sunday 25<sup>th</sup>, July 2021 (11:38:29 AM)', 'SUCCESS', 0),
(82, 'Created an new Account for client MWEEBE CHRISTOPHER', 25, 'July', 2021, 'jumagits', 'Sunday 25<sup>th</sup>, July 2021 (11:38:35 AM)', 'SUCCESS', 0),
(83, 'Created a New Client Account  malaria roed', 25, 'July', 2021, 'jumagits', 'Sunday 25<sup>th</sup>, July 2021 (11:52:24 AM)', 'SUCCESS', 0),
(84, 'Created an new Account for client malaria roed', 25, 'July', 2021, 'jumagits', 'Sunday 25<sup>th</sup>, July 2021 (11:52:31 AM)', 'SUCCESS', 0),
(85, 'Created an new Account for client ARINDA POROE', 25, 'July', 2021, 'jumagits', 'Sunday 25<sup>th</sup>, July 2021 (11:56:29 AM)', 'SUCCESS', 0),
(86, 'Created a New Client Account  ARINDA POROE', 25, 'July', 2021, 'jumagits', 'Sunday 25<sup>th</sup>, July 2021 (11:56:29 AM)', 'SUCCESS', 0),
(87, 'logged out from 127.0.0.1', 25, 'July', 2021, 'jumagits', 'Sunday 25<sup>th</sup>, July 2021 (11:59:08 AM)', 'INFO', 0),
(88, 'logged in from 127.0.0.1', 25, 'July', 2021, 'rendor', 'Sunday 25<sup>th</sup>, July 2021 (11:59:21 AM)', 'SUCCESS', 0),
(89, 'logged out from 127.0.0.1', 25, 'July', 2021, '', 'Sunday 25<sup>th</sup>, July 2021 (01:05:51 PM)', 'INFO', 0),
(90, 'logged out from 127.0.0.1', 25, 'July', 2021, 'rendor', 'Sunday 25<sup>th</sup>, July 2021 (01:40:13 PM)', 'INFO', 0),
(91, 'logged in from 127.0.0.1', 25, 'July', 2021, 'jumagits', 'Sunday 25<sup>th</sup>, July 2021 (01:40:23 PM)', 'SUCCESS', 0),
(92, 'logged out from 127.0.0.1', 25, 'July', 2021, 'jumagits', 'Sunday 25<sup>th</sup>, July 2021 (01:46:52 PM)', 'INFO', 0),
(93, 'logged in from 127.0.0.1', 25, 'July', 2021, 'rendor', 'Sunday 25<sup>th</sup>, July 2021 (01:47:40 PM)', 'SUCCESS', 0),
(94, 'logged out from 127.0.0.1', 25, 'July', 2021, 'rendor', 'Sunday 25<sup>th</sup>, July 2021 (01:50:20 PM)', 'INFO', 0),
(95, 'logged in from 127.0.0.1', 25, 'July', 2021, 'jumagits', 'Sunday 25<sup>th</sup>, July 2021 (01:50:28 PM)', 'SUCCESS', 0),
(96, 'logged out from 127.0.0.1', 25, 'July', 2021, 'jumagits', 'Sunday 25<sup>th</sup>, July 2021 (02:28:33 PM)', 'INFO', 0),
(97, 'logged in from 127.0.0.1', 25, 'July', 2021, 'rendor', 'Sunday 25<sup>th</sup>, July 2021 (02:28:45 PM)', 'SUCCESS', 0),
(98, 'logged in from 127.0.0.1', 25, 'July', 2021, 'jumagits', 'Sunday 25<sup>th</sup>, July 2021 (04:19:44 PM)', 'SUCCESS', 0),
(99, 'logged in from 127.0.0.1', 25, 'July', 2021, 'jumagits', 'Sunday 25<sup>th</sup>, July 2021 (08:31:22 PM)', 'SUCCESS', 0),
(100, 'logged in from 127.0.0.1', 25, 'July', 2021, 'jumagits', 'Sunday 25<sup>th</sup>, July 2021 (10:39:38 PM)', 'SUCCESS', 0),
(101, 'logged out from 127.0.0.1', 25, 'July', 2021, '', 'Sunday 25<sup>th</sup>, July 2021 (11:29:44 PM)', 'INFO', 0),
(102, 'logged out from 127.0.0.1', 26, 'July', 2021, '', 'Monday 26<sup>th</sup>, July 2021 (12:02:56 AM)', 'INFO', 0),
(103, 'logged out from 127.0.0.1', 26, 'July', 2021, 'jumagits', 'Monday 26<sup>th</sup>, July 2021 (07:56:35 AM)', 'INFO', 0),
(104, 'logged in from 127.0.0.1', 26, 'July', 2021, 'gits', 'Monday 26<sup>th</sup>, July 2021 (07:56:55 AM)', 'SUCCESS', 0),
(105, 'logged in from 127.0.0.1', 26, 'July', 2021, 'jumagits', 'Monday 26<sup>th</sup>, July 2021 (08:26:41 AM)', 'SUCCESS', 0),
(106, 'logged in from 127.0.0.1', 26, 'July', 2021, 'jumagits', 'Monday 26<sup>th</sup>, July 2021 (10:30:53 AM)', 'SUCCESS', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `sname` varchar(100) DEFAULT NULL,
  `fname` varchar(100) DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `gender` varchar(15) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` text DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `accountType` int(11) DEFAULT NULL,
  `account_status` int(11) NOT NULL DEFAULT 0,
  `pass_update` int(11) NOT NULL,
  `last_login_ip` text NOT NULL,
  `last_login_date` text NOT NULL,
  `created_by` varchar(40) NOT NULL,
  `registered` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_on` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `sname`, `fname`, `username`, `gender`, `email`, `mobile`, `password`, `role`, `accountType`, `account_status`, `pass_update`, `last_login_ip`, `last_login_date`, `created_by`, `registered`, `created_on`, `updated_on`) VALUES
(1, 'MUKOOVA', 'JUMA', 'gits', 'M', 'mukoovajuma183@gmail.com', '0704394949', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 1, 0, 0, '127.0.0.1', 'Saturday 24<sup>th</sup>, Jul 2021 (09:37:50 PM)', '', 0, '2020-08-09 18:01:08', '2021-07-24 18:37:50'),
(2, 'HAMISA', 'MOBETO', 'hamisa', '1', 'hamisa@gmail.com', '0702499549', '3297d01e375f6a070431172154218f46', 1, 0, 0, 0, '', '', '1', 0, '2020-08-09 18:09:53', '2020-09-04 03:15:06'),
(3, 'KASADHA', 'MAJID', 'regnorock', '1', 'majid@gmail.com', '0704333554', '9832379e796beccb3546e318bbca49b4', 1, 0, 0, 0, '', '', 'KILIGWAJJO HAMIS', 0, '2020-08-25 08:34:50', '2020-08-25 08:35:30'),
(5, 'f', 'k', 'mams', '1', 'mukod@gmail.com', '0705944939', 'e44150323ebbb07a8de72074fe4ebbec', NULL, NULL, 0, 0, '', '', 'kulaba bryan', 0, '2021-07-24 06:55:51', '2021-07-24 06:55:51');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL,
  `role_name` text DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NULL DEFAULT current_timestamp(),
  `updated_on` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `role_name`, `created_by`, `created_on`, `updated_on`) VALUES
(1, 'Administrator', 1, '2020-08-09 18:04:56', '2020-08-09 18:04:56'),
(2, 'Manager', 1, '2020-10-28 09:14:57', '2020-10-28 09:14:57'),
(3, 'User', 1, '2020-10-28 09:15:24', '2020-10-28 09:15:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accountability`
--
ALTER TABLE `accountability`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `borrowers`
--
ALTER TABLE `borrowers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `client_account`
--
ALTER TABLE `client_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_permission`
--
ALTER TABLE `client_permission`
  ADD PRIMARY KEY (`permission_id`);

--
-- Indexes for table `client_revenues`
--
ALTER TABLE `client_revenues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_roles`
--
ALTER TABLE `client_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `invoice_ledgers`
--
ALTER TABLE `invoice_ledgers`
  ADD PRIMARY KEY (`ledger_id`);

--
-- Indexes for table `loan_list`
--
ALTER TABLE `loan_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_plan`
--
ALTER TABLE `loan_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_schedules`
--
ALTER TABLE `loan_schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_types`
--
ALTER TABLE `loan_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mega_trans_logs`
--
ALTER TABLE `mega_trans_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_access_logs`
--
ALTER TABLE `system_access_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accountability`
--
ALTER TABLE `accountability`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `borrowers`
--
ALTER TABLE `borrowers`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `client_account`
--
ALTER TABLE `client_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `client_permission`
--
ALTER TABLE `client_permission`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `client_revenues`
--
ALTER TABLE `client_revenues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `client_roles`
--
ALTER TABLE `client_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_ledgers`
--
ALTER TABLE `invoice_ledgers`
  MODIFY `ledger_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `loan_list`
--
ALTER TABLE `loan_list`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `loan_plan`
--
ALTER TABLE `loan_plan`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `loan_schedules`
--
ALTER TABLE `loan_schedules`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `loan_types`
--
ALTER TABLE `loan_types`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `mega_trans_logs`
--
ALTER TABLE `mega_trans_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `system_access_logs`
--
ALTER TABLE `system_access_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
