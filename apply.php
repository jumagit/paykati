<?php include "includes/header.php";?>

<?php include "includes/topNav.php";?>

<?php include "includes/sideNav.php";?>



<?php
$customer_id = $_SESSION['client_id'];
$ac_details = get_account_details($customer_id);
$acc_date = f_date($ac_details['created_at']);
$acc_name = $ac_details['account_name'];
$acc_number = $ac_details['account_number'];
$acc_id = $ac_details['id'];
$acc_balance = $ac_details['balance'];
$name = $ac_details['account_name'];
?>


<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4 class="page-title">LOAN APPLICATION </h4>
            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row">

            <div class="col-lg-8">
                <div class="panel">
                    <div class="panel-body">
                        <form action="" id="applyForLoan">
                        <div id="calculation_table"></div>
                            <div class="form-group">
                                <label class="control-label">Loan Type</label>
                                <?php
$type = query("SELECT * FROM loan_types order by `type_name` desc ");
?>
                                <select name="loan_type_id" id="loan_type_id"
                                    class="custom-select browser-default select2">
                                    <option value=""></option>
                                    <?php while ($row = $type->fetch_assoc()): ?>
                                    <option value="<?php echo $row['id'] ?>"
                                        <?php echo isset($loan_type_id) && $loan_type_id == $row['id'] ? "selected" : '' ?>>
                                        <?php echo $row['type_name'] ?></option>
                                    <?php endwhile;?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Loan Plan</label>
                                <?php
$plan = query("SELECT * FROM loan_plan order by `months` desc ");
?>
                                <select name="plan_id" id="plan_id" class="custom-select browser-default select2">
                                    <option value=""></option>
                                    <?php while ($row = $plan->fetch_assoc()): ?>
                                    <option value="<?php echo $row['id'] ?>"
                                        <?php echo isset($plan_id) && $plan_id == $row['id'] ? "selected" : '' ?>
                                        data-months="<?php echo $row['months'] ?>"
                                        data-interest_percentage="<?php echo $row['interest_percentage'] ?>"
                                        data-penalty_rate="<?php echo $row['penalty_rate'] ?>">
                                        <?php echo $row['months'] . ' month/s [ ' . $row['interest_percentage'] . '%, ' . $row['penalty_rate'] . '% ]' ?>
                                    </option>
                                    <?php endwhile;?>
                                </select>
                                <small>months [ interest%,penalty% ]</small>
                            </div>
                            <div class="form-group ">
                                <label class="control-label">Loan Amount</label>
                                <input type="text" name="amount" class="form-control" id=""
                                    value="<?php echo isset($amount) ? $amount : '' ?>">
                            </div>

                            <div class="form-group ">
                                <label class="control-label">Purpose</label>
                                <textarea name="purpose" id="" cols="30" rows="2"
                                    class="form-control"><?php echo isset($purpose) ? $purpose : '' ?></textarea>
                            </div>

                            <div class="form-group ">
                                <button class="btn btn-primary " type="button" id="calculate">Calculate
                                </button>
                            </div>

                           

                            <div class="form-group">

                                <button class="btn btn-primary btn-sm ">Save</button>

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
        $(document).ready(function() {
            $('#calculate').on('click', () => {
                calculate();
            });

            //


    $('form#applyForLoan').on("submit", function(e) {
    e.preventDefault();

    var formData = new FormData(this);

    swal({
        title: "Are you sure?",
        text: "Okay to add a  New Application",
        type: "info",
        padding: 20,
        showCancelButton: true,
        confirmButtonColor: "#384888",
        confirmButtonText: "Yes, create!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "php_action/create_plans.php?t=save_loan",
                data: formData,
                success: function(result) {

                    if (result.status) {
                        swal({
                            title: "Good job!",
                            padding: 20,
                            text: "Good Job! A New Loan application has been submitted Successfully!",
                            type: "success"
                        });

                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    } else {
                        swal({
                            title: "Oops!",
                            padding: 20,
                            text: result.msg + "..please try again!",
                            type: "warning"
                        });
                    }
                },
                error: function(jqXHR) {
                    console.log(jqXHR);
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
});




















        });

        function calculate() {
            if ($('#loan_plan_id').val() == '' && $('[name="amount"]').val() == '') {
                alert("Select plan and enter amount first.", "warning");
                return false;
            }
            var plan = $("#plan_id option[value='" + $("#plan_id").val() + "']")
            $.ajax({
                url: "calculation_table.php",
                method: "POST",
                data: {
                    amount: $('[name="amount"]').val(),
                    months: plan.attr('data-months'),
                    interest: plan.attr('data-interest_percentage'),
                    penalty: plan.attr('data-penalty_rate')
                },
                success: function(resp) {
                    if (resp) {

                        $('#calculation_table').html(resp)

                    }
                }

            })
        }

        /////////////

  
</script>



        </script>
        <?php include 'includes/footer.php';?>