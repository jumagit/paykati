            <?php session_start(); ?>
            <?php

            require "includes/db.php";

            $sql_deposits = "SELECT * FROM sacco_transfer_logs where trans_type='Deposit' AND account_id='$acc_id' AND business_id='$businessid' order by record_date Desc";
            $deposits_result = mysqli_query($connection, $sql_deposits);

            if (mysqli_num_rows($deposits_result) > 0) {
                $no=1;
                // output data of each row
                while($row = mysqli_fetch_assoc($deposits_result)) {
                   ?>
             <tr>
                <td>
                    <?php echo $no; ?>. 
                    <?php  echo $row['description']; ?> on
                    <?php  echo $row['trans_date']; ?>
                         
                </td>
             </tr> 
            <?php
            $no++;
                }
            } else {
                echo "<h4 style='color:red;'>No savings for this Account</h4>";
            }

mysqli_close($connection);


            ?>


            <?php

$sql_deposits = "SELECT * FROM sacco_transfer_logs where account_id='$acc_id' AND business_id='$businessid' order by record_date Desc";
$deposits_result = mysqli_query($connection, $sql_deposits);

if (mysqli_num_rows($deposits_result) > 0) {
    $no=1;
    // output data of each row
    while($row = mysqli_fetch_assoc($deposits_result)) {

?>
<tr>
    <td>
     <?php echo $no; ?>.
     <?php echo $row['description']; ?> on 
     <?php echo $row['trans_date']; ?></td>
</tr>
<?php
$no++;
    }

} else {
    echo "<h4 style='color:red;'>No savings for this Account</h4>";
}

?>

<!-- //update code -->


<?php

if (isset($_POST['update'])) {
    $pname = clean($_POST['acc_provider']);
    $aname = clean($_POST['acc_name']);
    $anumber = clean($_POST['acc_number']);

    $pnameold = clean($_POST['acc_providerold']);
    $anameold = clean($_POST['acc_nameold']);
    $anumberold = clean($_POST['acc_numberold']);

    $update = query("UPDATE sacco_bank_accounts set service_provider='$pname', account_name='$aname',  account_number='$anumber' where id='$acc_id' AND business_id='$businessid'") or die(mysqli_error($connection));
    if ($update) {
        $action = "Updated a bank account as below:<br>
            <b>Bank Name: </b>from " . $pnameold . " <b>to</b> " . $pname . "<br>
            <b>Account Names: </b> from " . $anameold . " <b>to</b> " . $aname . "<br>
            <b>Account Number: </b> from " . $anumberold . " <b>to</b> " . $anumber;

        $log_update = query("INSERT INTO sacco_audittrail(action, date, business_id, user_id) VALUES ('$action','$date', '$businessid','$userid')");
        if ($log_update) {
            $heading = "Successful";
            $message = "Updated bank Account successfully.";
            $loaderBg = "ff6849";
            $icon = "success";
            ?>
<meta http-equiv="refresh" content="0;URL='../bank_accounts/account_details?id=<?php echo $acc_id; ?>&message=<?php echo $message; ?>&heading=<?php echo $heading; ?>&loaderbg=<?php echo $loaderBg; ?>&icon=<?php echo $icon; ?>'" />
<?php
} else {
            echo "Error recording Account update log " . mysqli_error($connection);
        }
    } else {
        echo "Failed to update group. " . mysqli_error($connection);
        $action = "Failed to update group, " . $pnameold . " Account";

        $log_sql = mysqli_query($connection, "INSERT INTO sacco_audittrail(action, date, business_id, user_id) VALUES ('$action','$date', '$businessid','$userid')");
    }
}
?>