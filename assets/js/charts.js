     jQuery(document).ready(function($) {
        
         client_chart();
         customer_chart();
         order_charts();
         combined();        
         admin_chart();
       

          function admin_chart() {
             $.post('php_action/client_charts.php', function(data, textStatus, xhr) {
                 //console.log(data);
                 var day = [];
                 var total = [];

                 for (var i in data) {
                     day.push(data[i].day);
                     total.push(data[i].total);
                 }

                 var chartdata = {

                     labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                     datasets: [{
                         label: 'orders',
                         backgroundColor: '#FE7117',
                         borderColor: '#ff6600',
                         hoverBackgroundColor: '#FE7417',
                         hoverBorderColor: '#FE7117',
                         data: total,

                     }]

                 };

                 var graphTarget = $("#adminC");

                 var barGraph = new Chart(graphTarget, {
                     type: 'line',
                     data: chartdata,
                     options: {
                         scales: {
                             yAxes: [{
                                 ticks: {
                                     beginAtZero: false
                                 }
                             }]
                         }
                     },
                 });

             });

         }

         function client_chart() {
             $.post('php_action/client_charts.php', function(data, textStatus, xhr) {
                 //console.log(data);
                 var day = [];
                 var total = [];

                 for (var i in data) {
                     day.push(data[i].day);
                     total.push(data[i].total);
                 }

                 var chartdata = {

                     labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                     datasets: [{
                         label: 'orders',
                         backgroundColor: '#FE7117',
                         borderColor: '#ff6600',
                         hoverBackgroundColor: '#FE7417',
                         hoverBorderColor: '#FE7117',
                         data: total,

                     }]

                 };

                 var graphTarget = $("#clientSales");

                 var barGraph = new Chart(graphTarget, {
                     type: 'bar',
                     data: chartdata,
                     options: {
                         scales: {
                             yAxes: [{
                                 ticks: {
                                     beginAtZero: true
                                 }
                             }]
                         }
                     },
                 });

             });

         }

         function customer_chart() {
             $.post('php_action/customers_charts.php', function(data, textStatus, xhr) {


                 var chartdata = {
                     labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                     datasets: [{
                         label: 'Customers',
                         backgroundColor: '#2CABE3',
                         borderColor: '#333333',
                         hoverBackgroundColor: '#002F47',
                         hoverBorderColor: '#FE7117',
                         data: data
                     }],
                     options: {
                         scales: {
                             yAxes: [{
                                 ticks: {
                                     beginAtZero: true
                                 }
                             }]
                         }
                     }
                 };

                 var graphTarget = $("#customersC");

                 var barGraph = new Chart(graphTarget, {
                     type: 'line',
                     data: chartdata,
                     options: {
                         scales: {
                             yAxes: [{
                                 ticks: {
                                     beginAtZero: true
                                 }
                             }]
                         }
                     },
                 });

             });

         }



         function order_charts() {

             $.post('php_action/order_charts.php', function(data, textStatus, xhr) {

                 var chartdata = {
                     labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                     datasets: [{
                         label: 'Orders',
                         backgroundColor: '#FFC36D',
                         borderColor: '#FF7676',
                         hoverBackgroundColor: '#002F47',
                         hoverBorderColor: '#FE7117',
                         data: data
                     }],
                     options: {
                         scales: {
                             yAxes: [{
                                 ticks: {
                                     beginAtZero: true
                                 }
                             }]
                         }
                     }
                 };

                 var graphTarget = $("#ordersCh");

                 var barGraph = new Chart(graphTarget, {
                     type: 'line',
                     data: chartdata,
                     options: {
                         scales: {
                             yAxes: [{
                                 ticks: {
                                     beginAtZero: true
                                 }
                             }]
                         }
                     },
                 });

             });

         }



         function combined() {
             $.post('php_action/combined.php', function(data, textStatus, xhr) {



                 var chartdata = {
                     labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                     datasets: [{
                             label: 'Expenses',
                             backgroundColor: '#FF7676',
                             borderColor: '#FF7676',
                             hoverBackgroundColor: '#002F47',
                             hoverBorderColor: '#FE7117',
                             data: data.expenses,
                             yAxisID: "y-axis-1"
                         }, {
                             label: 'Revenues',
                             backgroundColor: '#2CABE3',
                             borderColor: '#445da2',
                             hoverBackgroundColor: '#ffcc05',
                             hoverBorderColor: '#FE7117',
                             data: data.revenues,
                             yAxisID: "y-axis-2"
                         }

                     ],

                 };

                 var graphTarget = $("#chart2");

                 var barGraph = new Chart(graphTarget, {
                     type: 'bar',
                     data: chartdata,
                     options: {
                         scales: {

                             yAxes: [{
                                 type: "linear",
                                 display: true,
                                 position: "left",
                                 id: "y-axis-1",
                                 ticks: {
                                     beginAtZero: true
                                 }
                             }, {
                                 type: "linear",
                                 display: true,
                                 position: "right",
                                 id: "y-axis-2",
                                 ticks: {
                                     beginAtZero: true
                                 }
                             }],

                         }
                     },
                 });

             });

         }


         //pie chart



              


       

         //end of functions
     });