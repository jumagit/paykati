<?php include "a_includes/header.php";?>
<?php include "a_includes/topNav.php";?>
<?php include "a_includes/sideNav.php";?>

<style>
    
    td{
        vertical-align: middle !important;
    }
    td p{
        margin: unset
    }
    
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="col-lg-12">
            <br>
        </div>
        <div class="row">
            <!-- FORM Panel -->
            <div class="col-md-4">
            <form action="" id="PlanCreate">
                <div class="panel">
                    <div class="panel-heading">
                           Plan's Form
                    </div>
                    <div class="panel-body">
                            <input type="hidden" name="id">
                            <div class="form-group">
                                <label class="control-label">Plan (months)</label>
                                <input type="text" name="months" id="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Interest</label>
                                <div class="input-group">
                                  <input type="text"  min="0" max="100" class="form-control " name="interest_percentage" aria-label="Interest">
                                  <div class="input-group-append">
                                    <span class="input-group-text">%</span>
                                  </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Monthly Over due's Penalty</label>
                                <div class="input-group">
                                  <input type="text"  min="0" max="100" class="form-control " aria-label="Penalty percentage" name="penalty_rate">
                                  <div class="input-group-append">
                                    <span class="input-group-text">%</span>
                                  </div>
                                </div>
                            </div> 
                    </div>
                            
                    <div class="panel-footer">           
                       <button class="btn btn-sm btn-primary "> Save</button> 
                    </div>
                </div>
            </form>
            </div>
            <!-- FORM Panel -->

            <!-- Table Panel -->
            <div class="col-md-8">
                <div class="panel">
                    <div class="panel-body">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Plan</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $i = 1;
                                $plan = query("SELECT * FROM loan_plan order by id asc");
                                while($row=$plan->fetch_assoc()):
                                    $months = $row['months'];
                                    $months = $months / 12;
                                    if($months < 1){
                                        $months = $row['months']. " months";
                                    }else{
                                        $m = explode(".", $months);
                                        $months = $m[0] . " yrs.";
                                        if(isset($m[1])){
                                            $months .= " and ".number_format(12 * ($m[1] /100 ),0)."month/s";
                                        }
                                    }
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo $i++ ?></td>
                                    <td class="">
                                         <p>Years/Month: <b><?php echo $months ?></b></p>
                                         <p><small>Interest: <b><?php echo $row['interest_percentage']."%" ?></b></small></p>
                                         <p><small>Over dure Penalty: <b><?php echo $row['penalty_rate']."%" ?></b></small></p>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-sm btn-primary edit_plan" type="button" data-id="<?php echo $row['id'] ?>" data-months="<?php echo $row['months'] ?>" data-interest_percentage="<?php echo $row['interest_percentage'] ?>" >Edit</button>
                                        <button class="btn btn-sm btn-danger delete_plan" type="button" data-id="<?php echo $row['id'] ?>">Delete</button>
                                    </td>
                                </tr>
                                <?php endwhile; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>        
    </div>   
</div>


<script>
    $('form#PlanCreate').on("submit", function(e) {
        e.preventDefault();

        var formData = new FormData(this);

        swal({
            title: "Are you sure?",
            text: "Okay to add a  New Receipient",
            type: "info",
            padding: 20,
            showCancelButton: true,
            confirmButtonColor: "#384888",
            confirmButtonText: "Yes, create!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "php_action/create_plans.php?t=true",
                    data: formData,
                    success: function(result) {

                        if (result.status) {
                            swal({
                                title: "Good job!",
                                padding: 20,
                                text: "Good Job! A New plan has been Created Successfully!",
                                type: "success"
                            });

                            setTimeout(function() {
                                window.location.reload();
                            }, 2000);
                        } else {
                            swal({
                                title: "Oops!",
                                padding: 20,
                                text: result.msg + "..please try again!",
                                type: "warning"
                            });
                        }
                    },
                    error: function(jqXHR) {
                        console.log(jqXHR);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });
    });
</script>


<?php include "a_includes/footer.php";?>