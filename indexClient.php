<?php  include "includes/header.php"; ?>
<?php  include "includes/topNav.php"; ?>
<?php  include "includes/sideNav.php"; ?>
<?php 

$customer_id = $_SESSION['client_id'];
$ac_details = get_overall_client_details($customer_id);


 ?>
<div id="page-wrapper">
    <div class="container-fluid ">
        <div class="row">
            <div class="col-lg-12">
                <br>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-3 col-xs-6">
                <div class="panel ">
                    <div class="inner">

                        <h3><?php  echo count_members($_SESSION['code']);?></h3>
                        <p>Receipients</p>
                    </div>
                    <div class="text-center"> <img height="80" width="80" src="assets/images/customers.png"> </div>

                    <div class="panel-footer">
                        <a href="receipients.php" class="btn btn-sm btn-info">View More</a>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-xs-6">
                <div class="panel ">
                    <div class="inner">
                        <h3><?php echo (int)$ac_details['balance']; ?></h3>
                        <p>Balance</p>
                    </div>
                    <div class="text-center"> <img height="80" width="80" src="assets/images/wallet.png"> </div>

                    <div class="panel-footer">
                        <a href="client_deposit.php" class="btn btn-sm btn-info">View More</a>
                    </div>

                </div>
            </div>

            <div class="col-lg-3 col-xs-6">
                <div class="panel ">
                    <div class="inner">
                        <h3><?php echo (int)$ac_details['transfers']; ?></h3>
                        <p>Total Transferred</p>
                    </div>
                    <div class="text-center"> <img height="80" width="80" src="assets/images/fair.png"> </div>

                    <div class="panel-footer">
                        <a href="client_deposit.php" class="btn btn-sm btn-info">View More</a>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-xs-6">
                <div class="panel ">
                    <div class="inner">
                        <h3><?php echo (int)$ac_details['withdraws']; ?></h3>
                        <p>Total Withdrawn</p>
                    </div>
                    <div class="text-center"> <img height="80" width="80" src="assets/images/fair.png"> </div>

                    <div class="panel-footer">
                        <a href="client_withdraw.php" class="btn btn-sm btn-info">View More</a>
                    </div>

                </div>
            </div>


        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        TRANSACTIONS
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive text-left">
                            <table class="table table-striped table-bordered" id="myTable">
                                <thead>
                                    <tr>
                                        <th>S/N</th>
                                        <th>Description</th>
                                        <th>Type</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php echo mega_trans_logs_fetch($_SESSION['client_id'], $ac_details['account_number']); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <style>
        .panel {

            text-align: center;
        }

        .inner {

            text-align: center;
            padding-top: 20px;
        }
        </style>

        <?php   include "includes/footer.php"; ?>