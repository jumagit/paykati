<?php  include "a_includes/header.php"; ?>
<?php  include "a_includes/topNav.php"; ?>        
<?php  include "a_includes/sideNav.php"; ?>

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Clients</h4> 
                    </div>  
                </div>
                <div class="row">
                     <div class="col-sm-12">
                        <div class="white-box">                           
                            <div class="table-responsive">
                            <table id="myTables" class="table table-striped table-bordered dt-responsive "
                               >
                                <thead >
                                    <tr>
                                        <th>#</th>
                                        <th>FULLNAME</th>
                                        <th>USERNAME</th>
                                        <th>MOBILE</th>
                                        <th>LOCATION</th>
                                        <th>ACCOUNT STATUS</th>
                                        <th colspan="4">
                                            ACTIONS
                                    </th>
                                    </tr>
                                </thead>


                                <tbody>
                                  <?php echo fetch_clients(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </div>
    <!-- end page content-->

</div>
<!-- page wrapper end -->



<!-- Footer -->
<?php include "a_includes/footer.php"; ?>