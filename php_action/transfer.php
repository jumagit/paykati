<?php

include "check_if_logged_in.php";

require "../includes/db.php";

if ($_REQUEST['t'] == 'true') {

    //getting sender details
    $date = date('Y-m-d H:m:s');
    $transferamount =  clean($_POST['tamount']);
    $transferamount = (int) $transferamount;
    $receiver = clean($_POST['receiver']);       
    $client_id = $_SESSION['client_id']; //sender_id
    $sender_details =  get_account_details($client_id); 
    $sender_balance = (int) $sender_details['balance'];
    $sender_account = clean($sender_details['account_number']);
    $sender_name    = clean($sender_details['account_name']);
    accountability($client_id,'transfer',$transferamount);

    //getting reciever details

    $receiver_details = get_overall_client_details($receiver);
    $reciever_account = clean($receiver_details['account_number']);
    $receiver_name = clean($receiver_details['fullName']);
    $receiver_balance = clean($receiver_details['balance']);
    accountability($receiver,'transfer',$transferamount);

    //subtracting sent amount from sender account

    if ($sender_balance > $transferamount) {

        $from_query = query("UPDATE client_account SET balance=balance-$transferamount WHERE client_id='$client_id' AND account_number='$sender_account'");

        //recording the details to the sender account
        $desc = "Transfer withdraw of <b>" . number_format($transferamount) . "</b>  from Account (<b>" . $sender_account . "</b>) by " . $sender_name . " on " . $date;

        $sql_cash_log = query("INSERT INTO mega_trans_logs (account_id,trans_type,record_date, trans_date, trans_amount, description, member, client_id)
     VALUES('$sender_account', 'Transfer', '$date', '$date', '$transferamount', '$desc', '$sender_name', '$client_id') ") or die(mysqli_error($connection));

        //adding sent amount to the receiver account

        $to_query = query("UPDATE client_account SET balance=balance+$transferamount WHERE client_id='$receiver' AND account_number = '$reciever_account' ");

        //recording the details to the receiver account
        $detail = "Received <b>" . number_format($transferamount) . "</b>  from  Account (<b>" . $sender_account . "</b>) by " . $sender_name . " on " . $date;

        $sql_cash_log2 = query("INSERT INTO mega_trans_logs (account_id,trans_type,record_date, trans_date, trans_amount, description, member, client_id)
        VALUES('$reciever_account', 'Transfer', '$date', '$date', '$transferamount', '$detail', '$sender_name', '$receiver') ") or die(mysqli_error($connection));

        if ($from_query and $to_query) {

            $feed_back = array('status' => true, 'msg' => 'success');

        } else {

            $feed_back = array('status' => false, 'msg' => mysqli_error($connection));
        }

        $dataX = json_encode($feed_back);
        header('Content-Type: application/json');
        echo $dataX;

    } else {

        $feed_back = array('status' => false, 'msg' => "You don't have Enough Credit on Your Account,
    Please Recharge first and Try Again...");

        $dataX = json_encode($feed_back);
        header('Content-Type: application/json');
        echo $dataX;

    }

}
