<?php

include "check_if_logged_in.php";

require "../includes/db.php";

if ($_REQUEST['t'] == 'true') {

    $withdrawAmount = clean($_POST['withdrawAmount']);
    $withdrawnBy = clean($_POST['withdrawnBy']);
    $account_number = clean($_POST['account_number']);
    $client_id = clean($_SESSION['client_id']);
    $date = date('Y-m-d H:m:s');
    $balance = clean($_POST['balance']);
    $balance = (int) $balance;
    $withdrawAmount = (int) $withdrawAmount;

    if ($balance < $withdrawAmount) {

        $feed_back = array('status' => false, 'msg' => "You don't have Enough Credit on Your Account,
    Please Recharge first and Try Again...", );

    } else {

        accountability($client_id,'withdraw',$withdrawAmount);

        $sql_bank = query("UPDATE client_account SET balance=balance-$withdrawAmount WHERE client_id='$client_id'   ") or die(mysqli_error($connection));

        $description = "Withdraw of " . number_format($withdrawAmount) . " from  Account (" . $account_number . ") by " . get_client_name($withdrawnBy) . " on " . f_date($date);

        $sql_cash_log = query("INSERT INTO mega_trans_logs (account_id,trans_type,record_date, trans_date, trans_amount, description, member, client_id)
    VALUES('$account_number', 'Withdraw', '$date', '$date', '$withdrawAmount', '$description', '$withdrawnBy', '$client_id') ") or die(mysqli_error($connection));

        if ($sql_bank) {
            $feed_back = array('status' => true, 'msg' => 'success');
        } else {
            $feed_back = array('status' => false, 'msg' => mysqli_error($connection));
        }

    }

    $dataX = json_encode($feed_back);
    header('Content-Type: application/json');
    echo $dataX;
    $connection->close();

}
