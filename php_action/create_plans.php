<?php

include "check_if_logged_in.php";
require "../includes/db.php";

if ($_REQUEST['t'] == 'true') {
    $months = $_POST['months'];
    $interest_percentage = $_POST['interest_percentage'];
    $penalty_rate = $_POST['penalty_rate'];
    $sql_select = query("SELECT months FROM loan_plan WHERE months = '" . $months . "'") or die(mysqli_error($connection));
    $count = mysqli_num_rows($sql_select);
    if ($count == 0) {
        $sql = "INSERT INTO loan_plan (months, interest_percentage, penalty_rate)
	VALUES ('$months', '$interest_percentage', '$penalty_rate')";
        $query = query($sql) or die(mysqli_error($connection));
        if ($query) {
            $feed_back = array('status' => true, 'msg' => 'success');
        } else {
            $feed_back = array('status' => false, 'msg' => mysqli_error($connection));
        }
    } else {
        $feed_back = array('status' => false, 'msg' => "Plan " . strtoupper($penalty_rate) . "  already present in the table");
    }
    $dataX = json_encode($feed_back);
    header('Content-Type: application/json');
    echo $dataX;
    $connection->close();

} // /if $_POST

if ($_REQUEST['t'] == 'type') {
    $type_name = $_POST['type_name'];
    $description = $_POST['description'];
    $sql_select = query("SELECT type_name FROM loan_types WHERE type_name = '" . $type_name . "'") or die(mysqli_error($connection));
    $count = mysqli_num_rows($sql_select);
    if ($count == 0) {
        $sql = "INSERT INTO loan_types (type_name,description)
	VALUES ('$type_name', '$description')";
        $query = query($sql) or die(mysqli_error($connection));
        if ($query) {
            $feed_back = array('status' => true, 'msg' => 'success');
        } else {
            $feed_back = array('status' => false, 'msg' => mysqli_error($connection));
        }
    } else {
        $feed_back = array('status' => false, 'msg' => "Plan " . strtoupper($type_name) . "  already present in the table");
    }
    $dataX = json_encode($feed_back);
    header('Content-Type: application/json');
    echo $dataX;
    $connection->close();
}

if ($_REQUEST['t'] == 'save_loan') {

    $ref_no = mt_rand(1, 99999999);
    $borrower_id = clean($_SESSION['client_id']);
    $loan_type_id = clean($_POST['loan_type_id']);
    $plan_id = clean($_POST['plan_id']);
    $amount = clean($_POST['amount']);
    $purpose = clean($_POST['purpose']);  
    $date = clean(date('Y-m-d'));

    $sql = "INSERT INTO loan_list (ref_no ,loan_type_id ,borrower_id , 	purpose ,amount ,plan_id ,status, 	date_released )
	VALUES ('$ref_no', '$loan_type_id','$borrower_id','$purpose','$amount','$plan_id',0,'$date')";
    $query = query($sql) or die(mysqli_error($connection));
    if ($query) {
        $feed_back = array('status' => true, 'msg' => 'success');
    } else {
        $feed_back = array('status' => false, 'msg' => mysqli_error($connection));
    }

    $dataX = json_encode($feed_back);
    header('Content-Type: application/json');
    echo $dataX;
    $connection->close();
}



if ($_REQUEST['t'] == 'approve') {
    $ref = $_GET['ref'];  
    $query = query("UPDATE  loan_list SET status = 1 WHERE  ref_no  = '$ref' ");
    if ($query) {
        $feed_back = array('status' => true, 'msg' => 'success');
    } else {
        $feed_back = array('status' => false, 'msg' => mysqli_error($connection));
    }
    $dataX = json_encode($feed_back);
    header('Content-Type: application/json');
    echo $dataX;
}




if ($_REQUEST['t'] == 'delete') {
    $id = $_GET['id'];
    $query = query("DELETE FROM loan_plan WHERE loan_plan_id='{$id}' AND client_id='$client_id' ");

    $products = query("DELETE FROM products WHERE loan_plan_id = '{$id}'  AND client_id='$client_id'  ");
    if ($query && $products) {
        $feed_back = array('status' => true, 'msg' => 'success');
    } else {
        $feed_back = array('status' => false, 'msg' => mysqli_error($connection));
    }
    $dataX = json_encode($feed_back);
    header('Content-Type: application/json');
    echo $dataX;
}

if ($_REQUEST['t'] == 'edit') {

    if (isset($_SESSION['fullName'])) {
        $created_by = $_SESSION['fullName'];
        $client_id = $_SESSION['client_id'];
    }

    $category_name = clean($_POST['loan_plan_name']);
    $loan_plan_id = clean($_POST['loan_plan_id']);

    $sql = "UPDATE loan_plan SET loan_plan_name  = '$category_name' WHERE loan_plan_id = '$loan_plan_id' AND client_id= '$client_id'  ";

    $query = query($sql) or die(mysqli_error($connection));

    if ($query) {
        $feed_back = array('status' => true, 'msg' => 'success');
    } else {
        $feed_back = array('status' => false, 'msg' => mysqli_error($connection));
    }

    $dataX = json_encode($feed_back);
    header('Content-Type: application/json');
    echo $dataX;

    $connection->close();

} // /if $_POST
