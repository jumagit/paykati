<?php

include "check_if_logged_in.php";

require "../includes/db.php";

if ($_REQUEST['t'] == 'true') {

    if (isset($_SESSION['user_id'])) {

        function generatePIN($digits = 3)
        {
            $i = 0;
            $pin = "";
            while ($i <= $digits) {
                $pin .= mt_rand(0, 9);
                $i++;
            }
            return $pin;
        }

        $ccd = "";
        $created_by = clean($_SESSION['user_id']);
        $fullName = clean($_POST['fullName']);
        $username = clean($_POST['username']);
        $location = clean($_POST['location']);
        $mobile = clean($_POST['mobile']);
        $email = clean($_POST['email']);
        $password = generatePassword();
        $code = generatePIN();
        $newpass = md5($password);
        $accountname = clean($_POST['accountname']);
        $accountno = clean($_POST['accountno']);
        $mobile_money_number = clean($_POST['mobile_money_number']);

        $sql_select = query("SELECT email FROM clients WHERE email = '$email'  ") or die(mysqli_error($connection));

        $count = mysqli_num_rows($sql_select);

        if ($count == 0) {

            $sql = "INSERT INTO clients (code,fullName,email,mobile,location,username,password,cpassword,created_by)
        VALUES ('$code','$fullName', '$email', '$mobile', '$location', '$username', '$newpass','$password', '$created_by')";

            $create_client = query($sql);

            $last_id = mysqli_insert_id($connection);

            $insert_account = query("INSERT INTO client_account(account_name, account_number,mobile_money_number, balance, status, client_id)
              VALUES( '$accountname', '$accountno','$mobile_money_number', '0', '1', '$last_id')");

             $accountability = query("INSERT INTO accountability (client_id) VALUES('$last_id')");

            //create clients
            writeLog("Created a New Client Account  {$fullName}", $_SESSION['username'], "SUCCESS");

            if ($insert_account) {
                $feed_back = array('status' => true, 'msg' => 'success');
            } else {
                $feed_back = array('status' => false, 'msg' => mysqli_error($connection));
            }

            $client_body_body_message = "
                   <p>&nbsp;</p>
                   <p>Dear {$fullName},</p>
                   <p>Please, use the following credentials to access the bank System</p>
                   <p>Username : <strong>{$username}</strong></p>
                   <p>Password : <strong>{$password}</strong></p>
                   <p>It is recomended that you change the password after logon.</p>
                   <p>&nbsp;</p>
                   <p><a href='" . URL . "index.php'>To login into your account, please follow this link. </a></p>
                   <p>&nbsp;</p>
                   <p>Thank you</p>
                   <p><small>The payKati Support Team</small></p>

                   <p>&nbsp;</p>";

            $account = sendMailM($email, 'Account Creation', $client_body_body_message);

            $dataX = json_encode($feed_back);
            header('Content-Type: application/json');
            echo $dataX;

            $connection->close();

        } else {

            $feed_back = array('status' => false, 'msg' => 'The account already exists, please try with a different email address...');
            $dataX = json_encode($feed_back);
            header('Content-Type: application/json');
            echo $dataX;
            $connection->close();
        }

    }

}

if ($_REQUEST['t'] == 'delete') {
    $id = $_GET['id'];
    $query = query("DELETE FROM clients WHERE client_id='{$id}'");
    if ($query) {
        $feed_back = array('status' => true, 'msg' => 'success');
    } else {
        $feed_back = array('status' => false, 'msg' => mysqli_error($connection));
    }
    $dataX = json_encode($feed_back);
    header('Content-Type: application/json');
    echo $dataX;
}

if ($_REQUEST['t'] == 'activateAccount') {
    $id = $_GET['id'];
    $query = query("UPDATE  clients  SET account_status = '1' WHERE client_id='{$id}'");
    if ($query) {
        $feed_back = array('status' => true, 'msg' => 'success');
    } else {
        $feed_back = array('status' => false, 'msg' => mysqli_error($connection));
    }
    $dataX = json_encode($feed_back);
    header('Content-Type: application/json');
    echo $dataX;
}

if ($_REQUEST['t'] == 'deactivateAccount') {
    $id = $_GET['id'];
    $query = query("UPDATE  clients  SET account_status = '0' WHERE client_id='{$id}'");
    if ($query) {
        $feed_back = array('status' => true, 'msg' => 'success');
    } else {
        $feed_back = array('status' => false, 'msg' => mysqli_error($connection));
    }
    $dataX = json_encode($feed_back);
    header('Content-Type: application/json');
    echo $dataX;
}
