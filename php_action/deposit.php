<?php

include "check_if_logged_in.php";

require "../includes/db.php";


if ($_REQUEST['t'] == 'true') {
    
    $nameOfDepositor = clean($_POST['nameOfDepositor']);
    $depositAmount = clean($_POST['depositAmount']);
    $account_number = clean($_POST['account_number']);
    $balance = clean($_POST['balance']);
    $client_id = $_SESSION['client_id'];    
    $date = date('Y-m-d H:m:s');
    $sql_bank = query("UPDATE client_account SET balance=balance+$depositAmount WHERE client_id='$client_id'  ") or die(mysqli_error($connection));
//recording deposit
       accountability($client_id,'deposit',$depositAmount);
    
    $description = "Deposit of " . number_format($depositAmount) . " to Account (" . $account_number . ") by " . $nameOfDepositor . " on " . f_date($date);
    $sql_cash_log = query("INSERT INTO mega_trans_logs (account_id,customer_id,trans_type,record_date, trans_date, trans_amount, description, member, client_id)
VALUES('$account_number','$client_id', 'Deposit', '$date', '$date', '$depositAmount', '$description', '$nameOfDepositor', '$client_id') ") or die(mysqli_error($connection));
    
    //confirm insertion
    if ($sql_bank) {
        $feed_back = array('status' => true, 'msg' => 'success');
    } else {
        $feed_back = array('status' => false, 'msg' => mysqli_error($connection));
    }

    $dataX = json_encode($feed_back);
    header('Content-Type: application/json');
    echo $dataX;
    $connection->close();

}

?>

