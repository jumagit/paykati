<?php  include "a_includes/header.php"; ?>
<?php  include "a_includes/topNav.php"; ?>
<?php  include "a_includes/sideNav.php"; ?>

<div id="page-wrapper">
    <div class="container-fluid ">
        <div class="row">
            <div class="container-fluid "> <br> </div>
        </div>



        <div class="row">

            <div class="col-lg-3 col-xs-6">
                <div class="panel ">
                    <div class="inner">
                        <h3><?php echo just_count_anything('users'); ?></h3>
                        <p>Administrators</p>
                    </div>
                    <div class="text-center"> <img height="80" width="80" src="assets/images/customers.png"> </div>

                    <div class="panel-footer">
                        <a href="users.php" class="btn btn-sm btn-info">View More</a>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-xs-6">
                <div class="panel ">
                    <div class="inner">
                        <h3><?php     echo just_count_anything('clients'); ?></h3>
                        <p>Registered Clients</p>
                    </div>
                    <div class="text-center"> <img height="80" width="80" src="assets/images/group.png"> </div>

                    <div class="panel-footer">
                        <a href="suppliers.php" class="btn btn-sm btn-info">View More</a>
                    </div>

                </div>
            </div>

            <div class="col-lg-3 col-xs-6">
                <div class="panel ">
                    <div class="inner">
                        <h3><?php     echo just_count_anything('loan_list'); ?></h3>
                        <p>Loan Applicantions</p>
                    </div>
                    <div class="text-center"> <img height="80" width="80" src="assets/images/fair.png"> </div>

                    <div class="panel-footer">
                        <a href="loan_lists.php" class="btn btn-sm btn-info">View More</a>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-xs-6">
                <div class="panel ">
                    <div class="inner">
                        <h3>1200</h3>
                        <p>Total Deposited</p>
                    </div>
                    <div class="text-center"> <img height="80" width="80" src="assets/images/wallet.png"> </div>

                    <div class="panel-footer">
                        <a href="suppliers.php" class="btn btn-sm btn-info">View More</a>
                    </div>

                </div>
            </div>


        </div>

        <style>
        .panel {

            text-align: center;
            padding-top: 20px !important;
        }

        .inner {

            text-align: center;
        }
        </style>

        <div class="row">

            <div class="col-md-12 col-lg-6 col-sm-12 col-xs-12">
                <div class="white-box">
                    <h3 class="box-title">RECENTLY REISTERED CLIENTS <a href="suppliers.php"><span
                                class="pull-right btn btn-sm btn-primary">View all</span></a></h3>

                    <hr>

                    <div class="table-responsive">
                        <table id="myTables" class="table table-striped table-bordered dt-responsive ">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>FULLNAME</th>
                                    <th>USERNAME</th>
                                    <th>MOBILE</th>
                                    <th>LOCATION</th>

                                </tr>
                            </thead>


                            <tbody>
                                <?php echo fetch_clients_on_dashboard(); ?>
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>



            <div class="col-md-12 col-lg-6 col-sm-12 col-xs-12">
                <div class="white-box">
                    <h3 class="box-title">ANALYTICS</h3>

                    <canvas id="pieC" height="230"></canvas>

                </div>
            </div>
        </div>
    </div>
</div>


<?php   include "a_includes/footer.php"; ?>