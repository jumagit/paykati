<?php  include "a_includes/header.php"; ?>
<!-- ============================================================== -->
<?php  include "a_includes/topNav.php"; ?>

<!-- End Top Navigation -->

<?php  include "a_includes/sideNav.php"; ?>
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-3 col-md-3 col-xs-12">
                <h4 class="page-title"> ADD SYSTEM USER</h4>
            </div>
        </div>
        <!-- /row -->
        <div class="row">

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <form action="" id="addUserForm">
                                <div class="form-group ">
                                    <label for="sname">FirstName :</label>
                                    <input type="text" name="sname" class="form-control" placeholder="Enter FirstName"
                                        required>
                                </div>


                                <div class="form-group ">


                                    <label for="lname">LastName :</label>

                                    <input type="text" name="lname" class="form-control" placeholder="Enter LastName"
                                        required>

                                </div>


                                <div class="form-group ">

                                    <label for="username">Username :</label>

                                    <input type="text" name="username" class="form-control" placeholder="Enter Username"
                                        required>

                                </div>

                                <div class="form-group ">

                                    <label for="email">Email :</label>

                                    <input type="email" name="email" class="form-control" parsley-type="email" required
                                        placeholder="Enter Email Address">

                                </div>


                                <div class="form-group ">

                                    <label for="mobile">Phone :</label>

                                    <input type="number" name="mobile" class="form-control" data-parsley-type="number"
                                        placeholder="Enter Phone Contact" required data-parsley-minlength="10"
                                        data-parsley-maxlength="10" />

                                </div>





                                <div class="form-group ">

                                    <label for="gender">Gender :</label>

                                    <select name="gender" class="form-control">
                                        <option value="#" selected="selected" disabled="disabled"> ~~ Select
                                            Gender ~~
                                        </option>
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                    </select>

                                </div>




                                


                                <div class="form-group">

                                    <button type="submit" class="btn  btn-info btn-block waves-effect waves-light">
                                        Create User
                                    </button>

                                </div>

                            </form>



                        </div>




                    </div>
                </div>
            </div>
        </div>

        <!--./row-->
    </div>

</div>


<script>
    jQuery(document).ready(function() {

        $(document).on("submit", "#addUserForm", function(e) {
          e.preventDefault();

          var formData = new FormData(this);

          swal({
               title: "Are you sure?",
               text: "Okay to Add New User",
               type: "info",
               padding: 20,             
              showCancelButton: true,   
              confirmButtonColor: "#ffdd00",   
              confirmButtonText: "Yes, Add!",   
              cancelButtonText: "No, cancel plz!",   
              closeOnConfirm: false,   
              closeOnCancel: false 
          }, function(isConfirm){
               if (isConfirm) {
                    $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_users.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A New User has been Created Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.href = "users.php";
                                   }, 3000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
               }
          });
     });



    })
</script>
<?php include "a_includes/footer.php";?>