<?php  include "includes/header.php"; ?>
<!-- ============================================================== -->
<?php  include "includes/topNav.php"; ?>

<!-- End Top Navigation -->

<?php  include "includes/sideNav.php"; ?>

<?php 

       
            $client_id = $_SESSION['client_id'];
            $sql = "SELECT * FROM clients WHERE client_id = '$client_id' ";
            $query = query($sql);
            while($result = mysqli_fetch_array($query)){
                  $client_id = $result['client_id'];
                  $username = $result['username'];
                  $fullName = $result['fullName'];
                  $email = $result['email'];
                  $location = $result['location'];
                  $Mobile = $result['mobile'];
                 
                  $profilePicture  = substr($result['profileImage'], 3);
            }

           // 



     ?>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Profile SECTION</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">




            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="white-box">
                    <div class="user-bg"> <img width="100%" alt="user" src="../plugins/images/large/img1.jpg">
                        <div class="overlay-box">
                            <div class="user-content">
                                <a href="javascript:void(0)"><img
                                        src="<?php echo(isset($profilePicture)) ?  $profilePicture : 'No Profile Pic' ?>"
                                        class="thumb-lg img-circle" alt="img"></a>
                                <h4 class="text-white"> <?php echo $_SESSION['fullName'] ?></h4>
                                <h5 class="text-white"> <?php echo $_SESSION['email'] ?></h5>
                            </div>
                        </div>
                    </div>

                </div>

                <hr>
                <form action="#" method="POST" id="changeProfilePicture" class="mt-2" enctype="multipart/form-data">
                    <div class="form-group">

                        <div>
                            <input type="file" class="form-control" name="ProfileImage" required />
                            <input type="hidden" name="pic_client_id" value="<?php echo $client_id; ?>">
                        </div>
                    </div>
                    <button class="text-5 text-light btn btn-block  btn-primary"><i class="fa fa-image"></i>
                        Update</button>
                </form>






            </div>
            <div class="col-md-8 col-xs-12">
                <div class="white-box">
                    <ul class="nav nav-tabs tabs customtab">
                        <li class="active tab">
                            <a href="#home" data-toggle="tab"> <span class="visible-xs"><i
                                        class="fa fa-home"></i></span> <span class="hidden-xs">Profile</span> </a>
                        </li>
                        <li class="tab">
                            <a href="#profile" data-toggle="tab"> <span class="visible-xs"><i
                                        class="fa fa-user"></i></span> <span class="hidden-xs">Password </span> </a>
                        </li>
                        <li class="tab">
                            <a href="#messages" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i
                                        class="fa fa-envelope-o"></i></span> <span class="hidden-xs">Username</span>
                            </a>
                        </li>
                        <li class="tab">
                            <a href="#settings" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i
                                        class="fa fa-cog"></i></span> <span class="hidden-xs">Update Profile</span> </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="home">
                            <table class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <th>
                                            FULLNAME
                                        </th>
                                        <td>
                                            <p class="col-sm-9 h5 text-info"><u><?php echo $fullName; ?></u></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            USERNAME
                                        </th>
                                        <td>
                                            <p class="col-sm-9 h5 text-info"><u><?php echo $username; ?></u></p>
                                        </td>

                                    </tr>
                                    <tr>
                                        <th>
                                            MOBILE PHONE
                                        </th>
                                        <td>
                                            <p class="col-sm-9 h5 text-info"><u><?php echo $Mobile; ?></u></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            EMAIL ADDRESS
                                        </th>
                                        <td>
                                            <p class="col-sm-9 h5 text-info"><u><?php echo $email; ?></u></p>
                                        </td>
                                    </tr>

                                    <tr>

                                        <th>
                                            LOCATION
                                        </th>
                                        <td>
                                            <p class="col-sm-9 h5 text-info"><u><?php echo $location; ?></u></p>
                                        </td>

                                    </tr>

                                </tbody>
                            </table>
                        </div>

                        <div class="tab-pane" id="profile">
                            <form action="" id="changePassword">
                                <div class="form-group ">
                                    <label for="currentPassword">Current Password :</label>

                                    <input type="password" name="currentPassword" class="form-control"
                                        placeholder="Enter your Old Password" required>

                                </div>

                                <div class="form-group ">

                                    <label for="password">New Password :</label>

                                    <input type="password" name="newPassword" class="form-control" id="pass2"
                                        placeholder="Enter your Favourite Password" required>

                                </div>

                                <div class="form-group ">

                                    <label for="confirmPassword">Confirm Password :</label>

                                    <input type="password" name="confirmPassword" data-parsley-equalto="#pass2"
                                        class="form-control" placeholder="Confirm Your Chosen Password" required>

                                </div>
                                <div class="form-group ">
                                    <input type="hidden" name="pclient_id" id="pclient_id"
                                        value="<?php echo $client_id; ?>" />
                                    <button type="submit" class="btn btn-block btn-primary waves-effect waves-light">
                                        <i class="mdi mdi-content-save"></i> Change Password
                                    </button>
                                </div>

                            </form>
                        </div>


                        <div class="tab-pane" id="messages">

                            <form action="" id="changeUsername">

                                <div class="form-group ">

                                    <label for="username">Username :</label>

                                    <input type="hidden" name="client_id" id="client_id"
                                        value="<?php echo $client_id ?>" />
                                    <input type="text" name="username" required class="form-control"
                                        value="<?php echo $username; ?>">

                                </div>

                                <div class="form-group ">

                                    <button type="submit" class="btn btn-primary  waves-effect btn-block waves-light">
                                        <i class="fa fa-save"></i> Change Username
                                    </button>
                                </div>


                            </form>

                        </div>



                        <?php

                                    if (isset($_POST['update'])) {
                                       
                                        $E_mobile = clean($_POST['mobile']);
                                        $E_email = clean($_POST['email']);
                                        $E_location = clean($_POST['location']);
                                        $E_fullName = clean($_POST['fullName']);  
                                        $E_username = clean($_POST['username']);   
                                       
                                        $E_client_id = clean($_POST['client_id']);


                                       

                                            if (!empty($E_mobile) && !empty($E_location) && !empty($E_username)) {

                                                $sql = "UPDATE clients SET mobile = '$E_mobile', email = '$E_email', fullName = '$E_fullName',
                                                username = '$E_username', location = '$E_location' WHERE client_id = '$E_client_id' ";
                                        
                                                $update_query = query($sql);
                                        
                                                if ($update_query) {
                                                    writeLog("Made an edit on the client's information {$E_fullName}  from {$IP}", $_SESSION['client_id'], "INFO");
                                        
                                                    $msg = '
                                                        <div class="col-lg-12">
                                                            <div class="alert alert-success ">
                                                                <button type="button" class="close" data-dismiss="alert">×</button> <strong>Well done!</strong> Saving changes! please wait ..............
                                                            </div>
                                                            <script type="text/javascript">setTimeout(function() { window.location.href = "pp.php";}, 2000);</script>
                                                         </div>
                                                        ';
                                                } else {
                                                    die(mysqli_error($connection));
                                                }
                                        
                                            }


                                    }

                                    ?>




                        <div class="tab-pane" id="settings">

                            <?php if(isset($msg)){echo $msg;} ?>


                            <form action="" method="POST" class="pt-2">

                                <div class="form-group row">

                                    <div class="col-md-3">
                                        <label for="username">Username :</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="username" class="form-control"
                                            value="<?php  echo $username; ?>">

                                        <input type="hidden" name="client_id" value="<?php echo $client_id; ?>">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label for="productPrice">Email :</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="email" name="email" class="form-control"
                                            value="<?php  echo $email; ?>">
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label for="username">Mobile :</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="mobile" class="form-control"
                                            value="<?php  echo $Mobile; ?>">
                                    </div>
                                </div>





                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label for="suppliercode">Location :</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="location" class="form-control"
                                            value="<?php echo $location; ?>">
                                    </div>
                                </div>



                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label for="fullName">FullNames :</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="fullName" class="form-control"
                                            value="<?php echo $fullName; ?>">
                                    </div>
                                </div>



                                <hr>

                                <div class="form-group">

                                    <button type="submit" class="btn btn-primary btn-block" name="update">
                                        <i class="mdi mdi-content-save"></i> Update Profile
                                    </button>

                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- ============================================================== -->
        <?php include "includes/footer.php"; ?>