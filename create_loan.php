<?php include "includes/header.php";?>

<?php include "includes/topNav.php";?>

<?php include "includes/sideNav.php";?>



<?php
$customer_id = $_SESSION['client_id'];
$ac_details = get_account_details($customer_id);
$acc_date = f_date($ac_details['created_at']);
$acc_name = $ac_details['account_name'];
$acc_number = $ac_details['account_number'];
$acc_id = $ac_details['id'];
$acc_balance = $ac_details['balance'];
$name = $ac_details['account_name'];
?>


<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4 class="page-title">LOAN APPLICATION </h4>
            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row">

            <div class="col-lg-8">
                <form class="form-horizontal" action="../transactions/member_loan" method="post" autocomplete="off">
                    <input type="hidden" name="csrfmiddlewaretoken" value="MQGyErRGAubSnIfGQ6e1tQbVQdiKUQ0R">
                    <div class="form-group" style="display: block;">
                        <label for="account_name" class="col-sm-3 control-label">Account*</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                <input type="text" class="form-control" name="account_name"
                                    value="<?php echo $acc_number; ?>" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1" class="col-sm-3 control-label">Loan Type*</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-check-circle"></i></div>
                                <select class="form-control input-lg" name="loan_type" required="">
                                    <option>--Loan Type--</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1" class="col-sm-3 control-label">Rate Type*</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-check-circle"></i></div>
                                <select class="form-control input-lg" name="rate_type" required="">
                                    <option>--Rate Type--</option>
                                    <option value="Flat">Flat rate</option>
                                    <option value="Reducing">Reducing balance rate</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1" class="col-sm-3 control-label">Amount*</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-credit-card-alt"></i></div>
                                <input type="text" class="form-control" name="amount"
                                    onchange="return get_rate(this.value);" placeholder="Requested Amount"
                                    data-type="currency">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1" class="col-sm-3 control-label">Rate (%)*</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-credit-card-alt"></i></div>
                                <input type="text" class="form-control number" name="rate"
                                    onchange="return get_rate(this.value);" placeholder="Rate (%)"
                                    id="setinterestrate<?php echo $row['code'];?>">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1" class="col-sm-3 control-label">Payment
                            Duration*</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" class="form-control" name="duration" placeholder="Payment duration">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1" class="col-sm-3 control-label">Issuing
                            officer*</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-user-plus"></i></div>
                                <select class="form-control input-lg" name="officer" required="">
                                    <option>--issuing officer--</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-b-0">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Post
                                Request</button>
                            <button type="reset" class="btn btn-danger waves-effect waves-light m-t-10">Reset</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <?php include 'includes/footer.php';?>