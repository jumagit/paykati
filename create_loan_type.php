<?php include "a_includes/header.php";?>
<?php include "a_includes/topNav.php";?>
<?php include "a_includes/sideNav.php";?>

<style>
td {
    vertical-align: middle !important;
}

td p {
    margin: unset
}
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="col-lg-12">
            <br>
        </div>
        <div class="row">
            <!-- FORM Panel -->
            <div class="col-md-4">
                <form action="" id="manage-loan-type">
                    <div class="panel">
                        <div class="panel-heading">
                            Loan Type Form
                        </div>
                        <div class="panel-body">
                            <input type="hidden" name="id">
                            <div class="form-group">
                                <label class="control-label">Type</label>
                                <textarea name="type_name" id="" cols="30" rows="2" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Description</label>
                                <textarea name="description" id="" cols="30" rows="2" class="form-control"></textarea>
                            </div>



                        </div>

                        <div class="panel-footer">
                            
                                    <button class="btn btn-sm btn-primary "> Save</button>
                                   
                              
                        </div>
                    </div>
                </form>
            </div>
            <!-- FORM Panel -->

            <!-- Table Panel -->
            <div class="col-md-8">
                <div class="panel">
                    <div class="panel-body">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Loan Type</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
								$i = 1;
								$types = query("SELECT * FROM loan_types order by id asc");
								while($row=$types->fetch_assoc()):
								?>
                                <tr>
                                    <td class="text-center"><?php echo $i++ ?></td>

                                    <td class="">
                                        <p>Type Name: <b><?php echo $row['type_name'] ?></b></p>
                                        <p><small>Description: <b><?php echo $row['description'] ?></b></small></p>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-sm btn-primary edit_ltype" type="button"
                                            data-id="<?php echo $row['id'] ?>"
                                            data-type_name="<?php echo $row['type_name'] ?>"
                                            data-description="<?php echo $row['description'] ?>">Edit</button>
                                        <button class="btn btn-sm btn-danger delete_ltype" type="button"
                                            data-id="<?php echo $row['id'] ?>">Delete</button>
                                    </td>
                                </tr>
                                <?php endwhile; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Table Panel -->
        </div>
    </div>
</div>
</div>


<script>
$('form#manage-loan-type').on("submit", function(e) {
    e.preventDefault();

    var formData = new FormData(this);

    swal({
        title: "Are you sure?",
        text: "Okay to add a  New Loan Type",
        type: "info",
        padding: 20,
        showCancelButton: true,
        confirmButtonColor: "#384888",
        confirmButtonText: "Yes, create!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "php_action/create_plans.php?t=type",
                data: formData,
                success: function(result) {

                    if (result.status) {
                        swal({
                            title: "Good job!",
                            padding: 20,
                            text: "Good Job! A New plan has been Created Successfully!",
                            type: "success"
                        });

                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    } else {
                        swal({
                            title: "Oops!",
                            padding: 20,
                            text: result.msg + "..please try again!",
                            type: "warning"
                        });
                    }
                },
                error: function(jqXHR) {
                    console.log(jqXHR);
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
});
</script>


<?php include "a_includes/footer.php";?>