
<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
<title>ONLINE BANKING</title>
<!-- Bootstrap Core CSS -->
<link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="css/colors/default.css" id="theme"  rel="stylesheet">


</head>

<style>
   
    .btn{
        background-color: #083666;
        outline: none;
        border: none;
        border-radius: 13px;
    }

    .item{
       border-radius: 13px;
    }

    .item img{
      height: 300px !important;
      border-radius: 13px;
    }

    h3{
      color: gold;
    }

    .white-box{
        
        background-color: #fff;
        border-radius: 13px;
        border: 1px solid #26AFC5 ;

    }
    #wrapper{
        background: #d6d6d6 !important;
       
       
    }
</style>


<body>

<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section style="padding:20px;" id="wrapper" >

              <div class="container">

                <div class="row">
                 
                  <div class="col-md-9">

                     <h2 class="text-left"><img src="assets/images/logo.png" width="140" alt=""> </h2>
                    
                  </div>
                  <div class="col-md-3">

                   <h2>
                     <a href="contact.php" class="btn btn-primary btn-lg pull-right">Reach us Now!</a>
                   </h2>
                    
               
                  </div>
                </div>

                <hr>

                <h2 class="text-center">
                  <b>SACCO MANAGEMENT SYSTEM FOR FARMERS</b>
                </h2>

               
                                <br>


                     <div id="myCarousel" class="carousel slide" data-ride="carousel">
                      <!-- Indicators -->
                      <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                      </ol>

                  <!-- Wrapper for slides -->
                      <div class="carousel-inner">
                        <div class="item active">
                          <img src="assets/images/y.png" alt="Los Angeles">
                           <div class="carousel-caption text-left">
                            <h3 class="text-white">EASY AND QUICK <br>
                              <p class="text-white">Payments Available on all Networks</p></h3>
                            
                          </div>
                        </div>

                        <div class="item">
                          <img src="assets/images/op.png" alt="Chicago">
                           <div class="carousel-caption text-left">
                            <h3 class="text-white">EASY AND QUICK <br>
                              <p class="text-white">Quick and easy to install</p></h3>
                            
                          </div>
                        </div>

                        <div class="item">
                          <img src="assets/images/yu.png" alt="New York">
                          <div class="carousel-caption text-left">
                            <h3 class="text-white">EASY AND QUICK <br>
                              <p class="text-white">Fast and reliable to use</p></h3>
                            
                          </div>
                        </div>
                      </div>

                  <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>

                <hr>
                 <div class=" text-center pt-3">
                              
                              <p  style="color:red" class=" h3 font-weight-bold">
                                <b>
                                  “Do not save what is left after spending; instead spend what is left after saving.”
                                    --Warren Buffett
                                </b>
                              </p>
                              
                </div>

                <hr>


                <h4 class="text-center font-weight-bold">
                  <b>CURRENTLY AVAILABLE MODULES</b>
                </h4>


                <hr>

              
             
                <div class="row">
                  <div class="col-md-4">
                    <div class="white-box text-center">
                  <center><h3 class="box-title m-b-0">Registration</h3>
                <small>Allow SACCO’s & MFI’s to know their clients by capturing, processing & analyzing bio data, demographic data from members and clients. </small></center>
                    <h3 class="text-info"> <button class="btn btn-info">Sign up Now!</button> </h3>
                </div>
                  </div>

                  <div class="col-md-4">
                    <div class="white-box text-center">
                  <center><h3 class="box-title m-b-0">Savings & Contributions</h3>
                <small>It allows multiple Saving Products including Interest Accruing Accounts, Fixed Deposit Accounts, Non-Interest Savings Account, with Ease.</small></center>
                    <h3 class="text-info"> <button class="btn btn-info">Sign up Now!</button> </h3>
                </div>
                  </div>

                  <div class="col-md-4">
                    <div class="white-box text-center">
                  <center><h3 class="box-title m-b-0">Loan Management</h3>
                <small>Supports multiple loan products as offered by MFI’s, SACCO’s & Credit Lending Companies including reducing balance interest based, straight line interest based, Micro finance</small></center>
                    <h3 class="text-info"> <button class="btn btn-info">Sign up Now!</button> </h3>
                </div>
                  </div>


                  <div class="col-md-12">
                    <div class="white-box text-center">
                  <center><h3 class="box-title m-b-0"> Notifications</h3>
                <small>The software notifies about anything activity carried out by the user in real time.</small></center>
                    <h3 class="text-info"> <button class="btn btn-info">Sign up Now!</button> </h3>
                </div>
                  </div>

                </div>
                
              </div>           
  
  
</section>
<!-- jQuery -->
<script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/custom.min.js"></script>
<!--Style Switcher -->
<script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

<script>
  function myFunction() {
  var x = document.getElementById("myInput");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
</script>
</body>
</html>




















