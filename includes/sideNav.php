  <div class="navbar-default sidebar" role="navigation">
      <div class="sidebar-nav slimscrollsidebar">
          <div class="sidebar-head">
              <h3>ADMIN</h3>
          </div>
          <ul class="nav" id="side-menu">



              <li><a href="indexClient.php" class="waves-effect"><i class="fa fa-dashboard fa-fw"></i> <span
                          class="hide-menu">Dashboard </span></a></li>

             <li><a href="receipients.php" class="waves-effect"><i class="fa fa-sitemap fa-fw"></i> <span
                          class="hide-menu">Receipients </span></a></li>


              <li><a href="client_deposit.php" class="waves-effect"><i class="fa fa-sitemap fa-fw"></i> <span
                          class="hide-menu">Deposits </span></a></li>

              <li><a href="client_withdraw.php" class="waves-effect"><i class="fa fa-sitemap fa-fw"></i> <span
                          class="hide-menu">Withdraws </span></a></li>

              <li><a href="client_transfer.php" class="waves-effect"><i class="fa fa-sitemap fa-fw"></i> <span
                          class="hide-menu">Transfers </span></a></li>


              <li><a href="apply.php" class="waves-effect"><i class="fa fa-sitemap fa-fw"></i> <span
                          class="hide-menu"> Loans </span></a></li>

              <li><a href="client_reports.php" class="waves-effect"><i class="fa fa-pie-chart fa-fw"></i>
                      Reports</a>
              </li>





          </ul>
      </div>
  </div>