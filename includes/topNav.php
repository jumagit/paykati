 <div id="wrapper">
     <nav class="navbar navbar-default navbar-static-top m-b-0 bg-dark">
         <div class="navbar-header">
             <div class="top-left-part">
                 <!-- Logo -->
                 <a class="logo" href="">
                    <b>
                        <img src="assets/images/logo.png" alt="home" class="dark-logo"
                             width="120" />
                        
                     </b>
                     
                     <span class="hidden-xs">
                       <img src="plugins/images/admin-text.png" alt="home"
                             class="dark-logo" />
                       <img src="plugins/images/admin-text-dark.png" alt="home"
                             class="light-logo" />
                     </span>
                 </a>
             </div>
         
             <ul class="nav navbar-top-links navbar-left">
             </ul>



             <ul class="nav navbar-top-links navbar-right pull-right">


                 <li><a href="ca.php" class=" waves-effect waves-light"><i class="fa fa-calculator"></i></a>
                 </li>





                 <li class="dropdown">
                     <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"> <i
                             class="mdi mdi-gmail"></i>
                         <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                     </a>
                     <ul class="dropdown-menu mailbox animated bounceInDown">
                         <li>
                             <div class="drop-title" id="count">You have 4 new messages</div>
                         </li>
                         <li>
                             <div class="message-center" id="dropdown-menu">
                                 <a href="#">
                                     <div class="user-img"> <img src="plugins/images/users/pawandeep.jpg" alt="user"
                                             class="img-circle"> <span class="profile-status online pull-right"></span>
                                     </div>
                                     <div class="mail-contnet">
                                         <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span>
                                         <span class="time">9:30 AM</span>
                                     </div>
                                 </a>

                             </div>
                         </li>
                         <li>
                             <a class="text-center" href="activity_logs.php"> <strong>See all notifications</strong> <i
                                     class="fa fa-angle-right"></i> </a>
                         </li>


                     </ul>
                     <!-- /.dropdown-messages -->
                 </li>


                 <li class="dropdown">
                     <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img
                             src="plugins/images/users/varun.jpg" alt="user-img" width="36" class="img-circle"><b
                             class="hidden-xs">
                             <?php 

                          if (isset($_SESSION['client_id'])) { echo $_SESSION['fullName']; }

                          ?></b>
                         <span class="caret"></span> </a>
                     <ul class="dropdown-menu dropdown-user animated flipInY ">
                         <li>
                             <div class="dw-user-box">
                                 <div class="u-img"><img src="plugins/images/users/varun.jpg" alt="user" /></div>
                                 <div class="u-text">
                                     <h4> <?php  if (isset($_SESSION['client_id'])) { echo $_SESSION['fullName']; } ?>
                                     </h4>
                                     <p class="text-muted">
                                         <?php  if (isset($_SESSION['client_id'])) { echo $_SESSION['email']; } ?></p>
                                 </div>



                             </div>
                         </li>

                         <li class="">
                             <ul class="list-group list-group-flush ">
                                 <li class="list-group-item"><a href="pp.php"><i class="ti-user"></i> <span
                                             class="hide-menu">My Profile</span></a></li>
                                 <li class="list-group-item"><a href="activity_logs.php"><i class="ti-wallet"></i> <span
                                             class="hide-menu">Logs</span></a></li>

                                 <li class="list-group-item"><a href="c_logout.php"><i class="fa fa-power-off"></i>
                                         <span class="hide-menu">Logout</span></a></li>
                             </ul>
                         </li>


                     </ul>
                     <!-- /.dropdown-user -->
                 </li>
                 <!-- /.dropdown -->
             </ul>
         </div>
         <!-- /.navbar-header -->
         <!-- /.navbar-top-links -->
         <!-- /.navbar-static-side -->
     </nav>