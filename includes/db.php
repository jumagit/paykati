<?php

date_default_timezone_set('Africa/Kampala');

/*
 *  EMAIL CONFIGS
 */
//
define('SYSTEM_EMAIL', 'mukoovajuma183@gmail.com');
define('SMTP_PORT', 587);
define('SMTP_HOST', 'smtp.gmail.com');
define('SYSTEM_EMAIL_PASSWORD', 'salahuddin1');

//
//
define('URL_PUBLIC_FOLDER', 'static');
define('URL_PROTOCOL', 'https://');
define('URL_DOMAIN', $_SERVER['HTTP_HOST']);
define('URL_SUB_FOLDER', str_replace(URL_PUBLIC_FOLDER, '', dirname($_SERVER['SCRIPT_NAME'])));

define('URL', URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER . "/");
//
define("UPLOAD_DIR", 'uploads/');

//connection configuration
$connectionHost = 'localhost';
$connectionUsername = 'root';
$connectionPassword = '';
$connectionName = 'store';

global $connection;

//Connect and select the database
$connection = mysqli_connect($connectionHost, $connectionUsername, $connectionPassword, $connectionName);

if (!$connection) {
    die("Connection failed: " . $connection->connect_error);
}
?>


<?php

function query($sql)
{

    global $connection;

    $sql = mysqli_query($connection, $sql);

    return $sql;
}

//////// Generating random Invoice Number /////////

function RandomInvoiceNumber()
{
    $chars = "003232303232023232023456789";
    srand((double) microtime() * 1000000);
    $i = 0;
    $pass = '';
    while ($i <= 7) {

        $num = rand() % 33;

        $tmp = substr($chars, $num, 1);

        $pass = $pass . $tmp;

        $i++;

    }
    return $pass;
}

function generate_invoice_number()
{

    $client_merchant_code = get_client_merchant_code($_SESSION['client_id']);

    $finalcode = $client_merchant_code . RandomInvoiceNumber();

    return $finalcode;

}

function sendMailFront($emailAddress, $subject, $body)
{

    require_once "thirdparty/PHPMailer/PHPMailerAutoload.php";
    $mail = new PHPMailer;
    $mail->SMTPAuth = true;

    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true,
        ),
    );
    //End of Abdi Joseph
    $mail->isSMTP();
    $mail->SMTPDebug = false;
    $mail->do_debug = 1;
    $mail->Debugoutput = 'html';
    $mail->SMTPSecure = "tls";
    $mail->Host = SMTP_HOST;
    $mail->Port = SMTP_PORT;
    $mail->Username = SYSTEM_EMAIL;
    $mail->Password = SYSTEM_EMAIL_PASSWORD;
    $mail->SetFrom(SYSTEM_EMAIL, 'ONLINE BANKING');
    $mail->Subject = $subject;
    $mail->addAddress($emailAddress);
    $mail->isHTML(true);
    $mail->Body = $body;
    if (!$mail->send()) // Dont forget to add "!" condition
    {
        echo $mail->isError();
    } else {

        return true;
    }

}

function checking_any_existance($field, $id, $table, $column)
{
    if (isset($_POST[$field])) {
        $field = $_POST[$field];
        $field = clean($field);
        $result = query("SELECT $id FROM $table WHERE $column = '$field'");
        while ($row = mysqli_fetch_array($result)) {
            $return_id = $row[0];
        }
        if (mysqli_num_rows($result) == 1) {
            return true;
        } else {
            return $result_id;
        }

    }
}

function printReport($html, $subject)
{

    global $connection;
    require_once 'thirdparty/mpdf/mpdf.php';
    $mpdf = new mPDF('c');
    $mpdf->mirrorMargins = 1;
    $mpdf->SetDisplayMode('fullpage');
    //$mpdf->SetWatermarkImage(URL.'static/images/logo-large.png');
    $mpdf->showWatermarkImage = true;
    $mpdf->SetTitle($subject);
    //$mpdf->SetHTMLHeader($header);
    //$mpdf->SetHTMLFooter($footer);

    // LOAD a stylesheet
    $stylesheet = file_get_contents('upgrade/css/bootstrap.min.css');
    $mpdf->WriteHTML($stylesheet, 1); // The parameter 1 tells that this is css/style only and no body/html/text
    $mpdf->WriteHTML($html);
    // Output a PDF file directly to the browser
    $mpdf->Output($subject . mt_rand(0, 9999) . ".pdf", "i");

}

function sendMailM($emailAddress, $subject, $body)
{

    require_once "../thirdparty/PHPMailer/PHPMailerAutoload.php";
    $mail = new PHPMailer;
    $mail->SMTPAuth = true;

    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true,
        ),
    );
    //End of Abdi Joseph
    $mail->isSMTP();
    $mail->SMTPDebug = false;
    $mail->do_debug = 1;
    $mail->Debugoutput = 'html';
    $mail->SMTPSecure = "tls";
    $mail->Host = SMTP_HOST;
    $mail->Port = SMTP_PORT;
    $mail->Username = SYSTEM_EMAIL;
    $mail->Password = SYSTEM_EMAIL_PASSWORD;
    $mail->SetFrom(SYSTEM_EMAIL, 'ONLINE BANKING');
    $mail->Subject = $subject;
    $mail->addAddress($emailAddress);
    $mail->isHTML(true);
    $mail->Body = $body;
    if (!$mail->send()) // Dont forget to add "!" condition
    {
        echo $mail->isError();
    } else {

        return true;
    }

}

function count_anything($table, $id)
{

    $result = "SELECT * FROM $table WHERE client_id =  '$id' ";
    $runx = query($result);
    $number = mysqli_num_rows($runx);
    return $number;

}

function count_member_clients($table, $code)
{

    $result = "SELECT * FROM $table WHERE code =  '$code' ";
    $runx = query($result);
    $number = mysqli_num_rows($runx);
    return $number;

}

function count_members($code)
{

    $result = "SELECT COUNT(*) AS count FROM clients WHERE code =  '$code' ";
    $runx = query($result);
    $count_rows = mysqli_fetch_assoc($runx);
    return intval($count_rows['count']);

}

function udate($format = 'u', $utimestamp = null)
{
    if (is_null($utimestamp)) {
        $utimestamp = microtime(true);
    }

    $timestamp = floor($utimestamp);
    $milliseconds = round(($utimestamp - $timestamp) * 1000000);

    return date(preg_replace('`(?<!\\\\)u`', $milliseconds, $format), $timestamp);
}

function message()
{

    $smsmessage = "Dear Customer, your group " . $name . " has openned an Account successfuly. You can now transact using your Group Account Number " . $ccode . " and made a deposit of " . number_format($initial_deposit) . "/= at a Charge on deposit of " . number_format($depositCharge) . "/= and Account opening process charges of " . number_format($initial_costs) . "/= Your Group Account balance is " . number_format($currentbal) . "/= Thank you for choosing " . $saccoName . ". For more Information and Inquiries Call: +256" . $bizcontact;

}

//end date
$date = udate('Y-m-d H:i:s.u');

//generating the code
function code($code_length)
{
    $selection = "0123456789";
    return substr(str_shuffle($selection), 0, $code_length);
}

function fetch_customers_of_client($client_id)
{

    $option = "";

    global $connection;

    $fcustomers = "SELECT * FROM customers WHERE client_id = '$client_id'";
    $quer = query($fcustomers);

    if ($quer) {

        while ($rowE = mysqli_fetch_array($quer)) {

            $customer_id = $rowE['customer_id'];
            $customer_names = $rowE['customer_names'];
            $option .= ' <li class="list-group-item"><a href="singleCustomer.php?customer_id=' . $customer_id . '" onclick="load_selected_customer_data_on_hover(' . $customer_id . ')">' . $customer_names . '</a></li>';
            // $option .= '<option value='.$customer_id.'>'.$customer_names.'</option>';

        }
    }

    return @$option;

}

function fetch_customers_per_client($client_id)
{

    $option = "";

    global $connection;

    $fcustomers = "SELECT * FROM customers WHERE client_id = '$client_id'";
    $quer = query($fcustomers);

    if ($quer) {

        while ($rowE = mysqli_fetch_array($quer)) {

            $customer_id = $rowE['customer_id'];
            $customer_names = $rowE['customer_names'];
            $option .= '<option value=' . $customer_id . '>' . $customer_names . '</option>';

        }
    }

    return @$option;

}

function fetch_members_per_client($code)
{

    $option = "";

    global $connection;

    $fcustomers = "SELECT * FROM clients WHERE code = '$code' ";
    $quer = query($fcustomers);

    if ($quer) {

        while ($rowE = mysqli_fetch_array($quer)) {

            $client_id = $rowE['client_id'];
            $fullName = $rowE['fullName'];
            $option .= '<option value=' . $client_id . '>' . $fullName . '</option>';

        }
    }

    return @$option;

}

function count_val($table)
{

    $result = "SELECT COUNT(*) FROM $table  ";
    $c_result1 = query($result);
    $c_return1 = mysqli_fetch_assoc($c_result1);
    //var_dump($c_return1);
    return intval($c_return1['COUNT(*)']);
}

function count_admin($table)
{

    $result = "SELECT COUNT(*) FROM $table ";
    $c_result1 = query($result);
    $c_return1 = mysqli_fetch_assoc($c_result1);
    //var_dump($c_return1);
    echo intval($c_return1['COUNT(*)']);
}

function f_date($date)
{
    //str_replace(",", "", $_POST['amount']);
    return date_format(date_create($date), ' l jS F Y');
}

function ozekiSend($sender, $phone, $msg, $debug = false)
{
    global $ozeki_user, $ozeki_password, $ozeki_url;

    $url = 'username=' . $ozeki_user;
    $url .= '&password=' . $ozeki_password;
    $url .= '&sender=' . urlencode($sender);
    $url .= '&recipient=' . urlencode($phone);
    $url .= '&message=' . urlencode($msg);

    $urltouse = $ozeki_url . $url;
    //if ($debug) { echo "Request: <br>$urltouse<br><br>"; }

    //Open the URL to send the message
    $response = file_get_contents($urltouse);
    if ($debug) {
        //echo "Response: <br><pre>".
        //str_replace(array("<",">"),array("&lt;","&gt;"),$response).
        //"</pre><br>";
    }

    return ($response);
}

function get_client_name($id)
{

    $sql = query("SELECT fullName FROM clients WHERE client_id = '$id' ");
    $client_name = mysqli_fetch_assoc($sql);
    $name = $client_name['fullName'];
    return $name;
}

function get_customer_account_details($customer_id, $client_id)
{

    $sql_bank = query("SELECT * FROM sacco_bank_accounts WHERE client_id='$client_id' AND customer_id = '$customer_id'  ") or die(mysqli_error($connection));
    $datas = array();

    if ($sql_bank) {

        while ($row = mysqli_fetch_array($sql_bank)) {
            $datas[] = $row;
        }

        foreach ($datas as $data) {
            return $data;
        }
    }

}

function clean($input)
{
    global $connection;
    $input = trim(htmlentities(strip_tags($input, ",")));
    if (get_magic_quotes_gpc()) {
        $input = stripslashes($input);
    }

    $input = mysqli_real_escape_string($connection, $input);

    $input = trim($input);
    return $input;
}

function duplicate_username($username)
{
    global $connection;

    $query = "SELECT Username FROM users WHERE Username = '$username'";

    $username_query = mysqli_query($connection, $query);

    if (mysqli_num_rows($username_query) > 0) {

        return true;

    } else {

        return false;
    }
}

function writeLog($activity, $username, $log_type)
{

    global $connection;
    $month = date('F');
    $day = date('j');
    $year = date('Y');
    $activity_time = date('l j<\s\u\p>S</\s\u\p>, F Y (h:i:s A)');
    $sql = "INSERT INTO system_access_logs (activity, day, month, year, username, activity_time, log_type)
     VALUES ( '$activity', '$day', '$month', '$year', '$username', '$activity_time', '$log_type')";
    $result = query($sql);
    if (!$result) {

        return false;
    } else {
        return true;
    }
}

function getUserIP()
{
    $client = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote = $_SERVER['REMOTE_ADDR'];

    if (filter_var($client, FILTER_VALIDATE_IP)) {
        $ip = $client;
    } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
        $ip = $forward;
    } else {
        $ip = $remote;
    }

    return $ip;
}
$IP = getUserIP();

function users_online()
{

    global $connection;

    if (isset($_GET['onlineusers'])) {
        if (!$connection) {

            $connection = mysqli_connect('localhost', 'root', '', 'project');

            session_start();

            $session = session_id();
            $time = time();
            $time_out_in_seconds = 05;
            $time_out = $time - $time_out_in_seconds;

            $query = "SELECT * FROM online_users WHERE session = '$session'";

            $users_online_count = mysqli_query($connection, $query);

            $count = mysqli_num_rows($users_online_count);

            if ($count == null) {

                $online_insert = mysqli_query($connection, "INSERT INTO online_users(session, time ) VALUES ('$session','$time')");
            } else {

                $online_insert = mysqli_query($connection, "UPDATE online_users SET time= '$time' WHERE session = '$session'");

                //users online
                $users_online = mysqli_query($connection, "SELECT * FROM online_users WHERE time > '$time_out'");

                echo $display_users = mysqli_num_rows($users_online);
            }

        }
    }
//get request isset()

}

users_online();

function admin_user()
{
    global $connection;

    if (isset($_GET['admin'])) {
        $the_user_id = $_GET['admin'];
        $query = "UPDATE users SET user_status = 'Admin' WHERE StdNo = {$the_user_id}";
        $admin_query = mysqli_query($connection, $query);
    }
}

// subscriber change
function subscriber_user()
{
    global $connection;

    if (isset($_GET['subscriber'])) {
        $the_user_id = $_GET['subscriber'];
        $query = "UPDATE users SET user_status = 'Subscriber' WHERE StdNo = {$the_user_id}";
        $admin_query = mysqli_query($connection, $query);
    }
}

function generatePassword($length = 9, $add_dashes = false, $available_sets = 'luds')
{
    $sets = array();
    if (strpos($available_sets, 'l') !== false) {
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
    }

    if (strpos($available_sets, 'u') !== false) {
        $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
    }

    if (strpos($available_sets, 'd') !== false) {
        $sets[] = '23456789';
    }

    if (strpos($available_sets, 's') !== false) {
        $sets[] = '!@#$%&*?';
    }

    $all = '';
    $password = '';
    foreach ($sets as $set) {
        $password .= $set[array_rand(str_split($set))];
        $all .= $set;
    }
    $all = str_split($all);
    for ($i = 0; $i < $length - count($sets); $i++) {
        $password .= $all[array_rand($all)];
    }

    $password = str_shuffle($password);
    if (!$add_dashes) {
        return $password;
    }

    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while (strlen($password) > $dash_len) {
        $dash_str .= substr($password, 0, $dash_len) . '-';
        $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
}

//money

function money($amount)
{
    $amount = str_replace(',', '', $amount);
    $amount = floatval($amount);
    $amount = intval($amount, 10);
    return $amount;
}

function get_client_merchant_code($id)
{

    $sql = "SELECT suppliercode FROM clients WHERE client_id = '$id' LIMIT 1";
    $result = query($sql);
    while ($row = mysqli_fetch_array($result)) {
        $code = $row[0];
    }
    return $code;

}

function orders_count()
{

    global $connection;

    $query = "SELECT invoice_number  FROM orders order by invoice_number DESC limit 1";
    $result = mysqli_query($connection, $query);
    while ($row = mysqli_fetch_array($result)) {
        $id = $row[0];
        return $id;
    }

}

function real_counting($table)
{

    $sql = "SELECT * FROM $table WHERE client_id = '" . $_SESSION['client_id'] . "'";
    $result = query($sql);
    $count = mysqli_num_rows($result);
    return $count;

}

function convertNumberToWord($num = false)
{
    $num = str_replace(array(',', ' '), '', trim($num));
    if (!$num) {
        return false;
    }
    $num = (int) $num;
    $words = array();
    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen',
    );
    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion',
    );
    $num_length = strlen($num);
    $levels = (int) (($num_length + 2) / 3);
    $max_length = $levels * 3;
    $num = substr('00' . $num, -$max_length);
    $num_levels = str_split($num, 3);
    for ($i = 0; $i < count($num_levels); $i++) {
        $levels--;
        $hundreds = (int) ($num_levels[$i] / 100);
        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
        $tens = (int) ($num_levels[$i] % 100);
        $singles = '';
        if ($tens < 20) {
            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '');
        } else {
            $tens = (int) ($tens / 10);
            $tens = ' ' . $list2[$tens] . ' ';
            $singles = (int) ($num_levels[$i] % 10);
            $singles = ' ' . $list1[$singles] . ' ';
        }
        $words[] = $hundreds . $tens . $singles . (($levels && (int) ($num_levels[$i])) ? ' ' . $list3[$levels] . ' ' : '');
    } //end for loop
    $commas = count($words);
    if ($commas > 1) {
        $commas = $commas - 1;
    }
    return implode(' ', $words);
}

function get_account_details($id)
{
    global $connection;
    $result = mysqli_query($connection, "SELECT * FROM client_account WHERE client_id= '$id' ");
    while ($row = mysqli_fetch_array($result)) {
        return $row;
    }

}

function accountability($id, $type, $amount)
{
    global $connection;
    if ($type === 'deposit') {
        $result = mysqli_query($connection, "UPDATE accountability SET deposits=deposits+$amount WHERE  client_id= '$id' ");
    } elseif ($type === 'withdraw') {
        $result = mysqli_query($connection, "UPDATE accountability SET withdraws=withdraws+$amount WHERE  client_id= '$id' ");
    } elseif ($type === 'transfer') {
        $result = mysqli_query($connection, "UPDATE accountability SET transfers=transfers+$amount WHERE  client_id= '$id' ");
    } else {}
    return true;
}

function get_overall_client_details($id)
{
    global $connection;
    $result = mysqli_query($connection, "SELECT * FROM clients as c INNER JOIN client_account as cc ON c.client_id = cc.client_id INNER JOIN accountability as a ON c.client_id = a.client_id WHERE c.client_id = '$id' ");
    while ($row = mysqli_fetch_array($result)) {
        return $row;
    }

}

?>