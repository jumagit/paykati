            <footer class="footer text-center"> <?php echo date("Y"); ?> &copy; Copyrights Reserved. </footer>
        </div>
    
    </div>

     
  
    <!-- Menu Plugin JavaScript -->
 
    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

    <!--Counter js -->
    <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
    <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
    <script src="plugins/bower_components/bootstrap-select/bootstrap-select.min.js"></script> 
    <script src="plugins/bower_components/sweetalert/sweetalert.min.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="plugins/bower_components/switchery/dist/switchery.min.js"></script>
    <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> 
    <!-- Magnific popup JavaScript -->
    <script src="plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
    <script src="plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
    <script src="plugins/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="plugins/bower_components/datatables/media/js/dataTables.bootstrap.js"></script>
    <script src="plugins/chart.min.js"></script> 
      
    <script src="plugins/bower_components/summernote/dist/summernote.min.js"></script>
    <script src="plugins/bower_components/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script> 

    <!--slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>  
    <script src="js/custom.min.js"></script>
    <script src="js/canvasjs.min.js"></script>
    <script src="assets/js/charts.js"></script> 
    <script src="js/Transactions.js"></script>
    <script src="js/functions.js"></script>


    <script>


      var chartss = new CanvasJS.Chart("chartss",
      {
        title: {
          text: "Division of products Sold in Quarter."

        },
        data: [
        {
          type: "stackedBar100",
          dataPoints: [
          { y: 600, label: "Water Filter" },
          { y: 400, label: "Modern Chair" },
          { y: 120, label: "VOIP Phone" },
          { y: 250, label: "Microwave" },
          { y: 120, label: "Water Filter" },
          { y: 374, label: "Expresso Machine" },
          { y: 350, label: "Lobby Chair" }

          ]
        },
         {
          type: "stackedBar100",
          dataPoints: [
          { y: 400, label: "Water Filter" },
          { y: 500, label: "Modern Chair" },
          { y: 220, label: "VOIP Phone" },
          { y: 350, label: "Microwave" },
          { y: 220, label: "Water Filter" },
          { y: 474, label: "Expresso Machine" },
          { y: 450, label: "Lobby Chair" }

          ]
         },
        {
          type: "stackedBar100",
          dataPoints: [
          { y: 300, label: "Water Filter" },
          { y: 610, label: "Modern Chair" },
          { y: 215, label: "VOIP Phone" },
          { y: 221, label: "Microwave" },
          { y: 75, label: "Water Filter" },
          { y: 310, label: "Expresso Machine" },
          { y: 340, label: "Lobby Chair" }

          ]
        }

        ]

      });

      chartss.render();


      //


      var chardt = new CanvasJS.Chart("chardtContainer", {
        title: {
          text: "Spline Area Chart with Stripline on Axis X"
        },
        axisX: {
          stripLines: [
          {
            value: 1940
          }
          ],
          valueFormatString: "####"
        },
        data: [
        {
          type: "splineArea",
          color: "rgba(83, 223, 128, .6)",
          dataPoints: [
            { x: 1910, y: 5 },
            { x: 1920, y: 9 },
            { x: 1930, y: 17 },
            { x: 1940, y: 32 },
            { x: 1950, y: 22 },
            { x: 1960, y: 14 },
            { x: 1970, y: 25 },
            { x: 1980, y: 18 },
            { x: 1990, y: 20 }
          ]
        }
        ]
      });
      chardt.render();

      //
      
              var chart = new CanvasJS.Chart("chartContainer", {
                title: {
                    text: "Worst Trafficking Cases"
                },
                exportFileName: "CanvasJS Chart",
                exportEnabled: true,
                animationEnabled: true,
                legend: {
                    verticalAlign: "bottom",
                    horizontalAlign: "center"
                },
                data: [
                {
                    type: "pie",
                    showInLegend: true,
                    toolTipContent: "{legendText}: <strong>{y}%</strong>",
                    indexLabel: "{label} {y}%",
                    dataPoints: [
                        { y: 35, legendText: "WOMEN", exploded: true, label: "WOMEN" },
                        { y: 20, legendText: "GIRLS", label: "GIRLS" },
                        { y: 18, legendText: "MEN", label: "MEN" },
                        { y: 15, legendText: "BOYS", label: "BOYS" },
                        { y: 5, legendText: "CHILDREN", label: "CHILDREN" },
                       
                    ]
                }
                ]
            });
            chart.render();

            //bar chart 

            var chartbar = new CanvasJS.Chart("chartBarContainer",
            {
                title:{
                    text: "SAMMARIES"
                },

                data: [
                {
                    type: "bar",

                    dataPoints: [
                    { x: 10, y: 90, label:"ARU" },
                    { x: 20, y: 70, label:"GUL" },
                    { x: 30, y: 50, label:"LUW" },
                    { x: 40, y: 60, label:"MAS" },
                    { x: 50, y: 20, label:"WAK" },
                    { x: 60, y: 30, label:"MBA" },
                    { x: 70, y: 35, label:"MUB" },
                    { x: 80, y: 40, label:"JIN" },
                    { x: 90, y: 30, label:"KAM" }
                    ]
                }
                ]
            });

            chartbar.render();


    </script>

 <script>



    $('.reason').hover(function() {
      $('.para').show();
    }, function() {
       $('.para').hide();
    });
    
    jQuery(document).ready(function() {
         
        $('.summernote').summernote({
            height: 200, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
            focus: true // set focus to editable area after initializing summernote
        });
        $('.inline-editor').summernote({
            airMode: true
        });
    });
    window.edit = function() {
        $(".click2edit").summernote()
    }, window.save = function() {
        $(".click2edit").destroy()
    }
           // Date Picker


   jQuery('select').selectpicker({
    dropupAuto:false,
    liveSearch:true,
    showTick:false,
    header:false,
   });


   jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });



     $(".paid").on('keyup', function() {

        $("#PaymentBox").css("display", "block");

        //basic inputs required not empty

        var address = $("#address");
        var emailAddress = $("#emailAddress");
        var customerName = $("#customerName");
        var customer_id  = $("#customer_id");
        var customerContact = $("#customerContact");
        var subTotal =  $("#subTotal");
        var grandTotal =  $("#grandTotal");
        var paid        = $("#paid");
        var paymentType = $("#paymentType");
        var mydatepicker = $(".mydatepicker");


        if(address.val() == "" ){

          $(".orderSection").css('border', '2px solid red');
          subTotal.prop('disabled', 'true').css('border', '1px solid red');
          grandTotal.prop('disabled', 'true').css('border', '1px solid red');
          paymentType.prop('disabled', 'true').css('border', '1px solid red');
          address.prop('disabled', 'true').css('border', '1px solid red');
          customerContact.prop('disabled', 'true').css('border', '1px solid red');
          customerName.prop('disabled', 'true').css('border', '1px solid red');
          emailAddress.prop('disabled', 'true').css('border', '1px solid red');
          mydatepicker.prop('disabled', 'true').css('border', '1px solid red');

         swal("Dear user, Fill in the customer Inputs to continue");
          //  setTimeout(function() {
            
          //   window.location.reload();
          // }, 3000);




        }else{


        }



    });

    //payment modes



    //load customer on hover

     load_selected_customer_data_on_hover =  function(id){
                $.ajax({
              url: 'php_action/fetch_selected_customer.php?t=fetch',
              type: 'POST',
              dataType: 'json',
              data: {id:id},
              success:function(response){
                  $(".customer_id").val(response[0]);
                  $(".customerName").val(response[1]);
                  $(".customerContact").val(response[4]);
                  $(".emailAddress").val(response[3]);
                  $(".address").val(response[2]);
                 

                  //console.log(response);


              }
          });
        }



//end load on hover





   


     load_selected_customer_data =  function(id){
                $.ajax({
              url: 'php_action/fetch_selected_customer.php?t=fetch',
              type: 'POST',
              dataType: 'json',
              data: {id:id},
              success:function(response){
                  $("#customer_id").val(response[0]);
                  $("#customerName").val(response[1]);
                  $("#customerContact").val(response[4]);
                  $("#emailAddress").val(response[3]);
                  $("#address").val(response[2]);
                  //$("#customerName").val(response[1]);


                  //console.log(response);


              }
          });
        }



        //payment modes on orders

     $(".man ").on('click', function() {
        $(".PaymentBox").css("display", "block");
    });





    //payment modes

        //end payment modes



          jQuery(document).on("submit", "#createOrderForm", function(e) {
          e.preventDefault();

          var formData = new FormData(this);


                    $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "create_ordersss.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A New Order has been Created Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.href = "orders.php";
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });

     });





      $('#myTable').DataTable();
      $('#myTables').DataTable();
       $('#myTabless').DataTable();

       $(".counter").counterUp({
            delay: 100,
            time: 1200
        });




//update Payments

  jQuery(document).on("submit", ".updatePayment", function(e) {


          e.preventDefault();

          var formData = new FormData(this);

          swal({
               title: "Are you sure?",
               text: "Okay to update Payment",
               type: "info",
               padding: 20,
               showCancelButton: true,
               confirmButtonText: "Yes",
               cancelButtonText: "No, cancel!",
               confirmButtonClass: "btn btn-success",
               cancelButtonClass: "btn btn-danger m-l-10",
               buttonsStyling: false
          }, function(isConfirm){
            if (isConfirm) {
                    $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/editPayment.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A Payment has been updated Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.href ="orders.php";
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
               }
          });
     });



   //customers

     $(document).on("submit", "#addCustomerForm", function(e) {
          e.preventDefault();

          var formData = new FormData(this);

          $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_customer.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A New Customer has been Created Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.href = "add_customer.php";
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });


     });


//end customers


     function sendMail(orderId) {

          var orderId = orderId;

          $.ajax({
               type: "POST",
               url: "php_action/send_email.php?t=true&id=" + orderId,
               data: { orderId: orderId },
               dataType: "json",
               success: function(result) {

                    if (result.status) {
                         swal({
                              title: "Good job!",
                              padding: 20,
                              text: "Good Job! Email Sent Successfully!",
                              type: "success"
                         });

                         setTimeout(function() {
                              window.location.reload();
                         }, 1000);
                    } else {
                         swal({
                              title: "Oops!",
                              padding: 20,
                              text: result.msg + "..please try again!",
                              type: "warning"
                         });
                    }

               }
          });
     }

     function deleteOrder(id) {
          var id = id;
          $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/removeOrder.php?t=delete&id=" + id,

                         success: function(result) {
                              // console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait an Order has been deleted!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
     }



//add sub Payment Modes


 $(document).on("submit", "#addMode", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({
            title: "Are you sure?",
            text: "You will Add Payment Mode!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#384888",
            confirmButtonText: "Yes, add it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_modes.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A Payment Mode has been Created Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.href = "modes.php";
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
            } else {
                swal("Cancelled", "Your Request has been cancelled :)", "error");
                  setTimeout(function() {
                      window.location.href = "modes.php";
                     }, 1000);
            }
        });
    });


//end payment mode


//add Bank Modes


 $(document).on("submit", "#addInstitution", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({
            title: "Are you sure?",
            text: "You will Add Institution!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#384888",
            confirmButtonText: "Yes, add it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_institution.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A Bank has been Created Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.href = "modes.php";
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
            } else {
                swal("Cancelled", "Your Request has been cancelled :)", "error");
                  setTimeout(function() {
                      window.location.href = "modes.php";
                     }, 1000);
            }
        });
    });


//end Bank mode


  $(document).on("submit", "#addCategoryForm", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({
            title: "Are you sure?",
            text: "You will be able to add category!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#384888",
            confirmButtonText: "Yes, add it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_categories.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A New Category has been Created Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.href = "categories.php";
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
            } else {
                swal("Cancelled", "Your Request has been cancelled :)", "error");
                 setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
            }
        });
    });

//sub category

    $(document).on("submit", "#addSubCategoryForm", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({
            title: "Are you sure?",
            text: "You will be able to add sub category!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#384888",
            confirmButtonText: "Yes, add it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_sub_categories.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A New Sub Category has been Created Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
            } else {
                swal("Cancelled", "Your Request has been cancelled :)", "error");
            }
        });
    });

//edit Sub categories


   $(document).on("submit", ".subcategoryEditForm", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({
            title: "Are you sure?",
            text: "You will be able to edit sub category!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#922d00",
            confirmButtonText: "Yes, edit it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_sub_categories.php?t=edit",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A Sub Category has been Updated Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.href = "sub_categories.php";
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
            } else {
                swal("Cancelled", "Your Request has been cancelled :)", "error");
            }
        });
    });

//edit category form

 $(document).on("submit", ".categoryEditForm", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({
            title: "Are you sure?",
            text: "You will be able to edit  Category!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#922d00",
            confirmButtonText: "Yes, edit it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_categories.php?t=edit",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A Sub Category has been Updated Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
            } else {
                swal("Cancelled", "Your Request has been cancelled :)", "error");
            }
        });
    });


//end edit category form




     $(document).on("submit", "#addBrandForm", function(e) {
          e.preventDefault();

          var formData = new FormData(this);

          swal({
               title: "Are you sure?",
               text: "Okay to add a  New Brand",
               type: "info",
               padding: 20,
               showCancelButton: true,
               confirmButtonText: "Yes",
               cancelButtonText: "No, cancel!",
               confirmButtonClass: "btn btn-success",
               cancelButtonClass: "btn btn-danger m-l-10",
               buttonsStyling: false
          }).then(willSave => {
               if (willSave) {
                    $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_brands.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A New Brand has been Created Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.href = "brands.php";
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
               }
          });
     });

     function deleteBrand(id) {
          var id = id;
          swal({
               title: "Are you sure?",
               text: "Okay to delete this Brand",
               type: "info",
               padding: 20,
               showCancelButton: true,
               confirmButtonText: "Yes",
               cancelButtonText: "No, cancel!",
               confirmButtonClass: "btn btn-success",
               cancelButtonClass: "btn btn-danger m-l-10",
               buttonsStyling: false
          }).then(willSave => {
               if (willSave) {
                    $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_brands.php?t=delete&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait a Brand has been deleted!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
               }
          });
     }

      function deleteCategory(id){
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this  file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                 $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_categories.php?t=delete&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait a Category has been deleted!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
            } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
            }
        });
    };



  function deleteSubCategory(id){
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this  file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                 $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_sub_categories.php?t=delete&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait a Sub Category has been deleted!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
            } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
            }
        });
    };


      function deleteMember(id) {

         var id = id;
         var client_id = client_id;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Member!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                   $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_agents.php?t=delete&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait a Member has been deleted!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
            } else {
                swal("Cancelled", "Your Request has been Cancelled :)", "error");
                  setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
            }
        });
    };



    //end sub categories

     function deleteCustomer(id,client_id) {

         var id = id;
          var client_id = client_id;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this  file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                   $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/delete_customer.php?t=delete&customer_id=" + id+"&client_id="+client_id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait a Customer has been deleted!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
            } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
            }
        });
    };




     function makeAvailable(id) {
          var id = id;
          swal({
               title: "Are you sure?",
               text: "Okay to Make this Available",
               type: "info",
               padding: 20,
               showCancelButton: true,
               confirmButtonText: "Yes",
               cancelButtonText: "No, cancel!",
               confirmButtonClass: "btn btn-success",
               cancelButtonClass: "btn btn-danger m-l-10",
               buttonsStyling: false
          }, function(isConfirm){
            if (isConfirm) {
                    $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_brands.php?t=available&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait a Brand has been changed to Available!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
               }
          });
     }

     function notAvailable(id) {
          var id = id;
          swal({
               title: "Are you sure?",
               text: "Okay to Make this Unavailable",
               type: "info",
               padding: 20,
               showCancelButton: true,
               confirmButtonText: "Yes",
               cancelButtonText: "No, cancel!",
               confirmButtonClass: "btn btn-success",
               cancelButtonClass: "btn btn-danger m-l-10",
               buttonsStyling: false
          }).then(willSave => {
               if (willSave) {
                    $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_brands.php?t=notavailable&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait a Brand has been changed to Not Available!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
               }
          });
     }

     //clients

    

     function deleteClient(id) {
          var id = id;
          swal({
               title: "Are you sure?",
               text: "Okay to delete this Client",
               type: "info",
               padding: 20,
               showCancelButton: true,
               confirmButtonText: "Yes",
               cancelButtonText: "No",
               confirmButtonClass: "btn btn-success",
               cancelButtonClass: "btn btn-danger m-l-10",
               buttonsStyling: false
          }).then(willSave => {
               if (willSave) {
                    $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_clients.php?t=delete&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait a Client has been deleted!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
               }
          });
     }


     //delete approve


   

   $(document).on("submit", "#submitAgentForm", function(e) {
          e.preventDefault();

          var formData = new FormData(this);
        swal({
            title: "Are you sure?",
            text: "You will be able to Add New User!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#2CABE3",
            confirmButtonText: "Yes, Add !",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                  $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_agents.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A New user Has been created Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
            } else {
                swal("Cancelled", "Your Request has been cancelled :)", "error");

                 setTimeout(function() {
                      window.location.reload();
                 }, 1000);

            }
        });
    });


 




     
     
     //user section

     //change profile Image

      $(document).on("submit", "#changeProfilePicture", function(e) {
          e.preventDefault();

          var formData = new FormData(this);

          swal({
               title: "Are you sure?",
               text: "Okay to change Profile Picture",
               type: "info",
               padding: 20,
               showCancelButton: true,
               confirmButtonText: "Yes",
               cancelButtonText: "No, cancel!",
               confirmButtonClass: "btn btn-success",
               cancelButtonClass: "btn btn-danger m-l-10",
               buttonsStyling: false
          }, function(isConfirm) {
               if (isConfirm) {
                    $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/change_profileImage.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! Profile Image Changed Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
               }
          });
     });





     //changing username

     $(document).on("submit", "#changeUsername", function(e) {
          e.preventDefault();

          var formData = new FormData(this);

          swal({
               title: "Are you sure?",
               text: "Okay to change Username",
               type: "info",
               padding: 20,
               showCancelButton: true,
               confirmButtonText: "Yes",
               cancelButtonText: "No, cancel!",
               confirmButtonClass: "btn btn-success",
               cancelButtonClass: "btn btn-danger m-l-10",
               buttonsStyling: false
          }, function(isConfirm) {
               if (isConfirm) {
                    $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/change_username.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! Username Changed Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
               }
          });
     });

     //change password

     $(document).on("submit", "#changePassword", function(e) {
          e.preventDefault();

          var formData = new FormData(this);

          swal({
               title: "Are you sure?",
               text: "Okay to change Your Password",
               type: "info",
               padding: 20,
               showCancelButton: true,
               confirmButtonText: "Yes",
               cancelButtonText: "No, cancel!",
               confirmButtonClass: "btn btn-success",
               cancelButtonClass: "btn btn-danger m-l-10",
               buttonsStyling: false
          },function(isConfirm){
             if(isConfirm){
                    $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/change_password.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! Password Changed Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
               }
          });
     });

     //addUserForm

     $(document).on("submit", "#addUserForm", function(e) {
          e.preventDefault();

          var formData = new FormData(this);

          swal({
               title: "Are you sure?",
               text: "Okay to add a  New User",
               type: "info",
               padding: 20,
               showCancelButton: true,
               confirmButtonText: "Yes",
               cancelButtonText: "No, cancel!",
               confirmButtonClass: "btn btn-success",
               cancelButtonClass: "btn btn-danger m-l-10",
               buttonsStyling: false
          }).then(willSave => {
               if (willSave) {
                    $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/create_users.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! A New User has been Created Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.href = "users.php";
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });
               }
          });
     });

     //make admin

     function makeAdmin(id) {
          var id = id;
          swal({
               title: "Are you sure?",
               text: "Okay to Make this User Admin",
               type: "info",
               padding: 20,
               showCancelButton: true,
               confirmButtonText: "Yes",
               cancelButtonText: "No, cancel!",
               confirmButtonClass: "btn btn-success",
               cancelButtonClass: "btn btn-danger m-l-10",
               buttonsStyling: false
          }).then(willSave => {
               if (willSave) {
                    $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_users.php?t=admin&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait, a User has been changed to Administrator!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
               }
          });
     }

     //revoke admin

     function revokeAdmin(id) {
          var id = id;
          swal({
               title: "Are you sure?",
               text: "Okay to Revoke Administration Access!",
               type: "info",
               padding: 20,
               showCancelButton: true,
               confirmButtonText: "Yes",
               cancelButtonText: "No, cancel!",
               confirmButtonClass: "btn btn-success",
               cancelButtonClass: "btn btn-danger m-l-10",
               buttonsStyling: false
          }).then(willSave => {
               if (willSave) {
                    $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_users.php?t=notadmin&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait, Administration Previlages have been removed Successifully!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
               }
          });
     }

     //deactivate account

     function deactivateAccount(id) {
          var id = id;
          swal({
               title: "Are you sure?",
               text: "Okay to Deactivate Account!",
               type: "info",
               padding: 20,
               showCancelButton: true,
               confirmButtonText: "Yes",
               cancelButtonText: "No, cancel!",
               confirmButtonClass: "btn btn-success",
               cancelButtonClass: "btn btn-danger m-l-10",
               buttonsStyling: false
          }).then(willSave => {
               if (willSave) {
                    $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_users.php?t=deactivateAccount&id=" + id,

                         success: function(result) {
                              console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: " While you wait, Administration Previlages have been removed Successifully!!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
               }
          });
     }

  
     function getTotal(row = null, value) {
          if (row) {
               var total = Number($("#price" + row).val()) * value;

               total = Number(total);

               $("#totalProductPrice" + row).val(total);

               subAmount();

          } else {
               alert('no row !! please refresh the page');
          }
     }

     //subamount function

     function subAmount() {
          var tableProductLength = $("#orderTable tbody tr").length;
          var totalSubAmount = 0;
          for (x = 0; x < tableProductLength; x++) {
               var tr = $("#orderTable tbody tr")[x];
               var count = $(tr).attr('id');
               count = count.substring(3);

               totalSubAmount = Number(totalSubAmount) + Number($("#totalProductPrice" + count).val());
          } // /for

          totalSubAmount = totalSubAmount.toFixed(2);

          // sub total
          $("#subTotal").val(totalSubAmount);
         
         
          // total amount
          var totalAmount = Number($("#subTotal").val());
          totalAmount = totalAmount.toFixed(2);
          $("#totalAmount").val(totalAmount);
          
          var discount = $("#discount").val();
          if (discount) {

               var grandTotal = Number($("#totalAmount").val()) - Number(discount);
               grandTotal = grandTotal.toFixed(2);
               $("#grandTotal").val(grandTotal);
              
          } else {

               $("#grandTotal").val(totalAmount);
               
          } // /else discount

          var paidAmount = $("#paid").val();
          if (paidAmount) {
               paidAmount = Number($("#grandTotal").val()) - Number(paidAmount);
               paidAmount = paidAmount.toFixed(2);
               $("#due").val(paidAmount);
               $("#dueValue").val(paidAmount);
          } else {
               $("#due").val($("#grandTotal").val());
               $("#dueValue").val($("#grandTotal").val());
          } // else

     } // /sub total amount

  

     function discountFunc() {
          var discount = $("#discount").val();
          var totalAmount = Number($("#totalAmount").val());
          totalAmount = totalAmount.toFixed(2);

          var grandTotal;

          if (totalAmount) {

               grandTotal = Number($("#totalAmount").val()) - Number($("#discount").val());
               grandTotal = grandTotal.toFixed(2);
               $("#grandTotal").val(grandTotal);
               $("#grandTotalValue").val(grandTotal);

          } else {}

          var paid = $("#paid").val();

          var dueAmount;

          if (paid) {

               dueAmount = Number($("#grandTotal").val()) - Number($("#paid").val());
               dueAmount = dueAmount.toFixed(2);

               $("#due").val(dueAmount);
              
          } else {

               $("#due").val($("#grandTotal").val());
              
          }
     } // /discount function

     function paidAmount() {
          var grandTotal = $("#grandTotal").val();

          if (grandTotal) {


               var dueAmount = Number($("#grandTotal").val()) - Number($("#paid").val());
               dueAmount = dueAmount.toFixed(2);




               $("#due").val(dueAmount);
             
          } // /if
     } // /paid amoutn function

     function resetOrderForm() {
          // reset the input field
          $("#createOrderForm")[0].reset();
          // remove remove text danger
          setTimeout(function() {
               window.location.reload();
          }, 100);
     } // /reset order form

     // print order function
     function printOrder(orderId) {

          var orderId = orderId;
          if (orderId) {
               $.ajax({
                    url: "php_action/print_order.php?t=true&id=" + orderId,
                    type: "post",
                    data: { orderId: orderId },
                    dataType: "text",
                    success: function(response) {
                         var mywindow = window.open(
                              "",
                              "Online Trade System",
                              "height=400,width=600"
                         );
                         mywindow.document.write("<html><head><title>Order Invoice</title>");
                         mywindow.document.write("</head><body>");
                         mywindow.document.write(response);
                         mywindow.document.write("</body></html>");

                         mywindow.document.close(); // necessary for IE >= 10
                         mywindow.focus(); // necessary for IE >= 10
                         mywindow.resizeTo(screen.width, screen.height);
                         setTimeout(function() {
                              mywindow.print();
                              mywindow.close();
                         }, 1250);

                         //mywindow.print();
                         //mywindow.close();
                    } // /success function
               }); // /ajax function to fetch the printable order
          } // /if orderId
     } // /print order function

     //remove order

     $("#orderDate").datepicker();
     $("#end_date").datepicker();
     $("#start_date").datepicker();

   
     //edit and update

     $(document).on("submit", "#editOrderForm", function(e) {
          e.preventDefault();

          var formData = new FormData(this);

                    $.ajax({
                         type: "POST",
                         dataType: "json",
                         url: "php_action/edit_order.php?t=true",
                         data: formData,
                         success: function(result) {
                              //console.log(result);
                              if (result.status) {
                                   swal({
                                        title: "Good job!",
                                        padding: 20,
                                        text: "Good Job! Order has been Updated Successfully!",
                                        type: "success"
                                   });

                                   setTimeout(function() {
                                        window.location.href = "orders.php";
                                   }, 1000);
                              } else {
                                   swal({
                                        title: "Oops!",
                                        padding: 20,
                                        text: result.msg + "..please try again!",
                                        type: "warning"
                                   });
                              }
                         },
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         },
                         cache: false,
                         contentType: false,
                         processData: false
                    });

          });

     </script>


   

    <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>
