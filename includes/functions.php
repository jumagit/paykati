<?php

include "db.php";


//format money







  function formatMoney($number, $fractional=false) {
      if ($fractional) {
          $number = sprintf('%.2f', $number);
      }
      while (true) {
          $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number);
          if ($replaced != $number) {
              $number = $replaced;
          } else {
              break;
          }
      }
      return $number;
  }

  //usage  echo formatMoney($zxc, true);


function get_user_info($username)
{
    $sql = "SELECT sname, fname, email FROM users WHERE account_status=1 AND username='$username'";
    $result = query($sql);
    $data = array();
    while ($row = mysqli_fetch_array($result)) {
        $data[] = $row;
    }
    return $data;
}

//fetch user function


function getClient($user_id){

  $sqlu = query("SELECT * FROM clients WHERE client_id='" . $user_id. "'");
  $rowu = mysqli_fetch_array($sqlu);          
  $client = $rowu['fullName'];
  return $client;
}


function revenueName($projection_id){

   $sqlc = query("SELECT revenue_name FROM revenue_categories WHERE rev_category_id = '" . $projection_id . "' ");
    $rowc = mysqli_fetch_array($sqlc);
    $revenue_name = $rowc['revenue_name'];
    return $revenue_name;


}



function checkIfSessionSet(){

   if (isset($_SESSION['client_id'])) {
      $client = $_SESSION['client_id'];
      return $client;

    }



}


 function fetchBanks(){

    $sql = query("SELECT * FROM banks");
        while ($row = mysqli_fetch_array($sql)) {  
                
       $data = '<option value='.$row['id'].'>'.$row['institution'].'</option>';
      }

      return $data;

 }

function distinctYear(){
        $sql = query("SELECT distinct(Year) FROM revenue_projections");
        while ($row = mysqli_fetch_array($sql)) {
        return '<option value='.$row['Year'].'>'.$row['Year'].'</option>';
      }
       
}


function selectMembers(){

   $sql1 = query("SELECT * FROM clients WHERE suppliercode = '".$_SESSION['suppliercode']."'");
   if($sql1){

    while ($row1 = mysqli_fetch_array($sql1)) {

     return '<option value="'.$row1['client_id'].'">'.$row1['fullName'].'</option>';
 
   
   }

   }
       
      
       
}



function selectProjections(){

   $sql1 = query("SELECT * FROM revenue_projections INNER JOIN revenue_categories ON revenue_projections.rev_category_id =revenue_categories.rev_category_id");

   while ($row1 = mysqli_fetch_array($sql1)) {

      return '<option value="'.$row1['rev_category_id'].'">'.$row1['revenue_name'].'</option>';
   
   }
       
      
       
}



function fetch_users()
{

    $sql = "SELECT user_id,sname,role,fname, email,mobile,gender,account_status, created_by, created_on FROM users ";
    $result = query($sql);
    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

            $count++;
            $user_id = $trailRow['user_id'];
            $sname = $trailRow['sname'];
            $fname = $trailRow['fname'];
            $role = $trailRow['role'];
            $created_by = $trailRow['created_by'];
            $created_on = $trailRow['created_on'];
            $email = $trailRow['email'];
            $mobile = $trailRow['mobile'];
            $account_status = $trailRow['account_status'];
            $created_on = date_format(date_create($trailRow['created_on']), ' l jS F Y');

            if ($account_status == 1) {

                $accountStatus = "Active";

            } else {

                $accountStatus = "Inactive";
            }



            //role

            if ($role == 1) {

                $role = "Administrator";

            } else {

                $role = "User";
            }


            $output .= "<tr>
               <td>{$count}</td>
               <td>{$sname} {$fname}</td>
               <td>{$email}</td>
               <td>{$mobile}</td>
               <td>{$accountStatus}</td>  
               <td>{$role}</td>         
               <td><a onclick='makeAdmin($user_id)' class='text-info'><i class='fa fa-user'></i>  Admin  </a></td>
               <td><a onclick='revokeAdmin($user_id)' class='text-success'><i class='fa fa-cut'></i> Revoke </a></td>
               <td><a onclick='deactivateAccount($user_id)' class='text-info'><i class='fa fa-times-circle'></i>  Deactivate  </a></td>
               <td><a href='edit_users.php?edit={$user_id}' ><i class='fa fa-edit'></i></a></td>
               <td><a onclick='deleteUser($user_id)' class='text-danger'><i class='fa fa-trash'></i></a></td>

               </tr>";

        }

    }

    return $output;

}

//fetch log function

function fetch_logs()
{

    $sql = "SELECT * FROM system_access_logs  ORDER BY id DESC";

    $result = query($sql);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

            $count++;
            $user = $trailRow['username'];
            $activityTime = $trailRow['activity_time'];
            $log_type = $trailRow['log_type'];
            $activity = $trailRow['activity'];

            $output .= "<tr>
           <td>{$count}</td>
           <td>{$user}</td>
           <td>{$activity}</td>
           <td>{$activityTime}</td>
           <td>{$log_type}</td>

           </tr>";

        }

          return $output;

    }

  
}


//fetch trans info


function mega_trans_logs_fetch($client_id,$account_id)
{

    $sql = "SELECT * FROM mega_trans_logs WHERE client_id = '$client_id' AND account_id ='$account_id'  ORDER BY id DESC";

    $result = query($sql);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

            $count++;
           
            $description = $trailRow['description'];
            $trans_type = $trailRow['trans_type'];
            $trans_amount = $trailRow['trans_amount'];
            //check

           switch ($trans_type) {
             case 'Deposit':
              $tt = "<span class='badge badge-success'>".$trans_type."</span>";
               break;

             case 'Withdraw':
              $tt = "<span class='badge badge-warning'>".$trans_type."</span>";
               break;

               case 'Transfer':
              $tt = "<span class='badge badge-info'>".$trans_type."</span>";
               break;
             
             
             default:
                $tt = "<span class='badge badge-primary'>".$trans_type."</span>";
               break;
           }
            
            


            $output .= "<tr>
           <td>{$count}</td>
           <td>{$description}</td>
           <td>{$tt}</td>

           </tr>";

        }

          return $output;

    }

  
}


function fetch_customer_orders($client_id,$customer_id){



  
$sql = "SELECT * FROM orders WHERE client_id = '$client_id' AND customer_id = '$customer_id' ";   

$result = query($sql);

if (mysqli_num_rows($result) > 0) {

$output = "";

$count = 0;

while ($trailRow = mysqli_fetch_array($result)) {

$count++; 

$orderId =  $trailRow['order_id'];
$order_id = base64_encode($orderId);
$order_date = $trailRow['order_date'];
$customer_id = $trailRow['customer_id'];           
// $due = $trailRow['due'];
$grand_total =$trailRow['grand_total'];
$paid = $trailRow['paid'];

//payment status
$payment_status = $trailRow['payment_status'];






//fetching total order items           

$countOrderItemSql = "SELECT SUM(quantityTaken) FROM order_item WHERE order_id = '$orderId' ";
$itemCountResult = query($countOrderItemSql);  
while ($p = mysqli_fetch_array($itemCountResult)) {
$itemCountRow  = $p[0];   
}         



$fetch_customer = "SELECT customers.customer_id,customers.customer_names FROM customers WHERE customers.customer_id = $customer_id";
$fetchCustomer = query($fetch_customer);
while($row = mysqli_fetch_array($fetchCustomer)){

$customerName = $row[1];

//selecting balance



//check for order status


if($grand_total == $paid ){

$order_status = "<span class='btn btn-xs btn-success btn-outline btn-rounded'> <i class='fa fa-check'></i> Completed</span>";
$payButton = "<a   class='btn btn-xs btn-success btn-outline btn-rounded' > <i class='fa fa-check'></i> Paid</a></td>";




}else{

$order_status = "<span class='btn btn-xs btn-danger btn-outline btn-rounded'><i class='fa fa-times'></i> Pending</span>";
$payButton = " <button type='button' class='btn btn-xs btn-warning btn-outline btn-rounded' data-toggle='modal' data-target='#exampleModal_$orderId' ><i class='fa fa-money'></i>  Clear</button>"
;


}



}


$data = '<tr>
<td>3</td>
<td>ere</td>
<td>go</td>
<td>fop</td>
<td>r</td>
<td>4</td>
</tr>';

return $data;


}


}

}



function fetch_customer_id($name,$suppliercode){

  $select = query("SELECT * FROM customers WHERE customer_names = '".clean($name)."' AND suppliercode = '".clean($suppliercode)."' ");
      if($select){
        $cn = mysqli_fetch_array($select);
        $customer_id = $cn['customer_id'];
        return $customer_id;
    }

}

//fetch payments


function fetch_payments()
{

    $sql = "SELECT * FROM track_payments  ORDER BY id DESC";

    $result = query($sql);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

            $count++;
            $customer = $trailRow['client_name'];
            $activityTime = f_date($trailRow['created_at']);
            $amount_paid = $trailRow['amount_paid'];
            $order = $trailRow['order_id'];
            $order_id = base64_encode($order);

            $customer_id = base64_encode(fetch_customer_id($customer,$_SESSION['suppliercode']));

            $service_provider = findBank($trailRow['service_provider']);


            $output .= "<tr>
           <td>{$count}</td>
           <td>{$customer} </td>
           <td>{$amount_paid}/=</td>
           <td>{$activityTime}</td>
           <td><a href='payment_details.php?order_id={$order_id}&customer_id={$customer_id}'><i class='fa fa-shopping-basket fa-2x'></a></a></td>
             <td>{$service_provider}</td>
 
           </tr>";

        }

          return $output;

    }

  
}





function singleCustomer($client_id,$customer_id){

  $results = array();

  if(!empty($id) AND $id != '0'){

      $sql = "SELECT * FROM customers WHERE customer_id = '$customer_id' LIMIT 1";

      $res = query($sql);

      if($res){

      while ($row = mysqli_fetch_array($res)) {
        
         $results[] = $row;

      }


          foreach ($results as $key => $value) {

              return $value;
          }

        }

      }



}





//fetch customers
function fetch_customers($id)
{

    $sql = "SELECT * FROM customers WHERE client_id = '$id' ORDER BY customer_id DESC";

    $result = query($sql);

    if($result){

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

                $count++;
                $customer_id = $trailRow['customer_id'];
                $customer_names = $trailRow['customer_names'];
                $contact = $trailRow['contact'];
                $email_address = $trailRow['email_address'];
                $address = $trailRow['address'];
                $edit_id = base64_encode($customer_id);
                $client_id = $_SESSION['client_id'];

               

            $output .= "<tr>
           <td>{$count}</td>
           <td>{$customer_names}</td>
           <td>{$email_address}</td>
           <td>{$address}</td>
           <td>{$contact}</td>
            <td><a href='singleCustomer.php?edit={$edit_id}'  ><i class='fa fa-cogs fa-2x'></i> </a></td>
           <td><a href='edit_customer.php?edit={$edit_id}'  ><i class='fa fa-edit fa-2x'></i> </a></td>
           <td><a onclick='deleteCustomer($customer_id,$client_id)' ><i class='fa fa-trash fa-2x text-danger'></i> </a></td>
           </tr>";

        }

      return $output;
    }


  }

   
}



//fetch agents


function fetch_members($id, $code)
{

    $sql = "SELECT * FROM clients WHERE code ='$code' AND client_id != '$id' ORDER BY client_id DESC";

    $result = query($sql);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

                $count++;
                $agentId = $trailRow['client_id'];
                $e_id = base64_encode($agentId);
                $agent_name = $trailRow['fullName'];
                $contact = $trailRow['mobile'];
                $email_address = $trailRow['email'];
                $address = $trailRow['location'];
              

               

            $output .= "<tr>
           <td>{$count}</td>
           <td>{$agent_name}</td>
           <td>{$email_address}</td>
           <td>{$address}</td>
           <td>{$contact}</td>
          
           <td><a href='edit_member.php?edit={$e_id}'  ><i class='fa fa-edit fa-2x'></i> </a></td>
           <td><a onclick='deleteMember($agentId)' ><i class='fa fa-trash fa-2x text-danger'></i> </a></td>
           </tr>";

        }

        
    return $output;


    }

}


//categories 



function fetch_categories($id)
{

    $sql = "SELECT * FROM categories WHERE client_id = '$id' ORDER BY categories_id DESC";

    $result = query($sql);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

            $count++;
            $categories_id = $trailRow['categories_id'];
           
            $categories_name = $trailRow['categories_name'];
         
            $categoriesId = base64_encode($categories_id);


            $output .= "<tr>
           <td>{$count}</td>
           <td>{$categories_name}</td>  
            <td><a data-toggle='modal' data-target='#exampleModal-{$categories_id}' data-whatever='@fa'><i class='fa fa-edit fa-2x '></i>  </a></td>
           
             <div class='modal fade' id='exampleModal-{$categories_id}' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel1'>
                                <div class='modal-dialog' role='document'>
                                    <div class='modal-content'>
                                        <div class='modal-header bg-danger'>
                                            <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                            <h3 class='modal-title font-weight-bold' ><i class='fa fa-edit'></i> Edit  Category</h3> </div>
                                        <div class='modal-body'>
                                            <form method='POST' action='#' class='categoryEditForm'>
                                                <div class='form-group'>
                                                   
                                                     <input type='hidden' class='form-control' value='{$categories_id}' name='categories_id'>
                                                   </div>

                                                     <div class='form-group'>
                                                    <label for='categories_name' class='control-label'>Category Name:</label>
                                                     <input type='text' class='form-control' value='{$categories_name}' name='categories_name'>
                                                   </div>
                                               
                                         
                                        </div>
                                        <div class='modal-footer'>
                                            <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                                            <button type='submit' class='btn btn-primary'><i class='fa fa-edit'></i> Edit </button>
                                        </div>
                                           </form>
                                    </div>
                                </div>
                            </div>      
          
           <td><a onclick='deleteCategory($categories_id)' ><i class='fa fa-trash fa-2x text-danger'></i> </a></td>
           </tr>";

        }

        return $output;

    }


    
}


//payment modes

function fetch_modes($id)
{

    $sql = "SELECT * FROM paymentModes WHERE client_id = '$id' ORDER BY id DESC";

    $result = query($sql);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

            $count++;
            $id = $trailRow['id'];
            $mode = $trailRow['mode'];
         
           

            $categoriesId = base64_encode($id);


            $output .= "<tr>
           <td>{$count}</td>
           <td>{$mode}</td>          
           <td><button type='button' class='btn btn-info' data-toggle='modal' data-target='#exampleModal-{$id}' data-whatever='@fa'>Edit</button></td>
           
             <div class='modal fade' id='exampleModal-{$id}' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel1'>
                                <div class='modal-dialog' role='document'>
                                    <div class='modal-content'>
                                        <div class='modal-header'>
                                            <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                            <h4 class='modal-title' id='exampleModalLabel1'>Edit Mode</h4> </div>
                                        <div class='modal-body'>
                                            <form method='POST' action='#' class='editMode'>
                                                <div class='form-group'>
                                                    <label for='mode' class='control-label'>Payment Mode:</label>
                                                     <input type='hidden' class='form-control' value='{$id}' name='id'>
                                                    <input type='text' class='form-control' value='{$mode}' name='mode'> </div>
                                                
                                        </div>
                                        <div class='modal-footer'>
                                            <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                                            <button type='submit' class='btn btn-primary'> Edit </button>
                                        </div>
                                           </form>
                                    </div>
                                </div>
                            </div>
           <td><a onclick='deleteMode($id)' class=' btn  btn-danger'><i class='fa fa-times-circle'></i></a></td>
           </tr>";

        }

        return $output;

    }

    
}


//fetch expenses



function expense_projections($id)
{

    $sql = "SELECT * FROM expense_projections WHERE client_id = '$id' ORDER BY exp_projection_id DESC";

    $result = query($sql);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

            $count++;
            $id = $trailRow['exp_projection_id'];
            $cat_id = $trailRow['exp_category_id'];

             $sql11 = query("SELECT expense_name FROM expense_categories where exp_category_id='$cat_id'");
             $row11 = mysqli_fetch_array($sql11);
             $catName=$row11['expense_name'];
             $amount = $trailRow['amount'];
             $year = $trailRow['year'];
           


            $output .= "<tr>
           <td>{$count}</td>
           <td>{$catName}</td>          
           <td><button type='button' class='btn btn-info' data-toggle='modal' data-target='#exampleModal-{$id}' data-whatever='@fa'>Edit</button></td>
           
             <div class='modal fade' id='exampleModal-{$id}' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel1'>
                                <div class='modal-dialog' role='document'>
                                    <div class='modal-content'>
                                        <div class='modal-header'>
                                            <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                            <h4 class='modal-title' id='exampleModalLabel1'>Edit Mode</h4> </div>
                                        <div class='modal-body'>
                                            <form method='POST' action='#' class='editMode'>
                                                <div class='form-group'>
                                                    <label for='mode' class='control-label'>Payment Mode:</label>
                                                     <input type='hidden' class='form-control' value='{$id}' name='id'>
                                                    <input type='text' class='form-control' value='{$mode}' name='mode'> </div>
                                                
                                        </div>
                                        <div class='modal-footer'>
                                            <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                                            <button type='submit' class='btn btn-primary'> Edit </button>
                                        </div>
                                           </form>
                                    </div>
                                </div>
                            </div>
           <td><a onclick='deleteExpehnseProjection($id)' class=' btn  btn-danger'><i class='fa fa-times-circle'></i></a></td>
           </tr>";

        }

        return $output;

    }

    
}




//fetch expense cats



function fetch_expense_categories($id)
{

    $sql = "SELECT * FROM expense_categories WHERE client_id = '$id' AND status = 1 ORDER BY exp_category_id DESC";

    $result = query($sql);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

            $count++;
            $id = $trailRow['exp_category_id'];
            $expense_name = $trailRow['expense_name'];
            $expense_description = $trailRow['expense_description'];
            $client_id = $trailRow['client_id'];

            $sql2 = query("SELECT fullName FROM clients WHERE client_id = '$client_id' ");
            while ($row = mysqli_fetch_array($sql2)) {
              $fullName = $row[0];
            }




         
           

         


            $output .= "<tr>
           <td>{$count}</td>
           <td>{$expense_name}</td>   
           <td>{$expense_description}</td>  
            <td>{$fullName}</td>        
           <td><a data-toggle='modal' data-target='#expenseCat-{$id}' data-whatever='@fa'><i class='fa fa-edit fa-2x '><i></a></td>
           
             <div class='modal fade' id='expenseCat-{$id}' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel1'>
                                <div class='modal-dialog' role='document'>
                                    <div class='modal-content'>
                                        <div class='modal-header'>
                                            <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                            <h4 class='modal-title' id='exampleModalLabel1'>Edit Mode</h4> </div>
                                        <div class='modal-body'>
                                            <form method='POST' action='#' class='editMode'>
                                                <div class='form-group'>
                                                    <label for='expense_name' class='control-label'>Payment Mode:</label>
                                                     <input type='hidden' class='form-control' value='{$id}' name='id'>
                                                    <input type='text' class='form-control' value='{$expense_name}' name='expense_name'> 
                                                    </div>                                                
                                                
                                                 <div class='form-group'>
                                                    <label for='expense_description' class='control-label'>Payment Mode:</label>                                                   
                                                    <input type='text' class='form-control' value='{$expense_description}' name='expense_description'> 
                                                    </div>                                                
                                                
                                                </div>
                                        <div class='modal-footer'>
                                            <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                                            <button type='submit' class='btn btn-primary'> Edit </button>
                                        </div>
                                           </form>
                                    </div>
                                </div>
                            </div>
           <td><a onclick='deleteExpenseCat($id)' ><i class='fa fa-trash fa-2x text-danger'></i></a></td>
           </tr>";

        }

        return $output;

    }

    
}


function fetch_revenue_categories($id)
{

    $sql = "SELECT * FROM revenue_categories WHERE client_id = '$id' AND status = 1 ORDER BY rev_category_id DESC";

    $result = query($sql);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

            $count++;
            $id = $trailRow['rev_category_id'];
            $revenue_name = $trailRow['revenue_name'];
            $revenue_description = $trailRow['revenue_description'];
            $client_id = $trailRow['client_id'];

            $sql2 = query("SELECT fullName FROM clients WHERE client_id = '$client_id' ");
            while ($row = mysqli_fetch_array($sql2)) {
              $fullName = $row[0];
            }


            $output .= "<tr>
           <td>{$count}</td>
           <td>{$revenue_name}</td>   
           <td>{$revenue_description}</td>  
            <td>{$fullName}</td>        
           <td><a  data-toggle='modal' data-target='#revenueCat-{$id}' data-whatever='@fa'><i class='fa fa-edit fa-2x'><i></a></td>
           
             <div class='modal fade' id='revenueCat-{$id}' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel1'>
                                <div class='modal-dialog' role='document'>
                                    <div class='modal-content'>
                                        <div class='modal-header'>
                                            <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                            <h4 class='modal-title' id='exampleModalLabel1'>Edit Revenue</h4> </div>
                                        <div class='modal-body'>
                                            <form method='POST' action='#' class='editRevCat'>
                                                <div class='form-group'>
                                                    <label for='revenue_name' class='control-label'>Name:</label>
                                                     <input type='hidden' class='form-control' value='{$id}' name='id'>
                                                    <input type='text' class='form-control' value='{$revenue_name}' name='revenue_name'> 
                                                    </div>                                                
                                                
                                                 <div class='form-group'>
                                                    <label for='revenue_description' class='control-label'>Revenue Description:</label>                                                   
                                                    <input type='text' class='form-control' value='{$revenue_description}' name='revenue_description'> 
                                                    </div>                                                
                                                
                                                </div>
                                        <div class='modal-footer'>

                                           <button type='submit' class='btn btn-primary'> Edit </button>
                                            <button type='button' class='btn btn-danger' data-dismiss='modal'>Close</button>
                                           
                                        </div>
                                           </form>
                                    </div>
                                </div>
                            </div>
           <td><a onclick='deleteRevenueCat($id)' ><i class='fa fa-trash fa-2x'></i></a></td>
           </tr>";

        }

        return $output;

    }

    
}


//fetch institutions

function fetch_institutions($id)
{

    $sql = "SELECT * FROM banks WHERE client_id = '$id' ORDER BY id DESC";

    $result = query($sql);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

            $count++;
            $id = $trailRow['id'];
            $institution = $trailRow['institution'];
         
           

            $categoriesId = base64_encode($id);


            $output .= "<tr>
           <td>{$count}</td>
           <td>{$institution}</td>          
           <td><a data-toggle='modal' data-target='#institution-{$id}' data-whatever='@fa'><i class='fa fa-edit fa-2x'></i></a></td>
           
             <div class='modal fade' id='institution-{$id}' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel1'>
                                <div class='modal-dialog' role='document'>
                                    <div class='modal-content'>
                                        <div class='modal-header'>
                                            <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                            <h4 class='modal-title' id='exampleModalLabel1'>Edit Institution</h4> </div>
                                        <div class='modal-body'>
                                            <form method='POST' action='#' class='editInstitution'>
                                                <div class='form-group'>
                                                    <label for='institution' class='control-label'>Institution Name:</label>
                                                     <input type='hidden' class='form-control' value='{$id}' name='id'>
                                                    <input type='text' class='form-control' value='{$institution}' name='institution'> </div>
                                                
                                        </div>
                                        <div class='modal-footer'>
                                            <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                                            <button type='submit' class='btn btn-primary'> Edit </button>
                                        </div>
                                           </form>
                                    </div>
                                </div>
                            </div>
           <td><a onclick='deleteInstitution($id)' ><i class='fa fa-trash fa-2x'></i></a></td>
           </tr>";

        }

        return $output;

    }

    
}



//sub categories

function fetch_sub_categories($id)
{
 global $connection;
    $sql = "SELECT sub_categories.id, sub_categories.subcategory_name,categories.categories_id ,categories.categories_name FROM sub_categories INNER JOIN categories ON sub_categories.category_id = categories.categories_id WHERE sub_categories.client_id = '$id' ORDER BY sub_categories.id DESC";
 // $sql = "SELECT  sub_categories.id, sub_categories.subcategory_name,categories.categories_id ,categories.categories_name  FROM sub_categories INNER JOIN categories ON sub_categories.category_id = categories.categories_id WHERE sub_categories.client_id = '$id'  ORDER BY id DESC";

    $result = query($sql) OR mysqli_error($connection);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

            $count++;

           // $output = $trailRow;
            $sub_cat_id = $trailRow[0];
            $sub_name = $trailRow[1];
            $cat_id  = $trailRow[2];
            $cat_name = $trailRow[3];
           

            $sub_cat_id_enc = base64_encode($sub_cat_id);



            //selecting categories 

              $sql1 = "SELECT categories_id, categories_name FROM categories WHERE client_id = '$id' ";

              $result1 = query($sql1);

              if (mysqli_num_rows($result1) > 0) {

                 

                 


            $output .= "<tr>
           <td>{$count}</td>
           <td>{$sub_name}</td>
           <td>{$cat_name}</td>
          
         
           <td><a  data-toggle='modal' data-target='#exampleModal-{$sub_cat_id}' data-whatever='@fa'><i class='fa fa-edit fa-2x'></i>  </a></td>
           
             <div class='modal fade' id='exampleModal-{$sub_cat_id}' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel1'>
                                <div class='modal-dialog' role='document'>
                                    <div class='modal-content'>
                                        <div class='modal-header'>
                                            <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                            <h4 class='modal-title' id='exampleModalLabel1'>Edit Sub Category</h4> </div>
                                        <div class='modal-body'>
                                            <form method='POST' action='#' class='subcategoryEditForm'>
                                                <div class='form-group'>
                                                    <label for='recipient-name' class='control-label'>Name:</label>
                                                     <input type='hidden' class='form-control' value='{$sub_cat_id}' name='subcategory_id'>
                                                    <input type='text' class='form-control' value='{$sub_name}' name='subcategory_name'> </div>
                                                <div class='form-group'>
                                                    <label for='message-text' class='control-label'>Category:</label>
                                                   <select name='category' class='form-control'>";
                                                    while ($c = mysqli_fetch_array($result1)) {

                                                      
                                                  $output .= "<option value='{$c[0]}'>{$c[1]}</option> ";
                                                      }}


                                            $output .= " </select>
                                                </div>
                                         
                                        </div>
                                        <div class='modal-footer'>
                                            <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                                            <button type='submit' class='btn btn-primary'><i class='fa fa-edit'></i> Edit </button>
                                        </div>
                                           </form>
                                    </div>
                                </div>
                            </div>
                              <td><a onclick='deleteSubCategory($cat_id)' ><i class='fa fa-trash  text-danger fa-2x'></i> </a></td>
           </tr>";

        }

        return $output;

    }

    
}

//fetch clients
function fetch_clients()
{

    $sql = "SELECT * FROM clients ORDER BY client_id DESC";

    $result = query($sql);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

            $count++;
            $client_id = $trailRow['client_id'];
            $fullName = $trailRow['fullName'];
            $email = $trailRow['email'];
            $username = $trailRow['username'];
            $mobile = $trailRow['mobile'];
            $location = $trailRow['location'];
           

            $output .= "<tr>
           <td>{$count}</td>
           <td>{$fullName}</td>
           <td>{$email}</td>  
           <td>{$username}</td> 
           <td>{$mobile}</td>       
           <td>{$location}</td>
           <td><a href='edit_clients.php?edit={$client_id}' ><i class='fa fa-edit'></i></a></td>
           <td><a onclick='deleteClient($client_id)' class='text-danger'><i class='fa fa-trash'></i></a></td>
           </tr>";

        }

    }

    return $output;
}


//fetch products 


function fetch_products($id)
{

    $sql = "SELECT products.product_id, products.product_name, products.product_image,
    products.categories_id, products.quantity, products.active, products.status, 
     categories.categories_name,products.total_sales,products.price FROM products   
   INNER JOIN categories ON products.categories_id = categories.categories_id  
   WHERE  products.quantity >0 AND products.status = '1' AND  products.client_id = '$id' ";

    $result = query($sql);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

            $count++;
            $product_id = $trailRow[0];          
            $product_image  = substr($trailRow[2], 3);
            $num_sold = $trailRow[8];
            $product_category = $trailRow[7];
            $product_name = $trailRow[1];          
            $quantity = $trailRow[4];
            $status  =  $trailRow[6];
            $price = $trailRow[9];
            $sales_made = $num_sold * $price;
            if ($status === '1') {
                $status = "<span class='badge badge-success'>Available</span>";
            }else{
                $status = "<span class='badge badge-warning'>Not Available</span>"; 

            }


            $prod_id = base64_encode($product_id);
           
           

            $output .= "<tr>
           <td>{$count}</td>           
           <td>{$product_name}</td>  
           <td>{$quantity}</td>                 
           <td>{$product_category}</td>            
           <td>{$status}</td>           
           <td><a   onclick='pNotAvailable($product_id)' class='text-success '><i class='fa fa-battery-half fa-2x'></i>  </a></td>
           <td><a href='edit_products.php?edit={$prod_id}'><i class='fa fa-edit fa-2x'></i> </a></td>
           <td><a onclick='deleteProduct($product_id)'><i class='fa fa-trash fa-2x text-danger'></i> </a></td>
           </tr>";

        }

           return $output;

    }

 
}



//unavailable products 


function fetch_unavailable_products($id)
{

    $sql = "SELECT products.product_id, products.product_name, products.product_image,
    products.categories_id, products.quantity,  products.active, products.status, 
     categories.categories_name FROM products   
   INNER JOIN categories ON products.categories_id = categories.categories_id  
   WHERE  products.status = '0' AND products.client_id = ".$id."";

    $result = query($sql);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

            $count++;
            $product_id = $trailRow[0];   
            $product_category = $trailRow[7];
            $product_name = $trailRow[1];          
            $quantity = $trailRow[4];
            $status  =  $trailRow[6];

            if ($status === 1) {
                $status = "Available";
            }else{
                $status = "Not Available"; 

            }
           
             $prod_id = base64_encode($product_id);
           

            $output .= "<tr>
           <td>{$count}</td>          
           <td>{$product_name}</td>  
           <td>{$quantity}</td>                 
           <td>{$product_category}</td>
           <td>{$status}</td>
           <td><a onclick='pMakeAvailable($product_id)' class='btn btn-success btn-outline'><i class='fa fa-battery-full'></i> To On  </a></td>           
           <td><a href='edit_products.php?edit={$prod_id}' class='btn btn-info btn-outline' ><i class='fa fa-edit'></i> Edit</a></td>
           <td><a onclick='deleteProduct($product_id)' class='btn btn-danger btn-outline '><i class='fa fa-trash'></i></a></td>
           </tr>";

        }


         echo $output;


    }

   
}


function fetch_over_products($id)
{

    $sql = "SELECT products.product_id, products.product_name, products.product_image,
    products.categories_id, products.quantity,  products.active, products.status, 
     categories.categories_name FROM products   
   INNER JOIN categories ON products.categories_id = categories.categories_id  
   WHERE  products.quantity = '0' AND products.client_id = ".$id."";

    $result = query($sql);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {

            $count++;
            $product_id = $trailRow[0];   
            $product_category = $trailRow[7];
            $product_name = $trailRow[1];          
            $quantity = $trailRow[4];
            $status  =  $trailRow[6];

            if ($status === 1) {
                $status = "Available";
            }else{
                $status = "Not Available"; 

            }
           
             $prod_id = base64_encode($product_id);
           

            $output .= "<tr>
           <td>{$count}</td>          
           <td>{$product_name}</td>  
           <td>{$quantity}</td>                 
           <td>{$product_category}</td>
           <td>{$status}</td>
           <td><a onclick='pMakeAvailable($product_id)' class='btn btn-info btn-outline'><i class='fa fa-battery-full'></i> Available  </a></td>           
           <td><a href='edit_products.php?edit={$prod_id}' class='btn btn-info btn-outline'><i class='fa fa-edit'></i></a></td>
           <td><a onclick='deleteProduct($product_id)' class='btn btn-danger btn-outline'><i class='fa fa-trash'></i></a></td>
           </tr>";

        }


         echo $output;


    }

   
}




function fetch_out_of_stock()
{

    $sql = "SELECT order_item.total,order_item.quantityTaken,products.product_name,orders.client_name, orders.client_contact,order_item.created_at FROM order_item INNER JOIN products on order_item.product_id = products.product_id  INNER JOIN orders on order_item.order_id = orders.order_id";    

    $result = query($sql);

    if (mysqli_num_rows($result) > 0) {

        $output = "";

        $count = 0;

        while ($trailRow = mysqli_fetch_array($result)) {
         $count++;
           

        
       

$created_on = f_date($trailRow[5]);


            $output .= "<tr>
           <td>{$count}</td>
           <td>{$trailRow[0]}</td>
           <td>{$trailRow[1]}</td>
           <td>{$trailRow[2]}</td>
           <td>{$trailRow[3]}</td>           
           <td>{$trailRow[4]}</td>
           <td>{$created_on}</td>
          
           </tr>";

        }

    }else{

        echo" No out stcok ";
    }

    return $output;
}



 function total_sales($id){    
    $sql= "SELECT SUM(grand_total) FROM orders WHERE client_id = '$id' ";
    $total = query($sql);
    while ($e = mysqli_fetch_array($total)) {
        $t = $e['SUM(grand_total)']++;
        return $t;  
    }
    
 }



 function total_expenses($id){    
    $sql= "SELECT SUM(amount) FROM expenses WHERE client_id = '$id' ";
    $total = query($sql);
    while ($e = mysqli_fetch_array($total)) {
        $t = $e['SUM(amount)']++;
        return $t;  
    }
    
 }


  function total_revenues($id){    
    $sql= "SELECT SUM(amount) FROM revenues WHERE client_id = '$id' ";
    $total = query($sql);
    while ($e = mysqli_fetch_array($total)) {
        $t = $e['SUM(amount)']++;
        return $t;  
    }
    
 }
