<!DOCTYPE html>
<?php
date_default_timezone_set("Africa/Kampala");
//echo date_default_timezone_get();
?>
<?php
include 'includes/db.php';
session_start();
?>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Receipt</title>

  <!-- Normalize or reset CSS with your favorite library -->
 

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style type = "text/css">
   
      @page { size:8.5in 11in; margin: 0cm }
 
	</style>

  <!-- Custom styles for this document -->
  <link href='https://fonts.googleapis.com/css?family=Tangerine:700' rel='stylesheet' type='text/css'>
   <script type="text/javascript">
        
    function printfinction(){
      var print_div = document.getElementById("printme");
var print_area = window.open();
print_area.document.write(print_div.innerHTML);
print_area.document.close();
print_area.focus();
print_area.print();
print_area.close();
// This is the code print a particular div element
    }
  </script>



</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body style="size:57mm 40mm; margin: 1cm; display: none;" onload="printfinction();">
<div id="printme">
<div  style='box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5);padding:20px;margin: 10px auto;width: 110mm;background: #fff;font-family: "VT323", monospace;'>
<table  width="100%">
	<thead>
		<tr align="center">
			<td colspan="3"><h4>MEGAPAY <br>
					info@megapay.com <br>
					+265-703237229</h4>
						<hr>
			</td>
		</tr>
	</thead>

	<tbody>
		<!--Receipt Number is in the format date(yyyymmdd), transaction_id(taking the rest of the values)  -->
		<tr>
			<th align="left">Receipt Number: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo date('Ymd'); ?><?php echo '12333'; ?></td>
		</tr>
		<tr>
			<th align="left">Date: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo date("D, j M, Y"); ?></td>
		</tr>
		<tr>
			<th align="left">Time: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo date("g:i a");   ?></td>
		</tr>
		<tr>
			<th align="left">Teller name: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo 'todayTeller'; ?></td>
		</tr>

	</tbody>
</table>
<hr>
<table width="100%">
	<tbody>
		<tr>
			<th colspan="3">
				Cash Withdraw made
			</th>
		</tr>
		<tr>
			<th align="left">Amount: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo number_format(120000); ?> UGX</td>
		</tr>
		<tr>
			
			<td colspan="3" align="right">
				<?php 
            echo ucwords(convertNumberToWord("120000"));
                ?>
       shillings only<td>
			
		</tr>
		<tr>
			<th align="left">Cust Account: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo '1'; ?></td>
		</tr>
		<tr>
			<th align="left">Cust Name: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo 'juma'; ?></td>
		</tr>
		<tr>
			<th align="left">Trans ID: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo '2'; ?></td>
		</tr>
		<tr>
			<th align="left">Trans Charge: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo '1700'; ?> UGX</td>
		</tr>
		<tr>
			<th align="left">Account Balance: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo '1200'; ?> UGX</td>
		</tr>

	</tbody>
</table>
<hr>
<table width="100%"> 
	<tr>
		<td>Teller's Signature & stamp
		<p>&nbsp; <p></td>

	</tr>
	<tr><td>------------------------------<p>

	Thank you for saving with <?php echo 'megapay'; ?>. Contact us on +256<?php echo '0706005554'; ?> for any help.

	</td></tr>
</table>
</div>
</div>
</body>
<meta http-equiv="refresh" content="0;URL='manage_accounts.php'" />
</html>