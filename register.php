
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>ONLINE BANKING</title>
    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="css/colors/default.css" id="theme" rel="stylesheet">
    <link href="plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">

</head>

<style>
   
    .btn{
        background-color: #083666;
        outline: none;
        border-radius: 0px;
    }

    .white-box{
        padding: 30px;
        background-color: #fff;
        border-radius: 13px;
        border: 1px solid #26AFC5 ;

    }

    .bg-light{
        padding: 30px;
         background: #eeee !important;
         border-radius: 13px;

    }
    .form-control {
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
        padding-right: 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555555;
    background-color: #ffffff;
    background-image: none;
    border: 1px solid #1a9bb1;}
    #wrapper{
        background: #d6d6d6 !important;
       
       
    }
</style>


<body>

<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper"  >
      
    

                    <div class="row">
                         <div class="col-md-3 "></div>
                    
                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-heading">
                                     <center>
                                        <a href="index.php">
                                            <img src="assets/images/logo.png" alt="logo" width="140">
                                        </a>
                                        <h3 class="box-title m-b-0">Welcome to PayKati Quick Banking</h3>
                                     <small>create an account here</small></center>
                   
                                </div>
                                <div class="panel-body">
                                    <div class="white-box" >
                                    <form action="" id="addClientForm">
                               
                                 
                                        <div class="form-group ">

                                            <label for="email">Email :</label>

                                            <input type="email" name="email" class="form-control" parsley-type="email"
                                                required placeholder="Enter Email Address">

                                        </div>


                                        <div class="form-group ">

                                            <label for="location">Location :</label>

                                            <input type="text" name="location" class="form-control"
                                                placeholder="Enter Location" required>

                                        </div>



                                        <div class="form-group ">

                                            <label for="mobile">Phone :</label>

                                            <input type="text" name="mobile" class="form-control"
                                                data-parsley-type="number" placeholder="Enter Phone Contact" required
                                                data-parsley-minlength="10" data-parsley-maxlength="10">

                                        </div>

                                        <div class="form-group ">


                                            <label for="username">Full Name :</label>

                                            <input type="text" name="fullName" class="form-control"
                                                placeholder="Enter Full Names" required>

                                        </div>


                                        <div class="form-group ">
                                            <label for="username">Username :</label>
                                            <input type="text" name="username" class="form-control"
                                                placeholder="Enter Username" required>
                                        </div>
                                
                                  
                                        <div class="form-group">
                                            <p style="color:red;">Account Details ***</p>
                                        </div>

                                        <div class="alert alert-info">

                                        <div class="form-group">
                                            <label class=" control-label">Mobile Money Number <span
                                                    style="color:red;">*</span></label>

                                            <input type="text" class="form-control" id="" name="mobile_money_number"
                                                placeholder="Mobile Money Number" required="">
                                        </div>


                                        <div class="form-group">
                                            <label class=" control-label">Account Number <span
                                                    style="color:red;">*</span></label>
                                            <input type="text" class="form-control" name="accountno" id=""
                                                placeholder="Account Number">

                                        </div>

                                        <div class="form-group">
                                            <label class=" control-label">Account Name <span
                                                    style="color:red;">*</span></label>
                                            <input type="text" class="form-control" name="accountname" required="">


                                        </div>

                                    </div>


                                 


                                <div class="form-group"><br>

                                    <button type="submit" class="btn btn-info btn-block waves-effect waves-light">
                                        Create Account
                                    </button>
                                   </div>
                                 </form>
                                </div>
                            </div>
                        </div>
                    </div>

                       <div class="col-md-3 "></div>
            </div>
       
    </section>
    <!-- jQuery -->
    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

    <!--slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/custom.min.js"></script>
    <!--Style Switcher -->
    <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
     <script src="plugins/bower_components/sweetalert/sweetalert.min.js"></script>

  <script>
        
    $('form#addClientForm').on("submit", function(e) {
        e.preventDefault();

        var formData = new FormData(this);

        swal({
            title: "Are you sure?",
            text: "Okay to add a  New Client",
            type: "info",
            padding: 20,
            showCancelButton: true,
            confirmButtonColor: "#384888",
            confirmButtonText: "Yes, Continue!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "php_action/create_clients.php?t=true",
                    data: formData,
                    success: function(result) {

                        if (result.status) {
                            swal({
                                title: "Good job!",
                                padding: 20,
                                text: "Good Job! A New client has been Created Successfully!",
                                type: "success"
                            });

                            setTimeout(function() {
                                window.location.href = "suppliers.php";
                            }, 1000);
                        } else {
                            swal({
                                title: "Oops!",
                                padding: 20,
                                text: result.msg + "..please try again!",
                                type: "warning"
                            });
                        }
                    },
                    error: function(jqXHR) {
                        console.log(jqXHR);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });
    });
    </script>
</body>

</html>