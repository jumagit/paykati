<?php  include "includes/header.php"; ?>
    <!-- ============================================================== -->
<?php  include "includes/topNav.php"; ?>

        <!-- End Top Navigation -->
        
<?php  include "includes/sideNav.php"; ?>  
        <!-- ============================================================== -->


 <style>
    .calculator-grid{

  display:grid;
  padding:20px;
  align-content:center;
  justify-content:center;
  grid-template-columns:repeat(4,90px);
  grid-template-rows: minmax(40px,auto) repeat(5,50px);
  
}

.calculator-grid > button{
  cursor:pointer;
  outline:none;
  border:1px solid #ddd;
  background-color: #607D8B;
  font-size:30px;
}

.calculator-grid > button:hover{
  background-color:#ccc;
}

.output{
  justify-content:space-around;
  word-wrap:break-word;
  background-color:#9CCC65;
  align-items:flex-end;
  word-break:break-all;
  padding:5px;
  flex-direction:column;
  grid-column:1/-1;
  height: 90px !important;
  color:#333;
  display:flex;
  
}

.span-two{
  grid-column: span 2;
}

.output .current-operand{
  color:#333;
  font-size:3rem;
}

.output .previous-operand{
  color:#fff;
  font-size:1.5rem;
}


        </style>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">CALCULATOR</h4> </div>
                         <div class="col-lg-9 col-md-8 col-sm-8  col-xs-12">
                       	         <a href="#" class="btn btn-inverse pull-right"> <i class="fa fa-arrow-left"></i> Back</a>
                         </div>
                    
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /row -->
                <div class="row">
                
              
                    <div class="row">
                    <div class="col-md-12">
                        <div class="med">
                         
                             <div class="calculator-grid">

            <div class="output">

                    <div class="previous-operand" data-previous-operand>0</div>
                    <div class="current-operand" data-current-operand>0</div>

            </div>
           

            <button class="span-two" data-all-clear>Ac</button>
            <button data-delete>DEL</button>
            <button data-operation>÷</button>
            <button data-number> 1</button>
            <button data-number>2</button>
            <button data-number>3</button>
            <button data-operation>*</button>
             <button data-number>4</button>
             <button data-number>5</button>
             <button data-number>6</button>
             <button data-operation>+</button>
             <button data-number>7</button>
             <button data-number>8</button>
             <button data-number>9</button>
             <button data-operation>-</button>
             <button data-number>.</button>
             <button data-number>0</button>
             <button class="span-two" data-equals>=</button>


        </div>

                            
                        </div>
                    </div>
                </div>
                <!--./row-->
                </div>
            
            </div>

               <script  type="text/javascript">
        class Calculator{

  constructor(previousOperandTextElement, currentOperandTextElement){
    this.currentOperandTextElement = currentOperandTextElement;
    this.previousOperandTextElement = previousOperandTextElement;
    this.clear();
  }

  //clear
  clear(){

    this.currentOperand = '0';
    this.operation = undefined;
    this.previousOperand = '0';
    

  }

  //append number
  appendNumber(number){
    if(number === '.' && this.currentOperand.includes('.')) return;
    this.currentOperand  = this.currentOperand.toString() + number.toString();
  }

  //delete
  delete(){

    this.currentOperand = this.currentOperand.toString().slice(0,-1);
    
  }

//choose operation
  chooseOperation(operation){
    if(this.currentOperand === '') return;
    if(this.previousOperand !== ''){
      this.compute();
    }
    this.operation = operation;
    this.previousOperand = this.currentOperand;
    this.currentOperand = '';
    

  }

  //compute
  compute(){

    let computation;
    const prev = parseFloat(this.previousOperand);
    const current = parseFloat(this.currentOperand);

    if (isNaN(prev) || isNaN(current)) return;
    switch (this.operation) {
    case '+':
     computation = prev + current;
    break;
      
    case '-':
    computation = prev - current;
    break;

    case '*':
    computation = prev * current;
    break;

    case '÷':
    computation = prev / current;
    break;
       
    
    default:
    return;
    }

    this.operation = undefined;
    this.currentOperand = computation; 
    this.previousOperand = ''; 
    
  }

  //decimal display

  getDisplayNumber(number){
    const stringNumber = number.toString();
    const integerDigits = parseFloat(stringNumber.split('.')[0]);
    const decimalDigits = stringNumber.split('.')[1];
    let integerDisplay;

    if (isNaN(integerDigits)) {
      integerDisplay = '';           
    }else{
      integerDisplay = integerDigits.toLocaleString('en',{maximumFractionDigits: 0});
    }    
    

    if(decimalDigits != null){
      return `${integerDisplay}.${decimalDigits}`;
    }else{
    return integerDisplay;
  }
}

  // getDisplayNumber(number){
  //   const floatNumber = parseFloat(number);
  //   if(isNaN(floatNumber)) return'';
  //   return floatNumber.toLocaleString('en');
  // }

  //display
  updateDisplay(){
    this.currentOperandTextElement.innerText = this.getDisplayNumber(this.currentOperand);

    if(this.operation != null){
      this.previousOperandTextElement.innerText = `${this.getDisplayNumber(this.previousOperand)}${this.operation}${this.getDisplayNumber(this.currentOperand)}`;
    }else{
      this.previousOperandTextElement.innerText = '';
    }
    
  }

}



const numberButtons = document.querySelectorAll('[data-number]');
const operationButtons = document.querySelectorAll('[data-operation]');
const equalsButton = document.querySelector('[data-equals]');
const deleteButton = document.querySelector('[data-delete]');
const allClearButton = document.querySelector('[data-all-clear]');
const previousOperandTextElement = document.querySelector('[data-previous-operand]');
const currentOperandTextElement = document.querySelector('[data-current-operand]');

//creating an object calculator

const calculator = new Calculator(previousOperandTextElement,currentOperandTextElement);

numberButtons.forEach(button=> {

  button.addEventListener('click',() => {
    
    calculator.appendNumber(button.innerText);
    calculator.updateDisplay();
  });

});


operationButtons.forEach(button=> {

  button.addEventListener('click',() => {
    calculator.chooseOperation(button.innerText);
    calculator.updateDisplay();
  });

});

equalsButton.addEventListener('click', button => {

  calculator.compute();
  calculator.updateDisplay();

});

allClearButton.addEventListener('click', button => {

  calculator.clear();
  calculator.updateDisplay();

});

deleteButton.addEventListener('click', button => {

  calculator.delete();
  calculator.updateDisplay();

});
    </script>
 <?php   include "includes/footer.php"; ?>