<?php  include "includes/header.php"; ?>
    <!-- ============================================================== -->
<?php  include "includes/topNav.php"; ?>

        <!-- End Top Navigation -->
        
<?php  include "includes/sideNav.php"; ?>  
        <!-- ============================================================== -->


        <?php

$client_edit = base64_decode($_GET['edit']);

$sql = query("SELECT * FROM clients WHERE client_id = '$client_edit' ");

while ($row = mysqli_fetch_array($sql)) {

    $client_id = $row['client_id'];
    $mobile = $row['mobile']; 
    $username = $row['username'];
    $email = $row['email'];
    $location = $row['location'];
    $fullName = $row['fullName'];
   
   

}

?>


<?php

if (isset($_POST['update'])) {
   
    $E_mobile = clean($_POST['mobile']);
    $E_email = clean($_POST['email']);
    $E_location = clean($_POST['location']);
    $E_fullName = clean($_POST['fullName']);  
    $E_username = clean($_POST['username']);
    $E_client_id = clean($_POST['client_id']);


   

        if (!empty($E_mobile) && !empty($E_location) && !empty($E_username)) {

            $sql = "UPDATE clients SET mobile = '$E_mobile', email = '$E_email', fullName = '$E_fullName',
            username = '$E_username', location = '$E_location' WHERE client_id = '$E_client_id' ";
    
            $update_query = query($sql);
    
            if ($update_query) {
                writeLog("Made an update on the A Client : {$E_fullName}  from {$IP}", $_SESSION['user_id'], "INFO");
    
                $msg = '
                    <div class="col-lg-12">
                        <div class="alert alert-success ">
                            <button type="button" class="close" data-dismiss="alert">×</button> <strong>Well done!</strong> Saving changes! please wait ..............
                        </div>
                        <script type="text/javascript">setTimeout(function() { window.location.href = "suppliers.php";}, 2000);</script>
                     </div>
                    ';
            } else {
                die(mysqli_error($connection));
            }
    
        }


}

?>

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-3 col-md-3 col-xs-12">
                        <h3 class="page-title"> Edit Client Information</h3> </div> </div>
               
                <div class="row"> 
                     <div class="col-md-12">

                        <?php if(isset($msg)){echo $msg;} ?>
                          


                           <div class="panel panel-default">
                           
                            
                               

                               <div class="panel-body">
                                   
                                    <form action="" method="POST" class="pt-2">

                           


                                        <div class="form-group row">

                                            <div class="col-md-3">
                                                <label for="username">Username :</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" name="username" class="form-control"
                                                    value="<?php  echo $username; ?>">

                                                    <input type="hidden" name="client_id" value="<?php echo $client_id; ?>">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-3">
                                                <label for="productPrice">Email :</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="email" name="email" class="form-control"
                                                   value="<?php  echo $email; ?>">
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <div class="col-md-3">
                                                <label for="username">Mobile :</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" name="mobile" class="form-control"
                                                   value="<?php  echo $mobile; ?>">
                                            </div>
                                        </div>

                                       



                                        <div class="form-group row">
                                            <div class="col-md-3">
                                                <label for="suppliercode">Location :</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" name="location" class="form-control"
                                                   value="<?php echo $location; ?>">
                                            </div>
                                        </div>



                                        <div class="form-group row">
                                            <div class="col-md-3">
                                                <label for="fullName">FullNames :</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" name="fullName" class="form-control"
                                                   value="<?php echo $fullName; ?>">
                                            </div>
                                        </div>



                                   <hr>




                                <div class="form-group">
                                    <div class="pull-right">
                                        <button type="submit" class="btn btn-primary " name="update">
                                            <i class="mdi mdi-content-save"></i> Save
                                        </button>
                                       
                                    </div>
                                </div>



                            </form>

                               </div>
                           </div>


                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </div>
    <!-- end page content-->

</div>
<!-- page wrapper end -->


<!--   modal begins here -->




<!-- Footer -->
<?php include"includes/footer.php"; ?>