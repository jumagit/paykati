<!DOCTYPE html>
<?php
date_default_timezone_set("Africa/Kampala");
//echo date_default_timezone_get();
?>
<?php
include '../includes/db.php';
session_start();


$businessid=$_SESSION['business_id'];
$teller=$_SESSION['fullname'];


$code=$_GET['code'];
$amount=$_GET['amount'];
$charge=$_GET['charge'];
$balance=$_GET['balance'];
$accountto =$_GET['receiver'];
$nameto=$_GET['receiver_name'];
$date=$_GET['date'];
$name=$_GET['name'];
$message=$_GET['message'];
$heading=$_GET['heading'];
$loaderbg=$_GET['loaderbg'];
$icon=$_GET['icon'];

//$f = new NumberFormat("en", NumberFormat::SPELLOUT);


$sql = "SELECT name, email, contact, (select max(id)-1 from sacco_transaction where date='$date' AND business_id='$businessid')as transaction_id FROM sacco_business where id='$businessid'";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    $row = mysqli_fetch_assoc($result);
        $sacco_name=$row['name'];
        $sacco_email=$row['email'];
        $sacco_contact=$row['contact'];
        $transaction_id=$row['transaction_id'];

    
} else {
    $errormsg= "0 results".mysqli_error($conn);
}

?>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Receipt</title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style type = "text/css">
   <!--
      @page { size:8.5in 11in; margin: 0cm }
   -->
	</style>

  <!-- Custom styles for this document -->
  <link href='https://fonts.googleapis.com/css?family=Tangerine:700' rel='stylesheet' type='text/css'>
   <script type="text/javascript">
        
    function printfinction(){
      var print_div = document.getElementById("printme");
var print_area = window.open();
print_area.document.write(print_div.innerHTML);
print_area.document.close();
print_area.focus();
print_area.print();
print_area.close();
// This is the code print a particular div element
    }
  </script>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body style="size:57mm 40mm; margin: 1cm; display: none;" onload="printfinction();">
<div id="printme">
<table  width="100%">
	<thead>
		<tr align="center">
			<td colspan="3">
				<h2><?php echo $sacco_name; ?></h2>
					Email: <?php echo $sacco_email; ?>&nbsp; <br>Contact: +256<?php echo $sacco_contact; ?>
						<hr>
			</td>
		</tr>
	</thead>
	<tbody>
		<!--Receipt Number is in the format date(yyyymmdd), transaction_id(taking the rest of the values)  -->
		<tr>
			<th align="left">Receipt Number: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo date('Ymd'); ?><?php echo $transaction_id; ?></td>
		</tr>
		<tr>
			<th align="left">Date: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo date("D, j M, Y"); ?></td>
		</tr>
		<tr>
			<th align="left">Time: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo date("g:i a");   ?></td>
		</tr>
		<tr>
			<th align="left">Teller name: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo $teller; ?></td>
		</tr>

	</tbody>
</table>
<hr>
<table width="100%">
	<tbody>
		<tr>
			<th colspan="3">
				Money Transfer made
			</th>
		</tr>
		<tr>
			<th align="left">Amount: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo number_format($amount); ?> UGX</td>
		</tr>
		<tr>
			
			<td colspan="3" align="right"><?php include 'numbers.php';
echo ucwords(convertNumberToWord("$amount"));
      ?> shillings only<td>
			
		</tr>
		<tr>
			<th align="left">Cust Account: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo $code; ?></td>
		</tr>
		<tr>
			<th align="left">Cust Name: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo $name; ?></td>
		</tr>
		<tr>
			<th align="left">Recipient Account: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo $accountto; ?></td>
		</tr>
		<tr>
			<th align="left">Recipient Name: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo $nameto; ?></td>
		</tr>
		<tr>
			<th align="left">Trans ID: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo $transaction_id; ?></td>
		</tr>
		<tr>
			<th align="left">Trans Charge: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo number_format($charge); ?> UGX</td>
		</tr>
		<tr>
			<th align="left">Account Balance: </th>
			<td>&nbsp;</td>
			<td align="right"><?php echo number_format($balance); ?> UGX</td>
		</tr>

	</tbody>
</table>
<hr>
<table width="100%"> 
	<tr>
		<td>Teller's Signature & stamp
		<p>&nbsp; <p></td>

	</tr>
	<tr><td>------------------------------<p>

	Thank you for saving with <?php echo $sacco_name; ?>. Contact us on +256<?php echo $sacco_contact; ?> for any help.

	</td></tr>
</table>
</div>
</body>
<meta http-equiv="refresh" content="0;URL='manage_accounts.php'" />
</html>