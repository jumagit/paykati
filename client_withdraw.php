<?php include "includes/header.php";?>

<?php include "includes/topNav.php";?>

<?php include "includes/sideNav.php";?>

<?php
$customer_id = $_SESSION['client_id'];
$ac_details = get_overall_client_details($customer_id);
$acc_date = f_date($ac_details['created_at']);
$acc_name = $ac_details['account_name'];
$acc_number = $ac_details['account_number'];
$acc_id = $ac_details['id'];
$acc_balance = $ac_details['balance'];
$name = $ac_details['account_name'];
?>



<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4 class="page-title">WITHDRAW DOCKET FOR <?php echo $name; ?>  <span class="pull-right">TOTAL WITHDRAWS <b><?php echo $ac_details['withdraws'] ?></b></span> </h4>
            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row">
            <div class="col-md-3 col-xs-12">
                <div class="white-box">

                    <div class="user-content text-center"> <a href="javascript:void(0)"><img
                                src="assets/images/employee.png" class="thumb-lg img-circle" alt="img"></a>
                        <hr>
                        <h5 class="page-title ">ACCOUNT NAME: <br> <span
                                class="text-primary"><?php echo $name; ?></span> </h5>

                    </div>
                </div>
            </div>

            <div class="col-md-9 col-xs-12">
                <div class="med">
                    <ul class="nav nav-tabs ">
                        <li class="active tab"> <a href="#info" data-toggle="tab"> <span class="visible-xs">
                                    <i class="fa fa-home"></i></span> <span class="hidden-xs"> Account Info </span>
                            </a>
                        </li>
                        <li class="tab "> <a href="#transfer" data-toggle="tab" aria-expanded="false"> <span
                                    class="visible-xs"></span> <span class="hidden-xs"> Money Transfer </span>
                            </a>
                        </li>
                        <li class="tab "> <a href="#transactions" data-toggle="tab" aria-expanded="false"> <span
                                    class="visible-xs"></span> <span class="hidden-xs">
                                    Transactions </span>
                            </a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="info">
                            <div class="well">
                                <h4 class="page-title"> ACCOUNT DETAILS</h4>
                                <hr>

                                <div class="table-responsive">
                                    <table class="table  table-bordered table-striped">

                                        <tr>
                                            <th><b>ACCOUNT NUMBER</b> </th>
                                            <td><?php echo $acc_number; ?></td>
                                        </tr>

                                        <tr>
                                            <th> <b>ACCOUNT NAME</b> </th>
                                            <td><?php echo $acc_name; ?></td>
                                        </tr>

                                        <tr>
                                            <th><b>BALANCE</b></th>
                                            <td><?php echo number_format($acc_balance); ?></td>
                                        </tr>

                                        <tr>
                                            <th><b>CREATED ON</b> </th>
                                            <td><?php echo $acc_date; ?></td>
                                        </tr>



                                    </table>
                                </div>
                            </div>
                        </div>




                        <div class="tab-pane" id="transfer">
                            <div class="well">
                                <h4 class="page-title"> ACCOUNT TRANSFER ACTIONS</h4>
                                <br>

                                <h4>
                                    <p>
                                        <span class="badge badge-danger"> <i class="fa fa-warning"></i> Note</span> Be
                                        carefull when dealing with Money Transactions
                                    </p>
                                </h4>

                                <button class="btn btn-default btn-block btn-lg"><i class="fa fa-money"></i> Account
                                    Balance:
                                    <strong><?php echo number_format($acc_balance); ?></strong>
                                </button>

                                <br>

                                <button class="btn  btn-dark  btn-lg" data-toggle="modal"
                                    data-target="#add-withdraw-Modal"><i class="fa fa-upload"></i> Withdraw
                                </button>

                            </div>


                        </div>


                        <!-- withdraw model -->

                        <div id="add-withdraw-Modal" class="modal fade" tabindex="-1" role="dialog" backdrop="true"
                            aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header bg-light">
                                        <h4 class="page-title" id="myModalLabel">
                                            WITHDRAW
                                        </h4>
                                    </div>
                                    <div class="modal-body">
                                        <form class="withdraw" method="post" autocomplete="off">
                                            <div class="form-group">

                                                <input type="hidden" name="account_number"
                                                    value="<?php echo $acc_number; ?>">
                                                <input type="hidden" name="balance" value="<?php echo $acc_balance; ?>">
                                            </div>


                                            <div class="form-group">
                                                <label for="withdrawAmount" class=" control-label">Amount*</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                                    <input type="text" class="form-control " name="withdrawAmount"
                                                        data-type="currency" required>
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <label for="member" class=" control-label">Withdrawn By*</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                                    <select class="form-control" name="withdrawnBy" required="">
                                                        <option value="#">--Select member--</option>
                                                        <?php echo fetch_members_per_client($_SESSION['code']); ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                    <button type="submit" id="withdrawbtn" name="withdraw"
                                                        class="btn btn-default  btn-block "> <i
                                                            class="fa fa-refresh"></i> Withdraw </button>
                                                </div>

                                                <div class="col-md-6">
                                                    <button type="button" class="btn  btn-danger btn-block"
                                                        data-dismiss="modal">Cancel
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>






                        <div class="tab-pane" id="transactions">
                            <div class="well">
                                <h4 class="page-title"> MONEY TRANSFERS</h4>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="myTable">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Description</th>
                                                <th>Type</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php echo mega_trans_logs_fetch($_SESSION['client_id'], $acc_number); ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <!--###################### //end of transactions tab ################# -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'includes/footer.php';?>