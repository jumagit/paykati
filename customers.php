<?php  include "includes/header.php"; ?>
    
<?php  include "includes/topNav.php"; ?>

        
<?php  include "includes/sideNav.php"; ?>  
       
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">CUSTOMERS</h4> </div>
                         <div class="col-lg-9 col-md-8 col-xs-12">

                             <a href="add_customer.php" class="pull-right btn btn-default "><i class="fa fa-plus"></i> Add Customer</a>
                            
                        </div>
                    
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /row -->
                <div class="row">
                
              
                    <div class="col-sm-12">
                        <div class="white-box">
                           
                          
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>S/N</th>
                                            <th>Firstname</th>
                                            <th>Lastname</th>
                                            <th>Username</th>
                                            <th>Mobile</th>
                                            <th><i class="fa fa-cogs fa-2x"></i></th>
                                            <th>
                                                <i class="fa fa-edit fa-2x"></i>
                                            </th>

                                            <th>
                                               <i class="fa fa-trash fa-2x"></i>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php echo fetch_customers($_SESSION['client_id']);?>
                                     </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
 <?php   include "includes/footer.php"; ?>