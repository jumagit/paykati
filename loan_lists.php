<?php include "a_includes/header.php";?>
<?php include "a_includes/topNav.php";?>
<?php include "a_includes/sideNav.php";?>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">LOAN APPLICATIONS   </h4>
                <hr>
                <span class=""> <a href="create_plan.php" class="btn btn-sm btn-info">Loan Plan</a>|  <a href="create_loan_type.php" class="btn btn-sm btn-danger">Loan Types</a></span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="loan-list">
                            <colgroup>
                                <col width="10%">
                                <col width="25%">
                                <col width="25%">
                                <col width="20%">
                                <col width="10%">
                                <col width="10%">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Borrower</th>
                                    <th class="text-center">Loan Details</th>
                                    <th class="text-center">Next Payment Details</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php

$i = 1;
$type = query("SELECT * FROM loan_types where id in (SELECT loan_type_id from loan_list) ");
while ($row = $type->fetch_assoc()) {
    $type_arr[$row['id']] = $row['type_name'];
}
$plan = query("SELECT *,concat(months,' month/s [ ',interest_percentage,'%, ',penalty_rate,' ]') as plan FROM loan_plan where id in (SELECT plan_id from loan_list) ");
while ($row = $plan->fetch_assoc()) {
    $plan_arr[$row['id']] = $row;
}
$qry = query("SELECT l.*,concat(c.fullName,', ',c.email)as name, c.mobile, c.location from loan_list l inner join clients c on c.client_id = l.borrower_id  order by id asc");
while ($row = $qry->fetch_assoc()):
    $monthly = ($row['amount'] + ($row['amount'] * ($plan_arr[$row['plan_id']]['interest_percentage'] / 100))) / $plan_arr[$row['plan_id']]['months'];
    $penalty = $monthly * ($plan_arr[$row['plan_id']]['penalty_rate'] / 100);
    $payments = query("SELECT * from payments where loan_id =" . $row['id']);
    $paid = $payments->num_rows;
    $offset = $paid > 0 ? " offset $paid " : "";
    if ($row['status'] == 2):
        $next = query("SELECT * FROM loan_schedules where loan_id = '" . $row['id'] . "'  order by date(date_due) asc limit 1 $offset ")->fetch_assoc()['date_due'];
    endif;
    $sum_paid = 0;
    while ($p = $payments->fetch_assoc()) {
        $sum_paid += ($p['amount'] - $p['penalty_amount']);
    }

    ?>
	                                <tr>
	                                    <td class="text-center"><?php echo $i++ ?></td>
	                                    <td>
	                                        <p>Name :<b><?php echo $row['name'] ?></b></p>
	                                        <p><small>Contact # :<b><?php echo $row['mobile'] ?></small></b></p>
	                                        <p><small>Address :<b><?php echo $row['location'] ?></small></b></p>
	                                    </td>
	                                    <td>
	                                        <p>Reference :<b><?php echo $row['ref_no'] ?></b></p>
	                                        <p><small>Loan type
	                                                :<b><?php echo $type_arr[$row['loan_type_id']] ?></small></b></p>
	                                        <p><small>Plan :<b><?php echo $plan_arr[$row['plan_id']]['plan'] ?></small></b>
	                                        </p>
	                                        <p><small>Amount :<b><?php echo $row['amount'] ?></small></b></p>
	                                        <p><small>Total Payable Amount
	                                                :<b><?php echo number_format($monthly * $plan_arr[$row['plan_id']]['months'], 2) ?></small></b>
	                                        </p>
	                                        <p><small>Monthly Payable Amount:
	                                                <b><?php echo number_format($monthly, 2) ?></small></b></p>
	                                        <p><small>Overdue Payable Amount:
	                                                <b><?php echo number_format($penalty, 2) ?></small></b></p>
	                                        <?php if ($row['status'] == 2 || $row['status'] == 3): ?>
	                                        <p><small>Date Released:
	                                                <b><?php echo date("M d, Y", strtotime($row['date_released'])) ?></small></b>
	                                        </p>
	                                        <?php endif;?>
                                    </td>
                                    <td>
                                        <?php if ($row['status'] == 2): ?>
                                        <p>Date: <b>
                                                <?php echo date('M d, Y', strtotime($next)); ?>
                                            </b></p>
                                        <p><small>Monthly amount:<b><?php echo number_format($monthly, 2) ?></b></small>
                                        </p>
                                        <p><small>Penalty
                                                :<b><?php echo $add = (date('Ymd', strtotime($next)) < date("Ymd")) ? $penalty : 0; ?></b></small>
                                        </p>
                                        <p><small>Payable Amount
                                                :<b><?php echo number_format($monthly + $add, 2) ?></b></small></p>
                                        <?php else: ?>
                                        N/a
                                        <?php endif;?>
                                    </td>
                                    <td class="text-center">
                                        <?php if ($row['status'] == 0): ?>
                                        <span class="badge badge-warning">For Approval</span>
                                        <?php elseif ($row['status'] == 1): ?>
                                        <span class="badge badge-success">Approved</span>                                       
                                        <?php endif;?>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-outline-success " type="button"
                                           onclick="ConfirmLoanApproval('<?php echo $row['ref_no'] ?>')"><i class="fa fa-check"></i>Approve</button>
                                        
                                    </td>

                                </tr>

                                <?php endwhile;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div>
    <!-- end container-fluid -->
</div>
<!-- end page content-->

</div>
<style>
td p {
    margin: unset;
}

td img {
    width: 8vw;
    height: 12vh;
}

td {
    vertical-align: middle !important;
}
</style>


<script>
    $(document).ready(function(){
        approveLoan =   function (id) {
                if(comfirm('Approve Loan')){
                    var id = id; 
                    $.ajax({
                         type: "GET",
                         dataType: "json",
                         url: "php_action/create_plans.php?t=approve&id=" + id,
                         success: function(result) {
                              console.log(result);
                              if (result.status) { 
                                   setTimeout(function() {
                                        window.location.reload();
                                   }, 1000);
                              } 
                         error: function(jqXHR) {
                              console.log(jqXHR);
                         }
                    });
                }
               
          });

        }
     
    })
</script>
<?php include "a_includes/footer.php";?>