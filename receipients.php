<?php  include "includes/header.php"; ?>
<?php  include "includes/topNav.php"; ?>
<?php  include "includes/sideNav.php"; ?>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">RECEIPIENTS</h4>
            </div>
            <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                <a href="create_receipient.php" class="pull-right btn  btn-default"> <i class="fa fa-plus"></i> ADD
                    RECEIPIENTS</a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table id="myTable" class="table table-bordered table-striped" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>S/N</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Location</th>
                                    <th>Contact</th>
                                    <td><i class="mdi mdi-plus-circle"></i></td>
                                    <td><i class="mdi mdi-close-circle"></i></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php echo fetch_members($_SESSION['client_id'],$_SESSION['code']); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php   include "includes/footer.php"; ?>